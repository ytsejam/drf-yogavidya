from yogavidya.apps.statusses.models import Status, StatusComment
from yogavidya.apps.statusses.api.serializers import StatusSerializer, StatusCommentSerializer, CommentSerializer
from rest_framework import generics, status,  viewsets, filters
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from django.contrib.contenttypes.models import ContentType
from fluent_comments.models import FluentComment
class StatusList(generics.ListAPIView):
    serializer_class = StatusSerializer

    def get_queryset(self):
        """
        This view should return a list of all the purchases
        for the currently authenticated user.
        """
        user = self.request.user
        return Status.objects.filter(user=user)

class StatusViewSet(viewsets.ModelViewSet):
	queryset = Status.objects.all().order_by("-created_at")
	serializer_class = StatusSerializer
	

	def get_serializer_context(self):
		context = super(StatusViewSet, self).get_serializer_context()
		context.update({"request": self.request})
		
		return context

	def retrieve(self, request, pk=None):
		queryset = Status.objects.filter()
		status = get_object_or_404(queryset, pk=pk)
		serializer = StatusSerializer(status, context={ 'request': request })
		return Response(serializer.data)

	def perform_create(self, serializer):
		serializer.save(user=self.request.user)

class StatusRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
	queryset = Status.objects.all()
	serializer_class = StatusSerializer

class StatusCommentsListAPIView(generics.ListAPIView):
	queryset = StatusComment.objects.all()
	serializer_class = StatusCommentSerializer

	def get_queryset(self):
		kwarg_pk = self.kwargs.get("id")
		status = get_object_or_404(Status, pk=kwarg_pk)
		print(status)
		return StatusComment.objects.filter(status__pk=kwarg_pk).order_by("-created_at")


class StatusCommentRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
	queryset = StatusComment.objects.all()
	serializer_class = StatusCommentSerializer
	

class StatusCommentViewSet(viewsets.ModelViewSet):
    queryset = FluentComment.objects.all()
    serializer_class = CommentSerializer
    def create(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            data = self.request.data
            comment = data['comment']
            status = data['Status']
            if 'parent' in data:
                parent = data['parent']
            else:
                parent = None
            submit_date = datetime.now()
            content = ContentType.objects.get(model="Status").pk
            comment = FluentComment.objects.create(
            							object_pk=status, 
            							comment=comment, 
            							submit_date=submit_date,
            							content_type_id=content,
            							user_id = self.request.user.id,     
            							site_id=settings.SITE_ID, 
            							parent_id=parent)
            serializer = CommentSerializer(comment,context=  {'request': request})
            return Response(serializer.data)