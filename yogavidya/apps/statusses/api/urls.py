from django.urls import include, path
from rest_framework.routers import DefaultRouter
from yogavidya.apps.statusses.api import views as stav
from .views import StatusCommentViewSet

router = DefaultRouter()
router.register(r"statusses", stav.StatusViewSet)
router.register(r'comment',StatusCommentViewSet)

urlpatterns = [
	path("", include(router.urls)),
	path("statuslist/index/", 
			stav.StatusList.as_view(), 
			name="index-articles"
		),
	path("statusses/<int:id>/", 
			stav.StatusRUDAPIView.as_view(), 
			name="status-detail"
		),
	path("statusses/<int:id>/comments/", 
			stav.StatusCommentsListAPIView.as_view(), 
			name="comments-list"
		),
	path("statusses/comments/<int:pk>/", 
			stav.StatusCommentRUDAPIView.as_view(), 
			name="statuscomment-detail"
		),
	path("statusses/comments/", 
			stav.StatusCommentViewSet.as_view({'get': 'list'}), 
			name="status-comments-list"
		),
]