from rest_framework import serializers, viewsets
from django.contrib.contenttypes.models import ContentType
from sorl_thumbnail_serializer.fields import HyperlinkedSorlImageField
from fluent_comments.models import FluentComment

from yogavidya.apps.statusses.models import Status, StatusComment

class RecursiveField(serializers.Serializer):
		def to_representation(self, value):
				serializer = self.parent.parent.__class__(
						value,
						context=self.context)
				return serializer.data

class CommentSerializer(serializers.ModelSerializer):
		children = RecursiveField(many=True)
		
		class Meta:
				model = FluentComment
				fields = (
						'comment',
						'id',
						'children',
					 )
		


class StatusCommentSerializer(serializers.ModelSerializer):
	status = serializers.StringRelatedField(read_only=True)
	parent = serializers.StringRelatedField(read_only=True)
	user = serializers.StringRelatedField(read_only=True)
	created_at = serializers.SerializerMethodField(read_only=True)
	voters = serializers.SerializerMethodField(read_only=True)

	class Meta:
		model = StatusComment
		exclude = [ "updated_at"]

	def get_created_at(self, instance):
		return instance.created_at.strftime("%d %B %Y")

	def get_voters(self, instance):
		return instance.voters.count()

class StatusSerializer(serializers.ModelSerializer):
	comments = StatusCommentSerializer(many=True, read_only=True)

	user = serializers.StringRelatedField(read_only=True)
	username = serializers.StringRelatedField(source='user', read_only=True)
	user_image = serializers.StringRelatedField(source='user.image.url', read_only=True)
	user_thumbnail = HyperlinkedSorlImageField(
		'60x60',
		options={"crop": "center"},
		source='user.image',
		read_only=True
	)
	slug = serializers.SlugField(read_only=True)
	created_at = serializers.SerializerMethodField(read_only=True)
	likes_count = serializers.SerializerMethodField(read_only=True)
	comments_count = serializers.SerializerMethodField(read_only=True)
	# A thumbnail image, sorl options and read-only


	class Meta:
		model = Status
		exclude = ["updated_at", "height_field", "width_field", "image"]

	def get_created_at(self, instance):
		return instance.created_at.strftime("%d %B %Y")

	def get_user_has_liked(self, instance):
		request = self.context.get("request")
		return instance.users_like.filter(pk=request.user.pk).exists()

	def get_likes_count(self, instance):
		return instance.total_likes

	def get_comments_count(self, instance):
		return instance.comments.count()

	def get_comments(self,obj):
		status_comment = FluentComment.objects.filter(object_pk = obj.id, parent_id = None)
		serializer = CommentSerializer(status_comment,many=True)
		return serializer.data