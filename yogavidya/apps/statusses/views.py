import json
import redis
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.views.decorators.http import require_POST
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import HttpRequest, HttpResponseRedirect
#from django.template import loader
#from django.urls import reverse
from yogavidya.apps.actions.utils import create_action
from yogavidya.decorators import ajax_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.utils import timezone
from django.urls import reverse, reverse_lazy
from .forms import StatusModelForm, StatusCommentModelForm
from yogavidya.apps.status.models import Status, StatusComment
from yogavidya.apps.status.forms import StatusModelImageForm

r = redis.StrictRedis(host=settings.REDIS_HOST,
					port=settings.REDIS_PORT,
					db=settings.REDIS_DB)
from django.utils.decorators import method_decorator
from django.views.generic import (
							CreateView,
							ListView,
							UpdateView,
							DetailView,
							TemplateView,
							View)
def search(request):
		status_list = Status.objects.all()
		status_filter = StatusFilter(request.GET, queryset=status_list)
		return render(request, 'statusses/status_list.html', {'filter': status_filter})

@login_required
def status_create(request):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	if not request.user.is_authenticated():
		raise Http404

	form = StatusModelForm(request.POST or None)
	if request.method == "POST":
		form = StatusModelForm(request.POST or None, request.FILES or None)
		if form.is_valid():
			instance = form.save(commit=False)
			instance.description = request.POST.get("description")
			#assign categories to Status
			instance.categories_set.add(request.POST.get("categories"))
			instance.user = request.user
			instance.save()
			create_action(request.user,_('created_status'), instance)
			messages.success(request, "Succesfully Created")
			return redirect(instance.get_absolute_url())
		else:
			messages.error(request, "Status Cant be Added")
			form = StatusModelForm(data=request.GET)
	context = {
			"form": form
		}
	return render(request, "statusses/status_form.html", context)

def status_list(request):
	queryset_list = Status.objects.active().order_by('-order').order_by('-created_at')
	if request.user.is_staff or request.user.is_superuser:
		queryset_list = Status.objects.all().order_by('-created_at')
	query = request.GET.get("q")
	if query:
		queryset_list = queryset_list.filter(
							Q(title__icontains=query) |
							Q(content__icontains=query) |
							Q(user__first_name_icontains=query) |
							Q(user__last_name_icontains=query)
							).distinct()
	paginator = Paginator(queryset_list, 6)  # Show 6 articles per page
	number_of_pages = paginator.num_pages
	status_request_var = "page"
	page = request.GET.get(status_request_var)
	try:
			queryset = paginator.page(page)
	except PageNotAnInteger:
			# If page is not an integer, deliver first page.
			queryset = paginator.page(1)
	except EmptyPage:
		if request.is_ajax():
			return HttpResponse('')
			# If page is out of range (e.g. 9999), deliver last page of results.
		queryset = paginator.page(paginator.num_pages)
	if request.is_ajax():
			status_list = queryset_list
			context = {
						'is_paginated': True,
						'number_of_pages': number_of_pages,
						'statusses': queryset,
						'status_list': status_list,
						"status_request_var": status_request_var,
						"title": "Status List",
			}
			if request.user.is_authenticated:
				return render(request, 'statusses/status_list_logged.html', context)
			return render(request, 'statusses/status_list.html', context)
	status_list  = queryset_list
	context = {
				'is_paginated': True,
				'statusses': queryset,
				'status_list': status_list,
				"status_request_var": status_request_var,
				"title": "Status List",

	}
	if request.user.is_authenticated:
		return render(request, 'statusses/status_list_logged.html', context)
	return render(request, 'statusses/status_list.html', context)


def status_detail(request, slug):
		status = get_object_or_404(Status, slug=slug)
		similar_statusses = status.tags.similar_objects()
		total_views = r.incr('status:{}:views'.format(status.id))

		r.zincrby('status_ranking', status.id, 1)
		comments = status.comments.all()
		if request.method == 'POST':
		# comment has been added
			comment_form = StatusCommentModelForm(data=request.POST)
			print(comment_form)
			if comment_form.is_valid():
				parent_obj = None
				# get parent comment id from hidden input
				try:
					# id integer e.g. 15
					parent_id = int(request.POST.get('parent_id'))
				except:
					parent_id = None
				# if parent_id has been submitted get parent_obj id
				if parent_id:
					parent_obj = StatusComment.objects.get(id=parent_id)
					# if parent object exist
					if parent_obj:
						# create replay comment object
						replay_comment = comment_form.save(commit=False)
						# assign parent_obj to replay comment
						replay_comment.parent = parent_obj
				# normal comment
				# create comment object but do not save to database
				new_comment = comment_form.save(commit=False)
				# assign ship to the comment
				new_comment.status = status
				# save
				new_comment.save()
		else:
			comment_form = StatusCommentModelForm()
		context = {
					'status': status,
					'similar_statusses': similar_statusses,
					'total_views':total_views,
					'comments': comments,
					'comment_form': comment_form

		}
		return render(request, 'statusses/status_detail.html', context)
 
@login_required
def status_ranking(request):
	status_ranking = r.zrange('status_ranking', 0, -1, desc=True)[:10]
	status_ranking_ids = [int(id) for id in status_ranking]
	#get most viewed statusses
	most_viewed = list(Status.objects.filter(id__in=status_ranking_ids))
	most_viewed.sort(key=lambda x: status_ranking_ids.index(x.id))
	context = {
		'most_viewed': most_viewed
	}
	return render(reques, 'statusses/status_ranking.html', context)

@ajax_required
@login_required
def status_update(request):
		status_id = request.POST.get('status_id')
		print(status_id)
		instance = get_object_or_404(Status, pk=status_id)
		print(instance)
		form = StatusModelForm(request.POST or None,
											 request.FILES or None, instance=instance)
		if form.is_valid():
			instance = form.save(commit=False)
			instance.content = request.POST.get("content")
			instance.save()
			create_action(request.user,_('updated status'), instance)
			messages.success(request, "Succesfully Updated")
			data = {'is_valid': True, 'content': instance.content, 'status':'ok'}

			return JsonResponse(data)


@ajax_required
@login_required
def status_delete(request):
		status_id = request.POST.get('status_id')
		print(status_id)
		instance = get_object_or_404(Status, pk=status_id)
		instance.delete()
		create_action(request.user, _('Deleted status'), instance)
		messages.success(request, "Succesfully Deleted")
		return JsonResponse({'status': 'ok'})



@ajax_required
@login_required
@require_POST
def status_like(request):
	status_id = request.POST.get('id')
	action = request.POST.get('action')
	if status_id and action:
		try:
			status = Status.objects.get(id=status_id)
			print(status.total_likes)
			if action == 'like':
				status.users_like.add(request.user)
				print(status.total_likes)
				status.total_likes += 1
				print(status.total_likes)
				create_action(request.user, '_(likes)', status)
			else:
				status.users_like.remove(request.user)
				status.total_likes -= 1
			return JsonResponse({'status': 'ok'})
		except:
			pass
		return JsonResponse({'status': 'ko'})

@ajax_required
@login_required
@require_POST
def create_status(request):
	if request.method == 'POST':
		status_text = request.POST.get('content')
		data = {}

		status = Status(content=status_text, user=request.user.pk)
		status.save()

		data['result'] = 'Create Status successful!'
		data['status'] = status.pk
		data['content'] = status.content
		data['created_at'] = status.created_at.strftime('%B %d, %Y %I:%M %p')
		data['user'] = status.user.username

		return JsonResponse({'status': 'ok', 'data': data})
	else:
		pass
	return JsonResponse({'status': 'ko'})

@ajax_required
@login_required
@require_POST
def create_media(request):
	if request.method == 'POST':
		status_image = request.POST.get('image')
		user_id_image = request.POST.get('os_id')
		data = {}

		status = Status(image=status_image, user=user_id_image)
		status.save()

		data['result'] = 'Create Media successful!'
		data['status'] = status.pk
		data['image'] = status.image
		data['created_at'] = status.created_at.strftime('%B %d, %Y %I:%M %p')
		data['user'] = status.user.username

		return JsonResponse({'status': 'ok', 'data': data})
	else:
		pass


	return JsonResponse({'status': 'ko'})


@method_decorator([login_required], name='dispatch')
class StatusMediaView(CreateView):
	model = Status
	form_class = StatusModelImageForm
	success_url = reverse_lazy('profiles:profile_index')

	def post(self, request, *args, **kwargs):
		form = StatusModelImageForm(request.POST or request.FILES or None)
		status_image = request.POST.get('image')
		if form.is_valid():
			form.instance.image = status_image
			form.instance.user = self.request.user
			form.save()
		
			messages.success(self.request, 'Profile photo updated with success!')
			return HttpResponse(reverse_lazy('profiles:profile_index'))
		else:
			return HttpResponse(reverse_lazy('profiles:profile_index'))
			
@ajax_required
@login_required
@require_POST
def create_answer(request):
	if request.method == 'POST':
		status_text = request.POST.get('content')
		status_id = request.POST.get('status_id')
		
		data = {}

		status = StatusComment(content=status_text, user=request.user, status_id=status_id)
		status.save()
		print(status)
		data['result'] = 'Create StatusComment successful!'
		data['node_id'] = status.pk
		data['content'] = status.content
		data['created_at'] = status.created_at.strftime('%B %d, %Y %I:%M %p')
		data['user'] = status.user.username

		return JsonResponse({'status': 'ok', 'data': data})
	else:
		pass
	return JsonResponse({'status': 'ko'})


@ajax_required
@login_required
@require_POST
def create_commentanswer(request):
	if request.method == 'POST':

		status_text = request.POST.get('content')
		print(status_text)
		node_id = request.POST.get('node_id')
		data = {}

		status = StatusComment(content=status_text, user=request.user, parent=node_id)
		status.save()

		data['result'] = 'Create StatusComment successful!'
		data['status'] = status.pk
		data['content'] = status.content
		data['created_at'] = status.created_at.strftime('%B %d, %Y %I:%M %p')
		data['user'] = status.user.username

		return JsonResponse({'status': 'ok', 'data': data})
	else:
		pass
	return JsonResponse({'status': 'ko'})


@ajax_required
def status_list_ajax(request):
	queryset_list = Status.objects.order_by('-order').order_by('-created_at').values('content',)

	if request.is_ajax():
			status_list = json.dumps(list(queryset_list))
			data = {}
			data = {
						'status_list': status_list,
						"title": "Status List",
			}
			return JsonResponse(data)

def status_list_ajaxtt(request):
	queryset_list = Status.objects.order_by('-order').values('content')
	if request.user.is_staff or request.user.is_superuser:
		queryset_list = Status.objects.all().values('content')
	query = request.GET.get("q")
	if query:
		queryset_list = queryset_list.filter(
							Q(title__icontains=query) |
							Q(content__icontains=query) |
							Q(user__first_name_icontains=query) |
							Q(user__last_name_icontains=query)
							).distinct()
	paginator = Paginator(queryset_list, 4)  # Show 6 articles per page
	number_of_pages = paginator.num_pages
	status_request_var = "page"
	page = request.GET.get(status_request_var)
	try:
			queryset = paginator.page(page)
	except PageNotAnInteger:
			# If page is not an integer, deliver first page.
			queryset = paginator.page(1)
	except EmptyPage:
		if request.is_ajax():
			return HttpResponse('')
			# If page is out of range (e.g. 9999), deliver last page of results.
		queryset = paginator.page(paginator.num_pages)
	if request.is_ajax():
			status_list = json.dumps(list(queryset_list))
			
			print(status_list)

			context = {
						 
						'status_list': status_list,
						 
			}
			if request.user.is_authenticated:
				JsonResponse(status_list, safe=False)
			
	status_list  = json.dumps(list(queryset_list))
	context = {
				 
				'status_list': status_list,
				 

	}
 
	if request.user.is_authenticated:
		JsonResponse(status_list, safe=False)
