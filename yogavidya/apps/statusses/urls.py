from django.contrib import admin
from django.urls import include, path
from .views import(
    create_answer,
    create_media,
    create_commentanswer,
    create_status,
    status_like,
    status_update,
    status_delete,
    status_list_ajax,
    StatusMediaView
)

app_name = 'status'
"""
urlpatterns = [
    url(r'^$', status_list, name='list'),
    # ex: /category/5/
    url(r'^detail/(?P<slug>[\w-]+)/$', status_detail, name='detail'),
    url(r'^create/$', status_create, name='create'),
    url(r'^(?P<status_id>[0-9]+)/edit/$',status_update, name='update'),
    url(r'^(?P<status_id>[0-9]+)/delete/$',status_delete, name='delete'),
]
"""
urlpatterns = [
    # ex: /category/5/
    path('answer/', create_answer, name='create_answer'),
    path('comment-answer/', create_commentanswer, name='create_commentanswer'),
    path('media/', StatusMediaView.as_view(), name='create_media'),
    path('create/', create_status, name='create_status'),
    path('list/', status_list_ajax, name='ajax_list'),
    path('like',status_like, name='like'),
    path('edit/',status_update, name='update'),
    path('delete/',status_delete, name='delete'),
]
