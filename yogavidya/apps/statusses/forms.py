from django import forms
from yogavidya.apps.statusses.models import Status, StatusComment
from PIL import Image
from django.core.files import File

class StatusModelForm(forms.ModelForm):
	
	class Meta:
	   model=Status
	   fields = ["user", "content"]

class StatusCommentModelForm(forms.ModelForm):
	
	class Meta:
	   model=StatusComment
	   fields = ["parent", "content"]


class StatusModelImageForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())

	class Meta:
		model = Status
		fields = [ 'image', 'x', 'y', 'width', 'height',]

	def save(self):
		image = super(StatusModelImageForm, self).save()

		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(image.file)
		cropped_image = image.crop((x, y, w+x, h+y))
		resized_image = cropped_image.resize((200, 200), Image.ANTIALIAS)
		resized_image.save(image.file.path)

		return image