from django.contrib import admin
from yogavidya.apps.statusses.models import Status, StatusComment
 
admin.site.register(Status)
admin.site.register(StatusComment)
