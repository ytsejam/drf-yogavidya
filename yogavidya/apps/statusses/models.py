# -*- coding: utf-8 -*-
from django.conf import settings
from django.urls import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

from mptt.models import MPTTModel, TreeForeignKey
#from django.contrib.auth.models import User
# Create your models here.
class Status(models.Model):

    def upload_location(instance, filename):
        #filebase, extension = filename.split(".")
        # return "%s/%s.%s" %(instance.id, instance.id, extension)
        try:
            StatusModel = instance.__class__
            new_id = StatusModel.objects.order_by("id").last().id + 1
        except:
            new_id=1

        return "{} {}".format(new_id, filename)
    # Relations
    user = models.ForeignKey(
                settings.AUTH_USER_MODEL, 
                default=1,
                related_name='statusses',
                verbose_name=_("related_user"), 
                on_delete=models.CASCADE,)
 
    # Attributes - Mandatory
    id = models.AutoField(primary_key=True)
    content = models.TextField()
    height_field = models.IntegerField(default=1024, null=True, blank=True)
    width_field = models.IntegerField(default=768, null=True, blank=True)
    image = models.ImageField(
                            null=True,
                            blank=True,
                            height_field="height_field",
                            width_field="width_field",
                            upload_to=upload_location,)
    created_at = models.DateTimeField(
        auto_now=True, auto_now_add=False, verbose_name=("created_at"))
    updated_at = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name=_(
        "updated_at"), help_text=_("Enter a new date"))
    # Attributes - Optional
    users_like = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                        related_name='status_liked',
                                        blank=True)
    total_likes = models.PositiveIntegerField(db_index=True, default=0)
    # Object Manager

    # Custom Properties

    # Methods

    # Meta and String
    class Meta:
        verbose_name = _("Status")
        verbose_name_plural = _("Statuses")
        ordering = ("user",)
        
    def __str__(self):
        return "{}".format(self.user.username)

    def get_all_children(self, include_self=False):
        """
        Gets all of the comment thread.
        """
        children_list = self._recurse_for_children(self)
        if include_self:
            ix = 0
        else:
            ix = 1
        flat_list = self._flatten(children_list[ix:])
        return flat_list

    def _recurse_for_children(self, node):
        children = []
        children.append(node)
        for child in node.sub_comment.enabled():
            if child != self:
                children_list = self._recurse_for_children(child)
                children.append(children_list)
        return children

    def _flatten(self, L):
        if type(L) != type([]): return [L]
        if L == []: return L
        return self._flatten(L[0]) + self._flatten(L[1:])
    def get_parent(self): 
        if self.parent:
            return self.parent 
        else: 
            return self 
    def get_thread(self): 
        return self.children.all()


class StatusComment(MPTTModel):
    parent = models.ForeignKey('self', 
                            null=True, 
                            blank=True, 
                            related_name='replies',
                            on_delete=models.CASCADE)
    user = models.ForeignKey(
                    settings.AUTH_USER_MODEL,
                    default=1,
                    null=True,
                    on_delete=models.CASCADE,
                    related_name="statuscomments")
    status = models.ForeignKey(
                    Status, 
                    on_delete=models.CASCADE,
                    related_name="comments")
    active = models.BooleanField(default=True)
    published_at = models.DateField(
                    auto_now=False,
                    auto_now_add=False,
                    null=True,
                    blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    content = models.TextField()
    voters = models.ManyToManyField(settings.AUTH_USER_MODEL,blank=True,
                                    related_name="status_comment_votes")

    class Meta:
        # sort comments in chronological order by default
        ordering = ('created_at',)

    def __str__(self):
        return 'Comment by {}'.format(self.user.username)

