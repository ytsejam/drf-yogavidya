from django.contrib import admin

from yogavidya.apps.locations.models import Area, City, Country

admin.site.register(Area)
admin.site.register(City)
admin.site.register(Country)
