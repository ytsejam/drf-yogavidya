from django.urls import include, path
from rest_framework.routers import DefaultRouter
from yogavidya.apps.locations.api import views as lov

router = DefaultRouter()
router.register(r"countries", lov.CountryViewSet)
router.register(r"states", lov.StateViewSet)
router.register(r"cities", lov.CityViewSet)
router.register(r"areas", lov.AreaViewSet)

urlpatterns = [
	path("", include(router.urls))
]