from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from yogavidya.apps.locations.api.serializers import CountrySerializer
from yogavidya.apps.locations.api.serializers import StateSerializer
from yogavidya.apps.locations.api.serializers import CitySerializer
from yogavidya.apps.locations.api.serializers import AreaSerializer
from yogavidya.apps.locations.models import Country, City, Area, State

class CountryViewSet(viewsets.ModelViewSet):
	queryset = Country.objects.all()
	lookup_field = "slug"
	serializer_class = CountrySerializer
	permission_classes = [ IsAuthenticated]

	def perform_create(self, serializer):
		serializer.save(user=self.request.user)


class StateViewSet(viewsets.ModelViewSet):
	queryset = State.objects.all()
	lookup_field = "slug"
	serializer_class = StateSerializer
	permission_classes = [ IsAuthenticated]

	def perform_create(self, serializer):
		serializer.save(user=self.request.user)


class CityViewSet(viewsets.ModelViewSet):
	queryset = City.objects.all()
	lookup_field = "slug"
	serializer_class = CitySerializer
	permission_classes = [ IsAuthenticated]

	def perform_create(self, serializer):
		serializer.save(user=self.request.user)


class AreaViewSet(viewsets.ModelViewSet):
	queryset = Area.objects.all()
	lookup_field = "slug"
	serializer_class = AreaSerializer
	permission_classes = [ IsAuthenticated]

	def perform_create(self, serializer):
		serializer.save(user=self.request.user)