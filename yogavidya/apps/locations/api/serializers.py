from rest_framework import serializers
from yogavidya.apps.locations.models import Area,City, Country, State



class CountrySerializer(serializers.ModelSerializer):
	slug = serializers.SlugField(read_only=True)
	created_at = serializers.SerializerMethodField(read_only=True)
	class Meta:
		model = Country
		exclude = [  "updated_at"]


	def get_created_at(self, instance):
		return instance.created_at.strftime("%d %B %Y")

class StateSerializer(serializers.ModelSerializer):
	slug = serializers.SlugField(read_only=True)
	created_at = serializers.SerializerMethodField(read_only=True)
	country = serializers.StringRelatedField(read_only=True)
	class Meta:
		model = State
		exclude = [  "updated_at"]


	def get_created_at(self, instance):
		return instance.created_at.strftime("%d %B %Y")

class CitySerializer(serializers.ModelSerializer):	
	slug = serializers.SlugField(read_only=True)
	created_at = serializers.SerializerMethodField(read_only=True)
	country = serializers.StringRelatedField(read_only=True)
	class Meta:
		model = City
		exclude = [  "updated_at"]


	def get_created_at(self, instance):
		return instance.created_at.strftime("%d %B %Y")

class AreaSerializer(serializers.ModelSerializer):
	slug = serializers.SlugField(read_only=True)
	created_at = serializers.SerializerMethodField(read_only=True)
	city = serializers.StringRelatedField(read_only=True)
	class Meta:
		model = Area
		exclude = [  "updated_at"]


	def get_created_at(self, instance):
		return instance.created_at.strftime("%d %B %Y")