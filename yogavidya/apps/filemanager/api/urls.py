from django.urls import include, path
from rest_framework import routers
from yogavidya.apps.filemanager.api.views import DataViewSet

router = routers.DefaultRouter()
router.register(r'files', DataViewSet, basename='data')

urlpatterns = [
		path("", include(router.urls)),
]