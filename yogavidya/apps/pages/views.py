from django.contrib import messages
from django.shortcuts import get_object_or_404, render
from django.http import HttpRequest, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.utils import timezone
from .forms import PageModelForm
from yogavidya.apps.pages.models import Page


def page_create(request):
  if not request.user.is_staff or not request.user.is_superuser:
    raise Http404
  if not request.user.is_authenticated():
    raise Http404

  form = PageModelForm(request.POST or None)
  if request.method == "POST":
    form = PageModelForm(request.POST or None, request.FILES or None)
    if form.is_valid():
      instance = form.save(commit=False)
      instance.description = request.POST.get("description")
      instance.save()
      messages.success(request, "Succesfully Created")
      return HttpResponseRedirect(instance.get_absolute_url())
    else:
      messages.error(request, "Image Cant be Added")
  context = {
      "form": form
    }
  return render(request, "pages/page_form.html", context)

def page_list(request):
  today=timezone.now().date()
  queryset_list = Page.objects.active()

  if  request.user.is_staff or  request.user.is_superuser:
    queryset_list = Page.objects.all()
  query = request.GET.get("q")
  if query:
    queryset_list = queryset_list.filter(
              Q(title__icontains=query) |
              Q(content__icontains=query) |
              Q(user__first_name_icontains=query) |
              Q(user__last_name_icontains=query)
              ).distinct()
  paginator = Paginator(queryset_list, 9) # Show 25 contacts per article
  page_request_var = "page"
  page = request.GET.get('page_request_var')
  try:
      queryset = paginator.page(page)
  except PageNotAnInteger:
      # If page is not an integer, deliver first page.
      queryset = paginator.page(1)
  except EmptyPage:
      # If page is out of range (e.g. 9999), deliver last page of results.
      queryset = paginator.page(paginator.num_pages)
  context = {

        'images': queryset,
        'page_list': page_list,
        "page_request_var": page_request_var,
        "title": "List",
        "today": today,
  }
  return render(request, 'pages/page_list.html', context)

def page_detail(request, slug):
    page = get_object_or_404(Page, slug=slug)
    context = {
          'page': page
    }
    return render(request, 'pages/page_detail.html', context)

def page_update(request, page_id):
    instance = get_object_or_404(Page, pk=page_id)
    if instance.draft or instance.publish > timezone.now().date():
      if not request.user.is_staff or not request.user.is_superuser:
        raise Http404
    form = PageModelForm(request.POST or None, request.FILES or None, instance = instance)
    if form.is_valid():
      instance = form.save(commit=False)
      instance.description = request.POST.get("description")
      print(request.POST["description"])
      instance.save()
      messages.success(request, "Item Saved")
      return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        'instance': instance,
        "form": form
      }

    return render(request, 'pages/page_update.html', context)

def page_delete(request, page_id):
    instance = get_object_or_404(Page, pk=page_id)
    instance.delete()
    messages.success(request, "Succesfully Deleted")
    return redirect("pages:list")
