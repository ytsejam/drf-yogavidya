# -*- coding: utf-8 -*-
from django.conf import settings
from django.urls import reverse
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
from sorl.thumbnail import ImageField
# from django.contrib.auth.models import User
# Create your models here.


class Page(models.Model):

    def upload_location(instance, filename):
        #filebase, extension = filename.split(".")
        # return "%s/%s.%s" %(instance.id, instance.id, extension)
        try:
            PageModel = instance.__class__
            new_id = PageModel.objects.order_by("id").last().id + 1
        except:
            new_id = 1


        return "{} {}".format(new_id, filename)
    # Relations
 
    # Attributes - Mandatory
    title = models.CharField(max_length=200, verbose_name=_(
        "title"), help_text=_("Enter the Page title"))
    #user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1,  verbose_name=_(
    #    "related_user"),related_name="pages",blank=True, null=True, on_delete=models.CASCADE,)
    slug = models.SlugField(unique=True)
    height_field = models.IntegerField(default=0)
    width_field = models.IntegerField(default=0)
    image = models.ImageField(null=True, blank=True, upload_to=upload_location,
                              height_field="height_field", width_field="width_field",)
    author = models.CharField(max_length=500, default="Yoga Vidya",)
    content = models.TextField(verbose_name=_(
        "content"), help_text=_("Enter Page content"))
    draft = models.BooleanField(default=False)
    publish = models.DateField(auto_now=False, auto_now_add=False)
    created_at = models.DateTimeField(
        auto_now=True, auto_now_add=False, verbose_name=("created_at"))
    updated_at = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name=_(
        "updated_at"), help_text=_("Enter a new date"))
    # Attributes - Optional

    # Object Manager

    # Custom Properties

    # Methods
    def get_absolute_url(self):
        return reverse("pages:detail", kwargs={"slug": self.slug})

    def create_slug(instance, new_slug=None):
        slug = slugify(instance.title)
        if new_slug is not None:
            slug = new_slug
        qs = Page.objects.filter(slug=slug).order_by("-id")
        exists = qs.exists()
        if exists:
            new_slug = "%s-%s" % (slug, qs.first().id)
            return create_slug(instance, new_slug=new_slug)
        return slug

    # Meta and String
    class Meta:
        verbose_name = _("Page")
        verbose_name_plural = _("Pages")
        ordering = ( "title",)
        unique_together = ( "title",)

    def __str__(self):
        return "{}".format( self.title)


@receiver(pre_save, sender=Page)
def pre_save_page_receiver(sender, instance, raw, using, **kwargs):
    if instance and not instance.slug:
        slug = slugify(instance.title)
        random_string = generate_random_string()
        instance.slug = slug + "_" + random_string

pre_save.connect(pre_save_page_receiver, sender=Page)
