from django.contrib import admin
from django.urls import include, path
from .views import(
    page_list,
    page_create,
    page_detail,
    page_update,
    page_delete,
)

app_name = 'pages'

urlpatterns = [
    path('', page_list, name='list'),
    # ex: /category/5/
    path('p/<slug:slug>', page_detail, name='detail'),
    path('create/', page_create, name='create'),
    path('<int:pk>/edit/',page_update, name='update'),
    path('<int:pk>/delete/',page_delete, name='delete'),
]
