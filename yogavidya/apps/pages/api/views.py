from rest_framework import generics, status,  viewsets, filters
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404
from rest_framework.views import APIView
from django.views.generic import ListView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from yogavidya.apps.pages.api.permissions import IsAuthorOrReadOnly
from yogavidya.apps.pages.api.serializers import PageSerializer
from yogavidya.apps.pages.models import Page 

class PageViewSet(viewsets.ModelViewSet):
	queryset = Page.objects.all().order_by("-created_at")
	serializer_class = PageSerializer
	lookup_field = "slug"
	filter_backends = (filters.SearchFilter,)
	search_fields = ('index_page')

	def get_serializer_context(self):
		context = super(PageViewSet, self).get_serializer_context()
		context.update({"request": self.request})
		
		return context

	def retrieve(self, request, slug=None):
		queryset = Page.objects.filter()
		page = get_object_or_404(queryset, slug=slug)
		serializer = PageSerializer(page, context={ 'request': request })
		return Response(serializer.data)

	def perform_create(self, serializer):
		serializer.save(user=self.request.user)
