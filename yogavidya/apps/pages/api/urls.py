from django.urls import include, path
from rest_framework.routers import DefaultRouter
from yogavidya.apps.pages.api import views as pav

router = DefaultRouter()
router.register(r"pages", pav.PageViewSet)

urlpatterns = [
	path("", include(router.urls)),

]