from rest_framework import serializers
from yogavidya.apps.pages.models import Page

class PageSerializer(serializers.ModelSerializer):
	slug = serializers.SlugField(read_only=True)
	created_at = serializers.SerializerMethodField(read_only=True)
	
	# A larger version of the image, allows writing
	class Meta:
		model = Page
		
		fields = "__all__"
	def get_created_at(self, instance):
		return instance.created_at.strftime("%d %B %Y")

