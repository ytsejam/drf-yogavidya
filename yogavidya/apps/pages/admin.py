from django.contrib import admin
from django import forms
from yogavidya.apps.pages.models import Page
from yogavidya.apps.pages.forms import PageModelForm

class PageAdmin(admin.ModelAdmin):
  form = PageModelForm
  prepopulated_fields = {'slug': ('title',), }
  class Meta:
    model = Page

admin.site.register(Page, PageAdmin)
