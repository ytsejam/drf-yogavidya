from django import forms
from django.contrib import admin
from django.db import models
from yogavidya.apps.pages.models import Page

class PageModelForm(forms.ModelForm):
	class Meta:
	   model=Page
	   fields = [ "title", "slug", "content", "image", "draft","publish"]
 