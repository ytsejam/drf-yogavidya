from django.conf import settings
from django.contrib.auth.forms import PasswordResetForm
from rest_framework import serializers
from yogavidya.apps.profiles.models import Contact, User
from sorl_thumbnail_serializer.fields import HyperlinkedSorlImageField

from yogavidya.apps.profiles.models import Contact, User
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView

from django.core.exceptions import PermissionDenied
from django.utils.translation import gettext as _

from allauth.socialaccount.models import SocialLogin
from rest_auth.registration.serializers import SocialLoginSerializer
from rest_framework.exceptions import ValidationError
from sorl.thumbnail import get_thumbnail

class ContactsSerializer(serializers.ModelSerializer):
	user_from = serializers.StringRelatedField(read_only=True)
	user_to = serializers.StringRelatedField(read_only=True)
	class Meta:
		model = Contact
		fields = ["user_from", "user_to"]

class CustomTokenSerializer(serializers.Serializer):
	token = serializers.CharField()

class CallbackSerializer(SocialLoginSerializer):
	state = serializers.CharField()

	def validate_state(self, value):
		"""
		Checks that the state is equal to the one stored in the session.
		"""
		try:
			SocialLogin.verify_and_unstash_state(
				self.context['request'],
				value,
			)
		# Allauth raises PermissionDenied if the validation fails
		except PermissionDenied:
			raise ValidationError(_('State did not match.'))
		return value
 

class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
	def validate(self, attrs):
		data = super().validate(attrs)
		refresh = self.get_token(self.user)
		data['refresh'] = str(refresh)
		data['access'] = str(refresh.access_token)

		# Add custom claims
		data['username'] = self.user.username
		data['bio'] = self.user.bio
		return data
		
	@classmethod
	def get_token(cls, user):
		token = super(MyTokenObtainPairSerializer, cls).get_token(user)

		# Add custom claims
		token['username'] = user.username
		token['bio'] = user.bio
		return token


class UserMediaSerializer(serializers.ModelSerializer):
	thumb = HyperlinkedSorlImageField(
		'40x40',
		options={"crop": "center"},
		source='image',
		read_only=True
	)
	# A larger version of the image, allows writing
	image = HyperlinkedSorlImageField('1024')
	src=serializers.SerializerMethodField(read_only=True)
	
	class Meta:
		model = User
		fields = [ "thumb", "image", "src"]
	
	def get_src(self, obj):
		src = get_thumbnail(obj.image, '200x200', crop='center', quality=99).url
		return src

class UserDisplaySerializer(serializers.ModelSerializer):
	rel_to_set = serializers.StringRelatedField(
				many=True,
				read_only=True
				)
	rel_from_set = serializers.StringRelatedField(
				many=True,
				read_only=True
				)

	followers_count = serializers.SerializerMethodField()
	thumbnail = HyperlinkedSorlImageField(
		'40x40',
		options={"crop": "center"},
		source='image',
		read_only=True
	)
	middle_thumbnail = HyperlinkedSorlImageField(
		'75x75',
		options={"crop": "center"},
		source='image',
		read_only=True
	)
	big_thumbnail = HyperlinkedSorlImageField(
		'800x600',
		options={"crop": "center"},
		source='image',
		read_only=True
	)

	# A larger version of the image, allows writing
	image = HyperlinkedSorlImageField('1024')
	
	class Meta:
		model = User
		fields = [ "username", "big_thumbnail","middle_thumbnail", "thumbnail","image", "rel_to_set", "rel_from_set", "followers_count"]

	def get_followers_count(self, instance):
		followers_count = instance.rel_to_set.count()
		return followers_count



class UserListSerializer(serializers.ModelSerializer):
 
 
	thumbnail = HyperlinkedSorlImageField(
		'40x40',
		options={"crop": "center"},
		source='image',
		read_only=True
	)
	middle_thumbnail = HyperlinkedSorlImageField(
		'75x75',
		options={"crop": "center"},
		source='image',
		read_only=True
	)
	big_thumbnail = HyperlinkedSorlImageField(
		'120x120',
		options={"crop": "center"},
		source='image',
		read_only=True
	)
	 
	# A larger version of the image, allows writing
	image = HyperlinkedSorlImageField('1024')
	
	class Meta:
		model = User
		fields="__all__"
 
class UserSerializer(serializers.ModelSerializer):
	followers = ContactsSerializer(many=True, read_only=True)
	thumbnail = HyperlinkedSorlImageField(
		'40x40',
		options={"crop": "center"},
		source='image',
		read_only=True
	)

	big_thumbnail = HyperlinkedSorlImageField(
		'120x120',
		options={"crop": "center"},
		source='image',
		read_only=True
	)

	# A larger version of the image, allows writing
	image = HyperlinkedSorlImageField('1024')

	class Meta:
		model = User
		fields = 'id', 'username', 'email', 'first_name', 'last_name', 'is_active','image','thumbnail','big_thumbnail', 'date_joined', 'last_login', 'followers'




class UserInformationSerializer(serializers.ModelSerializer):
	rel_to_set = serializers.StringRelatedField(
				many=True,
				read_only=True
				)
	rel_from_set = serializers.StringRelatedField(
				many=True,
				read_only=True
				)

	followers_count = serializers.SerializerMethodField()
	thumbnail = HyperlinkedSorlImageField(
		'40x40',
		options={"crop": "center"},
		source='image',
		read_only=True
	)

	big_thumbnail = HyperlinkedSorlImageField(
		'120x120',
		options={"crop": "center"},
		source='image',
		read_only=True
	)
	city = serializers.StringRelatedField(read_only=True)
	# A larger version of the image, allows writing

	class Meta:
		model = User
		fields = [
						'first_name', 'last_name', 'thumbnail', 
						'big_thumbnail', 'bio', 'phone', 'city', 'state', 
						'country',  'email', 'username',
						'rel_to_set', 'rel_from_set', 'followers_count', 
						'hobbies', 'fav_tv', 'fav_books', 
						'fav_movies', 'fav_music', 'fav_routes', 
						'fav_writers', 'fav_games']

	def get_followers_count(self, instance):
		return instance.rel_to_set.count()

class PasswordResetSerializer(serializers.Serializer):
	email = serializers.EmailField()
	password_reset_form_class = PasswordResetForm
	def validate_email(self, value):
		self.reset_form = self.password_reset_form_class(data=self.initial_data)
		if not self.reset_form.is_valid():
			raise serializers.ValidationError(_('Error'))

		###### FILTER YOUR USER MODEL ######
		if not User.objects.filter(email=value).exists():

			raise serializers.ValidationError(_('Invalid e-mail address'))
		return value

	def save(self):
		request = self.context.get('request')
		opts = {
			'use_https': request.is_secure(),
			'from_email': getattr(settings, 'DEFAULT_FROM_EMAIL'),

			###### USE YOUR TEXT FILE ######
			'email_template_name': 'account/example_message.txt',

			'request': request,
		}
		self.reset_form.save(**opts)

# class UserProfileSerializer(serializers.ModelSerializer):
#     username = serializers.CharField(source='user.username', read_only=True)
#     email = serializers.EmailField(source='user.email', read_only=True)

#     class Meta:
#         model=User
#         fields = (phone, username, email)


