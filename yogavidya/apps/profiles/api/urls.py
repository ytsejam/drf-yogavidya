from django.urls import path
from yogavidya.apps.profiles.api import views as prov


urlpatterns = [
	path("user/", prov.CurrentUserAPIView.as_view(), name="current-user"),
	path("user/information/", prov.UserInformationAPIView.as_view(), name="current-user"),
	path("members/", prov.MemberListView.as_view(), name="members"),
	path("teachers/", prov.TeacherListView.as_view(), name="teachers"), 
	path("teachers/<slug:slug>/", prov.TeacherProfileView.as_view(), name="teachers") 
]