from django.utils import timezone
from datetime import timedelta
from django.dispatch import receiver
from django.http import HttpResponseRedirect
from django.utils.translation import gettext as _
from django.contrib.auth import get_user_model
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
from django.views.generic import ListView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import filters, generics, parsers, renderers, status,  viewsets
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404
from rest_auth.registration.views import SocialConnectView, SocialLoginView
from rest_framework_simplejwt.views import TokenObtainPairView
from rest_auth.registration.views import RegisterView

from rest_framework.decorators import api_view
from rest_framework.exceptions import NotFound
from allauth.account.models import EmailConfirmation, EmailConfirmationHMAC
from allauth.socialaccount.models import SocialLogin
from allauth.socialaccount.providers.base import AuthAction
from allauth.socialaccount.providers.github.views import GitHubOAuth2Adapter
from allauth.socialaccount.providers.oauth2.client import OAuth2Client
from allauth.socialaccount.providers.oauth2.views import OAuth2LoginView


from django_rest_passwordreset.signals import reset_password_token_created
#from vuedj.constants import site_url, site_full_name, site_shortcut_name
from django_rest_passwordreset.models import ResetPasswordToken
from django_rest_passwordreset.views import get_password_reset_token_expiry_time

from yogavidya.apps.profiles.api.permissions import IsAuthorOrReadOnly
from yogavidya.apps.profiles.api.serializers import MyTokenObtainPairSerializer, UserSerializer
from yogavidya.apps.profiles.api.serializers import CustomTokenSerializer, UserDisplaySerializer, CallbackSerializer, UserInformationSerializer, UserMediaSerializer,  ContactsSerializer, UserListSerializer
from yogavidya.apps.profiles.models import User, Contact


class CustomPasswordResetView:
		@receiver(reset_password_token_created)
		def password_reset_token_created(sender, reset_password_token, *args, **kwargs):
				"""
								Handles password reset tokens
								When a token is created, an e-mail needs to be sent to the user
				"""
				# send an e-mail to the user
				context = {
						'current_user': reset_password_token.user,
						'username': reset_password_token.user.username,
						'email': reset_password_token.user.email,
						'reset_password_url': "{}/password-reset/{}".format('http://localhost:8000/http://localhost:8080', reset_password_token.key),
						'site_name': site_shortcut_name,
						'site_domain': site_url
				}

				# render email text
				email_html_message = render_to_string(
						'email/user_reset_password.html', context)
				email_plaintext_message = render_to_string(
						'email/user_reset_password.txt', context)

				msg = EmailMultiAlternatives(
						# title:
						"Password Reset for {}".format(site_full_name),
						# message:
						email_plaintext_message,
						# from:
						"noreply@{}".format(site_url),
						# to:
						[reset_password_token.user.email]
				)
				msg.attach_alternative(email_html_message, "text/html")
				msg.send()


class CustomPasswordTokenVerificationView(APIView):
		"""
						An Api View which provides a method to verifiy that a given pw-reset token is valid before actually confirming the
						reset.
		"""
		throttle_classes = ()
		permission_classes = ()
		parser_classes = (parsers.FormParser,
											parsers.MultiPartParser, parsers.JSONParser,)
		renderer_classes = (renderers.JSONRenderer,)
		serializer_class = CustomTokenSerializer

		def post(self, request, *args, **kwargs):
				serializer = self.serializer_class(data=request.data)
				serializer.is_valid(raise_exception=True)
				token = serializer.validated_data['token']

				# get token validation time
				password_reset_token_validation_time = get_password_reset_token_expiry_time()

				# find token
				reset_password_token = ResetPasswordToken.objects.filter(
						key=token).first()

				if reset_password_token is None:
						return Response({'status': 'invalid'}, status=status.HTTP_404_NOT_FOUND)

				# check expiry date
				expiry_date = reset_password_token.created_at + \
						timedelta(hours=password_reset_token_validation_time)

				if timezone.now() > expiry_date:
						# delete expired token
						reset_password_token.delete()
						return Response({'status': 'expired'}, status=status.HTTP_404_NOT_FOUND)

				# check if user has password to change
				if not reset_password_token.user.has_usable_password():
						return Response({'status': 'irrelevant'})

				return Response({'status': 'OK'})


class CallbackMixin:
		adapter_class = GitHubOAuth2Adapter
		client_class = OAuth2Client
		# This is our serializer from above
		# You can omit this if you handle CSRF protection in the frontend
		serializer_class = CallbackSerializer

		# Not the prettiest but single source of truth
		@property
		def callback_url(self):
				url = self.adapter_class(self.request).get_callback_url(
						self.request,
						None,
				)
				return url


class CallbackCreate(CallbackMixin, SocialLoginView):
		"""
		Logs the user in with the providers data.
		Creates a new user account if it doesn't exist yet.
		"""


class CallbackConnect(CallbackMixin, SocialConnectView):
		"""
		Connects a provider's user account to the currently logged in user.
		"""

		# You can override this method here if you don't want to
		# receive a token. Omit it otherwise.
		def get_response(self):
				return Response({'detail': _('Connection completed.')})


class Login(APIView):
		adapter_class = GitHubOAuth2Adapter
		permission_classes = (AllowAny,)

		def post(self, request, format=None):
				"""
				Returns the URL to the login page of provider's authentication server.
				"""
				# You should have CSRF protection enabled, see
				# https://security.stackexchange.com/a/104390 (point 3).
				# Therefore this is a POST endpoint.
				# This code is inspired by `OAuth2LoginView.dispatch`.
				adapter = self.adapter_class(request)
				provider = adapter.get_provider()
				app = provider.get_app(request)
				view = OAuth2LoginView()
				view.request = request
				view.adapter = adapter
				client = view.get_client(request, app)
				# You can modify `action` if you have more steps in your auth flow
				action = AuthAction.AUTHENTICATE
				auth_params = provider.get_auth_params(request, action)
				# You can omit this if you want to validate the state in the frontend
				client.state = SocialLogin.stash_state(request)
				url = client.get_redirect_url(adapter.authorize_url, auth_params)
				return Response({'url': url})


class CurrentUserAPIView(APIView):
		permission_classes = (IsAuthenticated,)

		def get(self, request):
				serializer = UserSerializer(request.user)
				return Response(serializer.data)


class UserInformationAPIView(APIView):
		permission_classes = (IsAuthenticated,)

		def get(self, request):
				serializer = UserSerializer(request.user)
				return Response(serializer.data)

class MemberListView(APIView):

		queryset = User.objects.filter(is_active=1)

		def get(self, request, format=None):
				users = User.objects.filter(is_active=1)
				serializer = UserDisplaySerializer(users)
				return Response(serializer.data)


class TeacherListView(APIView):

		queryset = User.objects.all()
		# the many param informs the serializer that it will be serializing more than a single article.
		serializer_class = UserListSerializer
		lookup_field = "username"

		def get(self, request=None):
				users = User.objects.filter(is_teacher=True)
				serializer = UserDisplaySerializer(users, many=True)
				return Response(serializer.data)


class TeacherProfileView(APIView):

		queryset = User.objects.filter(is_active=1)
		# the many param informs the serializer that it will be serializing more than a single article.
		serializer_class = UserInformationSerializer
		lookup_field = "username"

		def get(self, request, format=None, slug=None):
				kwarg_username = self.kwargs.get("slug")
				user = User.objects.get(username=kwarg_username)
				print(user)
				serializer = UserInformationSerializer(user)
				print(serializer.data)
				return Response(serializer.data)


class MemberMediaView(APIView):

		queryset = User.objects.filter(is_active=1)
		# the many param informs the serializer that it will be serializing more than a single article.
		serializer_class = UserMediaSerializer
		lookup_field = "username"

		def get(self, request, format=None, slug=None):
				kwarg_username = self.kwargs.get("slug")
				user = User.objects.filter(is_active=1).filter(username=kwarg_username)
				print(user)
				serializer = UserMediaSerializer(user)
				return Response(serializer.data)


class FollowerListView(APIView):
		queryset = User.objects.all()
		# the many param informs the serializer that it will be serializing more than a single article.
		serializer_class = UserInformationSerializer
		lookup_field = "username"

		def get(self, request=None, format=None, slug=None):
				kwarg_username = self.kwargs.get("slug")
				user = get_object_or_404(User, username=kwarg_username)

				contacts = user.rel_to_set.all()
				user.followers = contacts

				serializer = UserInformationSerializer(user)
				return Response(serializer_data)


class ConfirmEmailView(APIView):
		permission_classes = [AllowAny]

		def get(self, *args, **kwargs):
				self.object = confirmation = self.get_object()
				confirmation.confirm(self.request)
				# A React Router Route will handle the failure scenario
				return HttpResponseRedirect('/api/rest-auth/login/')

		def get_object(self, queryset=None):
				key = self.kwargs['key']
				email_confirmation = EmailConfirmationHMAC.from_key(key)
				if not email_confirmation:
						if queryset is None:
								queryset = self.get_queryset()
						try:
								email_confirmation = queryset.get(key=key.lower())
						except EmailConfirmation.DoesNotExist:
								# A React Router Route will handle the failure scenario
								return HttpResponseRedirect('/login/failure/')
				return email_confirmation

		def get_queryset(self):
				qs = EmailConfirmation.objects.all_valid()
				qs = qs.select_related("email_address__user")
				return qs
