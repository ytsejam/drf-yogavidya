# -*- coding: utf-8 -*-
import datetime
from django.conf import settings
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import authenticate, login

from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.utils.timezone import now

from django.contrib.auth import logout as django_logout
from django.contrib.auth.forms import (
    AuthenticationForm,
    PasswordChangeForm,
    PasswordResetForm,
    SetPasswordForm,
)

from django.contrib import messages
#from django.contrib.auth.models import User



from ..decorators import student_required
from django.shortcuts import redirect, render
from django.views.generic import TemplateView
from django.contrib.auth.views import(
    LogoutView,
 
)

from yogavidya.apps.profiles.filters import UserFilter


from django.shortcuts import get_object_or_404

from yogavidya.apps.actions.utils import create_action






def home(request):
    if request.user.is_authenticated:
        if request.user.is_teacher:
            return redirect('teachers:quiz_change_list')
        else:
            return redirect('students:quiz_list')
    return render(request, 'classroom/home.html')



def signup(request):
  if request.method == 'POST':
    user_form = UserRegistrationForm(request.POST)
    if user_form.is_valid():
        cd = form.cleaned_data
        if cd['password'] != cd['password2']:
            message.error(request,'Passwords dont match')
        else:
            #Creata a new user object but avoid saving it yet
            new_user = user_form.save(commit=False)
            #Save the chosen password_reset
            new_user.set_password(user_form.cd['password'])
            #Save the User object
            new_user.save()
            profile = Profile.objects.create(user=new_user)
            return render(request, 'accounts/register_done.html',{'new_user': new_user})
    else:
        user_form = UserRegistrationForm({'user_form': user_form})



  return render(request, "accounts/login-signup.html", {'today': today, 'now': now()})






@login_required
def payments(request):
    pass
