# -*- coding: utf-8 -*-
#from datetime import datetime
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.views.generic import (
							CreateView,
							ListView,
							UpdateView,
							DetailView,
							TemplateView,
							View)
from django.urls import reverse, reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.timezone import datetime
from django.views.decorators.http import require_POST


from yogavidya.decorators import ajax_required

from yogavidya.apps.actions.models import Action
from yogavidya.apps.articles.models import Article
from yogavidya.apps.profiles.models import Contact
from yogavidya.apps.profiles.models import User
from yogavidya.apps.locations.models import City, Country, State
from yogavidya.apps.statusses.models import Status
from yogavidya.apps.videolessons.models import Videolesson

from photologue.models import Gallery, Photo
from yogavidya.apps.profiles.filters import UserFilter
from yogavidya.apps.galleries.forms import GalleryModelForm, PhotoModelForm 
from yogavidya.apps.profiles.forms import (
										LoginForm,
										StudentSignUpForm,
										UserCreationForm,
										UserProfileForm,
										UserPhotoForm,
										UserPhotoEditForm)
from yogavidya.apps.statusses.forms import StatusModelForm, StatusModelImageForm
from yogavidya.apps.actions.utils import create_action

from django.http import HttpResponse, JsonResponse
def search(request):
	user_list = User.objects.all()
	user_filter = UserFilter(request.GET, queryset=user_list)
	return render(request, 'classroom//teachers/teachers_list.html', {'filter': user_filter})


@login_required
def profile_index(request):
	today = datetime.today()
	now = datetime.now()
	index_article_list = Article.objects.filter(index_page=True).order_by("created_at")
	index_videolesson_list = Videolesson.objects.filter(index_page=True).order_by("created_at")
	chakra_videolesson_list = Videolesson.objects.filter(chakra_page=True).order_by("created_at")
	last_articles = Article.objects.filter(index_page=True).order_by("created_at")[:2]
	last_videolessons = Videolesson.objects.filter(index_page=True).order_by("created_at")[:2]
	
	birthdays = User.objects.filter(birth_date__day=today.day, 
						birth_date__month=today.month)
	actions = Action.objects.order_by("-created_at").exclude(user=request.user)[:5]
	statusses = Status.objects.order_by("-created_at")[:4]
	following_ids = request.user.following.values_list('id', flat=True)
	if following_ids:
		actions = Action.objects.order_by("-created_at").exclude(user=request.user)
		actions = actions.filter(user_id__in=following_ids).select_related('user', 'user__student').prefetch_related('target')
	actions= actions[:10]
	media_form = StatusModelImageForm()

	context =  {
						'today': today,
						'now': now,
						'media_form': media_form,
						'sectiontitle': 'YogaVidya',
						'actions': actions,
						'statusses': statusses,
						'followeds': following_ids,
						'birthdays': birthdays,
						'index_article_list': index_article_list,
						'index_videolesson_list': index_videolesson_list,
						'last_videolessons': last_videolessons,
						'chakra_videolesson_list': chakra_videolesson_list,
						'last_articles': last_articles,
	}
	if request.method == 'POST':
		form = StatusModelForm(request.POST or None)
		media_form = StatusModelImageForm(request.POST or request.FILES or None)
		if form.is_valid():
				
				instance = form.save(commit = False)
				print(instance)
				instance.user = request.user

				instance.save()
				create_action(request.user,_('created_status'), instance)
				return HttpResponseRedirect(reverse('profiles:profile_index'))
		else:
				form = StatusModelForm()
		return HttpResponseRedirect(reverse('profiles:profile_index'))

		if media_form.is_valid():
				
				instance = form.save(commit = False)
				print(instance)
				instance.user = request.user

				instance.save()
				create_action(request.user,_('created_status'), instance)
				return HttpResponseRedirect(reverse('profiles:profile_index'))
		else:
				form = StatusModelForm()
		return HttpResponseRedirect(reverse('profiles:profile_index'))


	return render(request, "yogavidya/newsfeed.html", context)

class SignUpView(CreateView):
	model = settings.AUTH_USER_MODEL
	form_class = StudentSignUpForm
	template_name = 'registration/signup_form.html'

	def get_context_data(self, **kwargs):
		kwargs['user_type'] = 'student'
		return super().get_context_data(**kwargs)

	def form_valid(self, form):
		user = form.save()
		login(self.request, user)
		return redirect('home')

class LogoutView(View):
	def get(self, request):
		logout(request)
		return HttpResponseRedirect(settings.LOGIN_URL)

@method_decorator([login_required], name='dispatch')
class UserMembersListView(ListView):

	model = settings.AUTH_USER_MODEL
	ordering = ('username', )
	context_object_name = 'members'
	template_name = 'classroom/accounts/members.html'

	def get_queryset(self):
		return User.objects.filter(is_active=1).exclude(username=self.request.user.username)


@method_decorator([login_required], name='dispatch')
class MemberDetailView(DetailView):
	model = User
	template_name = 'classroom/accounts/member_detail.html'
	slug_field = "username"
 

@method_decorator([login_required], name='dispatch')
class UserProfileUpdateView(UpdateView):

	model = settings.AUTH_USER_MODEL
	form_class = UserProfileForm
	template_name = 'classroom/profile_form.html'
	success_url = reverse_lazy('home')

	def get_object(self):
		return self.request.user.is_student

	def form_valid(self, form):
		messages.success(self.request, 'Profile updated with success!')
		return super().form_valid(form)

@method_decorator([login_required], name='dispatch')
class UserProfilePageAboutView(DetailView):
	
	model = settings.AUTH_USER_MODEL
	template_name = 'classroom/accounts/about.html'

	def get_object(self):
		return self.request.user



@method_decorator([login_required], name='dispatch')
class UserProfilePagePhotosView(UpdateView):
	
	model = settings.AUTH_USER_MODEL
	form_class = GalleryModelForm
	template_name = 'galleries/photos-upload.html'
	success_url = reverse_lazy('profiles:photos')

	def get_object(self):
		return self.request.user
		
	def form_valid(self, form):
		
		form.save(commit=True)
		messages.success(self.request, 'Gallery created with success!')
		return super().form_valid(form)


@login_required
def user_gallery_add(request):
	gallery_form = GalleryModelForm(request.POST or None)
	photos_form = PhotoModelForm(request.POST or None)
	photos = Photo.objects.all()
	user=request.user
	print(user)
	if request.method == "POST":
		form = GalleryModelForm(request.POST or None, request.FILES or None)
		form = PhotoModelForm(request.POST or None, request.FILES or None)
		if form.is_valid():
			instance = form.save(commit=False)
			instance.slug = slugify(instance.title)
			instance.user = user
			instance.save()
			create_action(request.user,_('gallery_added'), instance)
			messages.success(request, "Succesfully Created")
			return JsonResponse({'status': 'ok'})
		else:
			messages.error(request, "Gallery Cant be Added")
			form = GalleryModelForm(data=request.GET)
			return JsonResponse({'status': 'ko'})
	context = {
		"gallery_form": gallery_form,
		"photos_form": photos_form,
		"photos": photos
	}
	return render(request, "classroom/accounts/photos-upload.html", context)

@method_decorator([login_required], name='dispatch')
class ProfileUpdateView(UpdateView):
	model = settings.AUTH_USER_MODEL
	form_class = UserProfileForm
	template_name = 'classroom/accounts/profile_settings.html'
	success_url = reverse_lazy('profiles:profile_settings')

	def get_object(self):
		return self.request.user


	def form_valid(self, form):
		instance = form.save(commit=False)
		print(instance)
		messages.success(self.request, 'Profile updated with success!')
		return super().form_valid(form)

@method_decorator([login_required], name='dispatch')
class HobbiesUpdateView(UpdateView):
	model = settings.AUTH_USER_MODEL
	form_class = UserProfileForm
	template_name = 'classroom/accounts/hobbies_settings.html'
	success_url = reverse_lazy('profiles:hobbies_settings')
	

	def get_object(self):
		return self.request.user


	def form_valid(self, form):
		messages.success(self.request, 'Profile updated with success!')
		return super().form_valid(form)


@method_decorator([login_required], name='dispatch')
class PhotoUpdateView(UpdateView):
	model = settings.AUTH_USER_MODEL
	form_class = UserPhotoForm
	template_name = 'classroom/accounts/profile_photo.html'
	success_url = reverse_lazy('profiles:profile_settings')
	

	def get_object(self):
		return self.request.user


	def form_valid(self, form):
		
		form.save(commit=True)
		messages.success(self.request, 'Profile photo updated with success!')
		return super().form_valid(form)

def signup(request):
	if request.method == 'POST':
		user_form = UserRegistrationForm(request.POST)
		if user_form.is_valid():
			cd = form.cleaned_data
			if cd['password'] != cd['password2']:
				message.error(request,'Passwords dont match')
			else:
				#Creata a new user object but avoid saving it yet
				new_user = user_form.save(commit=False)
				#Save the chosen password_reset
				new_user.set_password(user_form.cd['password'])
				#Save the User object
				new_user.save()
				profile = Profile.objects.create(user=new_user)
				return render(request, 'accounts/register_done.html',{'new_user': new_user})
	else:
		user_form = UserRegistrationForm({'user_form': user_form})

	return render(request, "accounts/login-signup.html", {'today': today, 'now': now()})

def user_login(request):
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			print(cd)
			user = authenticate(username=cd['username'], password=cd['password'])
			if user is not None:
				if user.is_active:
					login(request, user)
					return redirect('home')
				else:
					messages.error(request,'username or password not correct')
					return HttpResponse('Disabled account')
			else:
				messages.error(request,'There is no user like this or not activated')
				return redirect('profiles:user_login')
		else:
			messages.error(request,'username or password not correct')
			return redirect('profiles:user_login')

	else:
		form = LoginForm()
	return render(request, 'accounts/login-signup.html', {'form': form})

def logout_then_login(request):
	if request.method == 'POST':
		form = LoginForm(request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			user = authenticate(username=cd['username'], password=cd['password'])
			if user is not None:
				if user.is_active:
					login(request, user)
					return HttpResponse('Authenticate Succesfully')
				else:
					return HttpResponse('Disabled account')
	else:
		form = LoginForm()
	return render(request, 'accounts/login_signup.html', {'form': form})

@login_required
def upload_picture(request):

	try:
		user = request.user
	except User.DoesNotExist:
		return HttpResponse("invalid user_profile!")
	print(user.username)
	if request.method == 'POST':
		form = UserPhotoForm(request.POST or None, request.FILES or None,  instance=user)
		if form.is_valid():
			pic = form.cleaned_data['image']
			instance = form.save(commit = False)
			# [...] Process whatever you do with that file there. I resize it, create thumbnails, etc.
			# Get an instance of picture model (defined below) 
			# picture = ...         
			instance.image = pic
			form.save()
			return HttpResponseRedirect(reverse('profiles:profile_settings'))
	return HttpResponseRedirect(reverse('profiles:profile_photo_update'))
@login_required
def update_profile(request):

	try:
		user = request.user
	except User.DoesNotExist:
		return HttpResponse("invalid user_profile!")
	if request.method == 'POST':
		form = UserProfileForm(request.POST or None, instance=user)
		print(form)
		if form.is_valid():
			 
			instance = form.save(commit = False)
			print(instance)
			# [...] Process whatever you do with that file there. I resize it, create thumbnails, etc.
			# Get an instance of picture model (defined below) 
			# picture = ...         
			instance.save()
			form.save()
			return HttpResponseRedirect(reverse('profiles:profile_settings'))
		else:
			form = UserProfileForm()
	return HttpResponseRedirect(reverse('profiles:profile_settings'))




@login_required
def upload_picturee(request, uid=None):
	"""
	Photo upload / dropzone handler
	:param request:
	:param uid: Optional picture UID when re-uploading a file.
	:return:
	"""
	#print(request.user.username)
	#form = UserPhotoForm(request.POST, request.FILES or None)
	#if form.is_valid():
	if request.method == 'POST' :
		current_user=request.user
		print(current_user.username)
		pic = request.FILES['image']
		print(pic)
		# [...] Process whatever you do with that file there. I resize it, create thumbnails, etc.
		# Get an instance of picture model (defined below) 
		# picture = ...         
		current_user.image = pic
		#user.save()
	 
		current_user.save()
		return reverse_lazy('profiles:profile_photo_update')
	return reverse_lazy('profiles:profile_settings')

@login_required
def profile_update(request):
 

  if (request.method == "POST" or None) :
	  instance = request.user
	  instance.first_name = request.POST.get("first_name")
	  instance.last_name = request.POST.get("last_name")
	  instance.city = City.objects.get(pk=request.POST.get("city"))
	  instance.country = Country.objects.get(pk=request.POST.get("country"))
	  #instance.state = State.objects.get(pk=request.POST.get("state"))
	  instance.phone = request.POST.get("phone")
	  instance.gender = request.POST.get("gender")
	  print(bio)
	  instance.bio = request.POST.get("bio")
	  d = datetime.datetime.strptime(request.POST.get("datetimepicker"), '%d/%m/%Y')
	  d.strftime('%Y/%m/%d')
	  instance.birth_date = d
	  instance.image = request.FILES.get("image")

	  instance.save()
	  messages.success(request, "Succesfully Created")
	  return HttpResponseRedirect(instance.get_absolute_url())
  else:
	  messages.error(request, "Profile Can not be Updated!!")
  context = {
	  "title": 'Profile'
	}
  return render(request, "articles/article_form.html", context)
 

@login_required
def photo_list(request):
	user = request.user
	if request.method == 'POST':
		form = UserPhotoEditForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			return redirect('profiles:profile_photo_update')
	else:
		form = UserPhotoeditForm()
	return render(request, 'profiles/classroom/photo_edit.html', {'form': form, 'user': user})
@method_decorator([login_required], name='dispatch')
class PhotoAltUpdateView(UpdateView):
	model = settings.AUTH_USER_MODEL
	form_class = UserProfileForm
	template_name = 'classroom/accounts/profile.html'
	success_url = reverse_lazy('profiles:profile_settings')
	

	def get_object(self):
		return self.request.user


	def form_valid(self, form):
		messages.success(self.request, 'Profile updated with success!')
		return super().form_valid(form)

@login_required
def about(request):
		user = request.user
		context = {
			'section': 'people',
			'user': user,
		}
		return render(request, 'accounts/user_detail.html', context)
@login_required
def timeline(request):
		user = request.user
		context = {
			'section': 'people',
			'user': user,
		}
		return render(request, 'accounts/user_newsfeed.html', context)

def user_register(request):
	if request.method == 'POST':
		form = UserCreationForm(request.POST)
		if form.is_valid():
			if form.is_valid():
				form.save()
				username = form.cleaned_data.get('username')
				raw_password = form.cleaned_data.get('password1')
				user = authenticate(username=username, password=raw_password)
				login(request, user)
				return redirect('home')
	else:
		form = UserCreationForm()
	return render(request, 'accounts/registration.html', {'form': form})


@login_required
def dashboard(request):
  today = datetime.date.today()
  #actions = Action.objects.all().exclude(user=request.user)
  following_ids = request.user.following.values_lsit('id', flat=True)
  #if following_ids:
	#actions = actions.filter(user_id__in=following_ids).select_related('user', 'user__profile').prefetch_related('target')
	#actions = actions[:10]
  context = {
		'today':today,
		'now':now(),
		'section':'dashboard',
		#'actions':actions
  }
  return render(request, "accounts/dashboard.html", context)
# @login_required
# def logout(request):
#     django_logout(request)
#     return redirect('home')
@login_required
def password_change(request):
	pass

@login_required
def account_settings(request):
	user = request.user
	context = {
		'user':user
	}
	return render(request, 'accounts/account_settings.html', context)
@login_required
def personal_information(request):
	user = request.user

	if request.method=='POST':
		user=request.user
		print(user)
		print(request.POST['first_name'])
		user.first_name=request.POST['first_name']
		user.last_name=request.POST['last_name']
		user.profile.bio=request.POST['bio']
		user.profile.city=request.POST['city']
		user.profile.country=request.POST['country']
		user.profile.phone=request.POST['phone']
		user.save()
	context = {
		'user':user
	}
	return render(request, 'accounts/personal_information.html', context)
@login_required
def user_list(request):
	users = User.objects.filter(is_active=True)
	context = {
		'section': 'people',
		'users': users,
	}
	return render(request, 'accounts/user_list.html', context)




@login_required
def user_detail(request):
	users = get_object_or_404(User, username=username, is_active= True)
	context = {
		'section': 'people',
		'user': user,
	}
	return render(request, 'accounts/user_detail.html', context)

@ajax_required
@require_POST
@login_required
def user_follow(request):
	user_id = request.POST.get('id')
	action = request.POST.get('action')
	if user_id and action:
		try:
			user = User.objects.get(id=user_id)
			if action == 'follow':
				Contact.objects.get_or_create(user_from=request.user,
											  user_to=user)
				create_action(request.user, 'is following', user)
			else:
				Contact.objects.filter(user_from=request.user,
									   user_to=user).delete()
			return JsonResponse({'status' : 'ok'})
		except User.DoesNotExist:
			return JsonResponse({'status' : 'ko'})
	return JsonResponse({'status': 'ko'})