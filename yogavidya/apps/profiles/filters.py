from django.conf import settings
import django_filters
from .models import User
class UserFilter(django_filters.FilterSet):

    class Meta:
        model = User
        fields = [ 'is_teacher', 'username', 'city']