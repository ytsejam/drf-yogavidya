from django_registration.forms import RegistrationForm
from django import forms
from django.conf import settings
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction
from django.forms.utils import ValidationError
from django.forms.widgets import Select
from yogavidya.apps.profiles.models import  Student, Question, Answer, StudentAnswer
from yogavidya.apps.locations.models import City, Country, State
from PIL import Image
from django.core.files import File
from django.forms.widgets import TextInput
from django.contrib.auth import authenticate
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.utils.translation import ugettext_lazy as _
from django.template import loader
from django.utils.encoding import force_bytes
from yogavidya.apps.profiles.models import User
class TeacherSignUpForm(UserCreationForm):
	class Meta(UserCreationForm.Meta):
		model = User

	def save(self, commit=True):
		user = super().save(commit=False)
		user.is_teacher = True
		if commit:
			user.save()
		return user

	

class StudentSignUpForm(UserCreationForm):
	 #interests = forms.ModelMultipleChoiceField(
	#    queryset=Subject.objects.all(),
	#    widget=forms.CheckboxSelectMultiple,
	#    required=True
	#)
	email = forms.EmailField(required=True, label='Email')
	
	def __init__(self, *args, **kwargs):
				super(StudentSignUpForm, self).__init__(*args, **kwargs)

				for fieldname in ['username', 'password1', 'password2']:
						self.fields[fieldname].help_text = None


	class Meta(UserCreationForm.Meta):
		model = User
		fields = ('email','username', 'password1', 'password2')

	@transaction.atomic
	def save(self):
		user = super().save(commit=False)
		user.is_student = True
		user.email = self.cleaned_data.get('email')
		user.username = self.cleaned_data.get('username')
		user.save()
		student = Student.objects.create(user=user)
		#student.interests.add(*self.cleaned_data.get('interests'))
		return user



class UserPhotoForm(forms.ModelForm):
 
	class Meta:
		model = User
		fields = ['image']
		
class UserPhotoEditForm(forms.ModelForm):
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())

	class Meta:
		model = User
		fields = ['image', 'x', 'y', 'width', 'height',]

	def save(self):
		image = super(UserPhotoEditForm, self).save()

		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(image.file)
		cropped_image = image.crop((x, y, w+x, h+y))
		resized_image = cropped_image.resize((200, 200), Image.ANTIALIAS)
		resized_image.save(image.file.path)

		return image

class UserProfileForm(forms.ModelForm):
	#state = forms.ModelChoiceField(queryset=State.objects.all(),)
	birth_date = forms.DateField(input_formats=['%d/%m/%Y'])
	widget=forms.DateInput(format='%d/%m/%Y',)

	class Meta:
		model = User
		fields = (
					'bio',
					'city',
					'country',
					'first_name',
					'last_name',
					'location',
					'phone',
					'gender',
					'birth_date',
					'terms',
					'image',
					'height_field',
					'width_field'
				 )
		
	def __init__(self, *args, **kwargs):
		super(UserProfileForm, self).__init__(*args, **kwargs)
		self.fields['country'].queryset = Country.objects.all()
		#self.fields['state'].queryset = State.objects.all()
		#self.fields['state'] = State(queryset=State.objects.all(),widget=forms.Select(attrs={'class': ''}),)
		self.fields['city'].queryset = City.objects.all()
		self.fields['birth_date'].widget.attrs.update({'class': 'datepicker form-control'})


def load_cities(request):
    country_id = request.GET.get('country')
    cities = City.objects.filter(country_id=country_id).order_by('name')
    return render(request, 'hr/city_dropdown_list_options.html', {'cities': cities})
    
class BiggerUserProfileForm(forms.ModelForm): 

	#state = forms.ModelChoiceField(queryset=State.objects.all(),)
	birth_date = forms.DateField(input_formats=['%d/%m/%Y'])
	widget=forms.DateInput(format='%d/%m/%Y',)
	x = forms.FloatField(widget=forms.HiddenInput())
	y = forms.FloatField(widget=forms.HiddenInput())
	width = forms.FloatField(widget=forms.HiddenInput())
	height = forms.FloatField(widget=forms.HiddenInput())
	class Meta:
		model = User
		fields = (
					'bio',
					'city',
					'country',
					'first_name',
					'last_name',
					'location',
					'phone',
					'gender',
					'birth_date',
					'terms',
					'image',
					'hobbies',
					'fav_music',
					'fav_tv',
					'fav_books',
					'fav_movies',
					'fav_routes',
					'x', 'y', 'width', 'height',
				 )
		
	def __init__(self, *args, **kwargs):
		super(UserProfileForm, self).__init__(*args, **kwargs)
		self.fields['country'].queryset = Country.objects.all()
		#self.fields['state'].queryset = State.objects.all()
		#self.fields['state'] = State(queryset=State.objects.all(),widget=forms.Select(attrs={'class': ''}),)
		self.fields['city'].queryset = City.objects.all()
		self.fields['birth_date'].widget.attrs.update({'class': 'datepicker form-control'})

	def clean_image(self):
		image = self.cleaned_data.get('image', False)
		if not self.instance.image == image:
			# validate image
			return None
	
	def save(self):
		image = super(UserProfileForm, self).save()

		x = self.cleaned_data.get('x')
		y = self.cleaned_data.get('y')
		w = self.cleaned_data.get('width')
		h = self.cleaned_data.get('height')

		image = Image.open(photo.file)
		cropped_image = image.crop((x, y, w+x, h+y))
		resized_image = cropped_image.resize((200, 200), Image.ANTIALIAS)
		resized_image.save(photo.file.path)

		return image


# from django.contrib.auth.forms import UserCreationForm
# from django.contrib.auth.models import User


class SignUpForm(UserCreationForm):
	first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
	last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
	email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')

	class Meta:
		model = User
		fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )
		
class LoginForm(forms.Form):
	username = forms.CharField()
	password = forms.CharField(widget=forms.PasswordInput)

class RegisterForm(forms.Form):
	pass

class PasswordResetForm(forms.Form):
    email = forms.EmailField(label=_("Email"), max_length=254)

    def save(self, domain_override=None,
             subject_template_name='registration/password_reset_subject.txt',
             email_template_name='registration/password_reset_email.html',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None):
        """
        Generates a one-use only link for resetting password and sends to the
        user.
        """
        from django.core.mail import send_mail
        UserModel = get_user_model()
        email = self.cleaned_data["email"]
        active_users = UserModel._default_manager.filter(
            email__iexact=email, is_active=True)
        for user in active_users:
            # Make sure that no email is sent to a user that actually has
            # a password marked as unusable
            if not user.has_usable_password():
                continue
            if not domain_override:
                current_site = get_current_site(request)
                site_name = current_site.name
                domain = current_site.domain
            else:
                site_name = domain = domain_override
            c = {
                'email': user.email,
                'domain': domain,
                'site_name': site_name,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'user': user,
                'token': token_generator.make_token(user),
                'protocol': 'https' if use_https else 'http',
            }
            subject = loader.render_to_string(subject_template_name, c)
            # Email subject *must not* contain newlines
            subject = ''.join(subject.splitlines())
            email = loader.render_to_string(email_template_name, c)
            send_mail(subject, email, from_email, [user.email])
class YogaUserForm(RegistrationForm):

	class Meta(RegistrationForm.Meta):
		model = User
		
class StudentInterestsForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = ('interests', )
        widgets = {
            'interests': forms.CheckboxSelectMultiple
        }


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ('text', )


class BaseAnswerInlineFormSet(forms.BaseInlineFormSet):
    def clean(self):
        super().clean()

        has_one_correct_answer = False
        for form in self.forms:
            if not form.cleaned_data.get('DELETE', False):
                if form.cleaned_data.get('is_correct', False):
                    has_one_correct_answer = True
                    break
        if not has_one_correct_answer:
            raise ValidationError('Mark at least one answer as correct.', code='no_correct_answer')


class TakeQuizForm(forms.ModelForm):
    answer = forms.ModelChoiceField(
        queryset=Answer.objects.none(),
        widget=forms.RadioSelect(),
        required=True,
        empty_label=None)

    class Meta:
        model = StudentAnswer
        fields = ('answer', )

    def __init__(self, *args, **kwargs):
        question = kwargs.pop('question')
        super().__init__(*args, **kwargs)
        self.fields['answer'].queryset = question.answers.order_by('text')
