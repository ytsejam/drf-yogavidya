from django.urls import include, path
# -*- coding: utf-8 -*-
from django.conf.urls.i18n import i18n_patterns

from django.utils.translation import gettext_lazy as _
from yogavidya.apps.profiles.views import classroom, students, teachers
from yogavidya.apps.profiles.views.classroom import(
    signup,
    dashboard,
    user_detail,
    user_follow,
    user_list,
    user_login,
    user_register,
    profile_index,
    #logout,
    logout_then_login,
    account_settings,
    personal_information,
    about

)
from yogavidya.apps.profiles.views.students import LoginView
from yogavidya.apps.galleries.views import BasicUploadView

from .views import classroom, students, teachers
app_name = 'profiles'
urlpatterns = [
    path('search/', classroom.search, name='teacher_search'),
    path('users/follow/', user_follow, name='user_follow'),
    path(_('about/'), classroom.UserProfilePageAboutView.as_view(), name='about'),
    path('detail/<str:slug>', classroom.MemberDetailView.as_view(), name='detail'),
    
    path(_('members/'), classroom.UserMembersListView.as_view(), name='members'),
    path(_('photos/'), BasicUploadView.as_view(), name='photos'),
    path(_('settings/'), classroom.UserProfileUpdateView.as_view(), name='profile_update'),
    path(_('profile_index'), classroom.profile_index, name='profile_index'),
    path(_('profile_settings'), classroom.ProfileUpdateView.as_view(), name='profile_settings'),
    path(_('hobbies_settings'), classroom.HobbiesUpdateView.as_view(), name='hobbies_settings'),
    path(_('profile_photo_update'), classroom.PhotoUpdateView.as_view(), name='profile_photo_update'),
    path('profile_photo_alt', classroom.PhotoAltUpdateView.as_view(), name='profile_photo_alt'),
    path('upload_picture', classroom.upload_picture, name='upload_picture'),
    path('update_profile', classroom.update_profile, name='update_profile'),
    path('profile_update', classroom.profile_update, name='profile_update'),
    

    
]





urlpatterns  += [
    
    path('account_settings', account_settings, name='account_settings'),
    path('personal_information', personal_information, name='personal_information'),

    path(_('signup/'), classroom.SignUpView.as_view(), name='signup'),

    path(_('student_login/'), LoginView.as_view(), name='student_login'),
    path(_('teacher_login/'), teachers.LoginView.as_view(), name='teacher_login'),
    path('logout_then_login', logout_then_login, name='logout_then_login'),
    #password related urls

    path('logout/', classroom.LogoutView.as_view(), name='logout'),
    path('users', user_list, name='user_list'),
    path('users/<slug:slug>', user_detail, name='user_detail'),
]


urlpatterns += [

    path('students/', include(([
        path('', students.QuizListView.as_view(), name='quiz_list'),
        path('interests/', students.StudentInterestsView.as_view(), name='student_interests'),
        path('taken/', students.TakenQuizListView.as_view(), name='taken_quiz_list'),
        path('quiz/<int:pk>/', students.take_quiz, name='take_quiz'),
    ], 'classroom'), namespace='students')),

    path('teachers/', include(([
        path('', teachers.TeachersListView.as_view(), name='teachers_list'),
        path('quiz_list/', teachers.QuizListView.as_view(), name='quiz_change_list'),
        path('info', teachers.TeachersInfoView.as_view(), name='teachers_info'),
        path('quiz/add/', teachers.QuizCreateView.as_view(), name='quiz_add'),
        path('quiz/<int:pk>/', teachers.QuizUpdateView.as_view(), name='quiz_change'),
        path('quiz/<int:pk>/delete/', teachers.QuizDeleteView.as_view(), name='quiz_delete'),
        path('quiz/<int:pk>/results/', teachers.QuizResultsView.as_view(), name='quiz_results'),
        path('quiz/<int:pk>/question/add/', teachers.question_add, name='question_add'),
        path('quiz/<int:quiz_pk>/question/<int:question_pk>/', teachers.question_change, name='question_change'),
        path('quiz/<int:quiz_pk>/question/<int:question_pk>/delete/', teachers.QuestionDeleteView.as_view(), name='question_delete'),
    ], 'classroom'), namespace='teachers')),
]

urlpatterns  += [
    
    path('account_settings', account_settings, name='account_settings'),
    path('personal_information', personal_information, name='personal_information'),

    path(_('signup/'), classroom.SignUpView.as_view(), name='signup'),
    path(_('signup_student/'), students.StudentSignUpView.as_view(), name='student_signup'),
    path(_('signup_teacher/'), teachers.TeacherSignUpView.as_view(), name='teacher_signup'),
    path(_('student_login/'), LoginView.as_view(), name='student_login'),
    path(_('teacher_login/'), teachers.LoginView.as_view(), name='teacher_login'),
    path('logout_then_login', logout_then_login, name='logout_then_login'),
    #password related urls

    path('logout/', classroom.LogoutView.as_view(), name='logout'),
    path('users', user_list, name='user_list'),
    path('users/<slug:slug>', user_detail, name='user_detail'),
]