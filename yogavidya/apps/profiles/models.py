from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.utils.html import escape, mark_safe
from yogavidya.apps.locations.models import Area, City, Country, State
from django.utils.translation import ugettext_lazy as _
from django.urls import reverse

class User(AbstractUser):
	MALE = 'MA'
	FEMALE = 'FE'
	GENDER = (
	(MALE, _('Male')),
	(FEMALE, _('Female')),
	)


	BEGINNER = 'First Level Yoga instructor'
	MEDIATE = 'Second Level Yoga instructor'
	MASTER = 'Master Level Yoga instructor'
	LEVEL = (
		(BEGINNER,_('First Level Yoga instructor')),
		(MEDIATE,_('Second Level Yoga instructor')),
		(MASTER,_('Master Level Yoga instructor')),
		# Relations
	)

	def upload_location(instance, filename):
		try:
			ProfileModel = instance.__class__
			new_id = ProfileModel.objects.order_by("id").last().id + 1
		except:
			new_id = 1
		return "{} {}".format(new_id, filename)
	
	is_student = models.BooleanField(default=False)
	is_teacher = models.BooleanField(default=False)
	bio = models.TextField(max_length=500, blank=True, null=True, default='')
	city = models.ForeignKey(City, on_delete=models.SET_NULL, null=True, blank=True)
	country = models.ForeignKey(Country, on_delete=models.SET_NULL, null=True, blank=True)
	area = models.ForeignKey(Area, on_delete=models.SET_NULL, null=True, blank=True)
	state = models.ForeignKey(State, on_delete=models.SET_NULL, null=True, blank=True)
	location = models.CharField(max_length=30, blank=True, null=True, default='')
	phone = models.CharField(max_length=30, blank=True, null=True, default='')
	gender = models.CharField(max_length=2,choices=GENDER, blank=True, null=True,)
	level = models.CharField(max_length=250,choices=LEVEL, blank=True, null=True,)
	birth_date = models.DateField(blank=True, null=True, )
	ministatus = models.TextField(max_length=500, blank=True, null=True, default='')
	terms = models.BooleanField(default=1)
	height_field = models.IntegerField(default=1024, null=True, blank=True)
	width_field = models.IntegerField(default=768, null=True, blank=True)
	image = models.ImageField(
							null=True,
							blank=True,
							height_field="height_field",
							width_field="width_field",
							upload_to=upload_location,)
	hobbies = models.TextField(max_length=500, blank=True, null=True, default='')
	fav_tv = models.TextField(max_length=500, blank=True, null=True, default='')
	fav_books = models.TextField(max_length=500, blank=True, null=True, default='')
	fav_games = models.TextField(max_length=500, blank=True, null=True, default='')
	fav_movies = models.TextField(max_length=500, blank=True, null=True, default='')
	fav_music = models.TextField(max_length=500, blank=True, null=True, default='')
	fav_writers = models.TextField(max_length=500, blank=True, null=True, default='')
	fav_routes = models.TextField(max_length=500, blank=True, null=True, default='')

	def __str__(self):
		return '{}'.format(self.username)

	def get_absolute_url(self):
		return reverse('profiles:profile_settings')

	def get_rep_name(self):
		full_name = '{} {}'.format(self.first_name, self.last_name)
		if self.first_name.length > 1:
			return full_name
		else:
			return 	self.username

class BlackListedToken(models.Model):
    token = models.CharField(max_length=500)
    user = models.ForeignKey(User, related_name="token_user", on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ("token", "user")




class Contact(models.Model):
	user_from = models.ForeignKey(User,related_name='rel_from_set', on_delete=models.CASCADE,)       
	user_to = models.ForeignKey(User,related_name='rel_to_set', on_delete=models.CASCADE,)
	created_at = models.DateTimeField(auto_now_add=True, db_index=True)

	class Meta:
		ordering = ('-created_at', )

	def __str__(self):
		return '{} follow {}'.format(self.user_from, self.user_to)


class Subject(models.Model):
    name = models.CharField(max_length=30)
    color = models.CharField(max_length=7, default='#007bff')

    def __str__(self):
        return self.name

    def get_html_badge(self):
        name = escape(self.name)
        color = escape(self.color)
        html = '<span class="badge badge-primary" style="background-color: %s">%s</span>' % (color, name)
        return mark_safe(html)


class Quiz(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='quizzes')
    name = models.CharField(max_length=255)
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE, related_name='quizzes')

    def __str__(self):
        return self.name


class Question(models.Model):
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name='questions')
    text = models.CharField('Question', max_length=255)

    def __str__(self):
        return self.text


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answers')
    text = models.CharField('Answer', max_length=255)
    is_correct = models.BooleanField('Correct answer', default=False)

    def __str__(self):
        return self.text


class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    quizzes = models.ManyToManyField(Quiz, through='TakenQuiz')
    interests = models.ManyToManyField(Subject, related_name='interested_students')

    def get_unanswered_questions(self, quiz):
        answered_questions = self.quiz_answers \
            .filter(answer__question__quiz=quiz) \
            .values_list('answer__question__pk', flat=True)
        questions = quiz.questions.exclude(pk__in=answered_questions).order_by('text')
        return questions

    def __str__(self):
        return self.user.username


class TakenQuiz(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='taken_quizzes')
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE, related_name='taken_quizzes')
    score = models.FloatField()
    date = models.DateTimeField(auto_now_add=True)


class StudentAnswer(models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE, related_name='quiz_answers')
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE, related_name='+')
