from django.contrib import admin
from django import forms
from yogavidya.apps.profiles.models import Contact, User

class UserAdmin(admin.ModelAdmin):
	
  class Meta:
    model = User
    
admin.site.register(User, UserAdmin)


class ContactAdmin(admin.ModelAdmin):

  class Meta:
    model = Contact

admin.site.register(Contact, ContactAdmin)