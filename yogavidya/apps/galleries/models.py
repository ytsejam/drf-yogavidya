import datetime
from django.db import models
from django.conf import settings
from photologue.models import Gallery, Photo
from django.utils.text import slugify
from yogavidya.apps.profiles.models import User
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver

class GalleryExtended(models.Model):

	# Link back to Photologue's Gallery model.
	gallery = models.ForeignKey(Gallery, related_name='extended', on_delete=models.CASCADE, null=True, blank=True)
	slug=models.SlugField(unique=True)
	cover_photo = models.ForeignKey(Photo, related_name='cover_photo', on_delete=models.CASCADE,blank=True, null=True)
	user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="galleries",default=1,on_delete=models.CASCADE)
	title = models.CharField(max_length=250, null=True, blank=True)
	# This is the important bit - where we add in the tags.
	created_at = models.DateTimeField(auto_now_add=True)
	# Boilerplate code to make a prettier display in the admin interface.
	class Meta:
		verbose_name = u'Gallery'
		verbose_name_plural = u'Galleries'

	def __str__(self):
		return self.title




class PhotoExtended(models.Model):

	# Link back to Photologue's Gallery model.
	gallery = models.ForeignKey(GalleryExtended, related_name='photos', on_delete=models.CASCADE, null=True, blank=True)
	#temp_gallery = models.ForeignKey(TempGallery, related_name='photos', on_delete=models.CASCADE,  null=True, blank=True)
	header_photo = models.BooleanField(default=False)
	photo = models.ForeignKey(Photo, related_name='extended', on_delete=models.CASCADE,)
	slug=models.SlugField(unique=True)
	user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="user_photos",default=1,on_delete=models.CASCADE)
	title = models.CharField(max_length=250, null=True, blank=True)
	# This is the important bit - where we add in the tags.
	created_at = models.DateTimeField(auto_now_add=True)
	# Boilerplate code to make a prettier display in the admin interface.
	class Meta:
		verbose_name = u'Photo'
		verbose_name_plural = u'Photos'

	def __str__(self):
		return self.title

	

# @receiver(pre_save, sender=GalleryExtended)
# def pre_save_gallery_receiver(sender, instance, raw, using, **kwargs):
# 	gallery = Gallery.objects.create(title=instance.title, slug=slugify(instance.title))
# 	gallery.save()
# 	galleryextended = GalleryExtended.objects.create(gallery=gallery)
# 	galleryextended.save() 
# pre_save.connect(pre_save_gallery_receiver, sender=GalleryExtended)

 
# @receiver(pre_save, sender=PhotoExtended)
# def pre_save_photo_receiver(sender, instance, raw, using, **kwargs):
# 	photo = Photo.objects.create(title=instance.title, slug=slugify(instance.title))
# 	photo.save()
# 	photoextended = PhotoExtended.objects.create(photo=photo, title=instance+datetime.datetime.now())
# 	photoextended.save() 
# pre_save.connect(pre_save_photo_receiver, sender=PhotoExtended)
