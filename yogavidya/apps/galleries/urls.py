from django.urls import path
from .views import (
	GalleryListView,
	GalleryCreateView,
	gallery_create,
	update_photos,
	BasicUploadView
)

app_name="galleries"

urlpatterns = [
	path('', GalleryListView.as_view(), name='list'),
	path('create/', gallery_create, name='create'),
	path('add_photos/', update_photos, name='add'),
	path('basic-upload/', BasicUploadView.as_view(), name='basic_upload'),
	#path('<slug:slug>', MerchandiseDetailView.as_view(), name='detail'),
	#path('<int:id>/update', MerchandiseUpdateView.as_view(), name='update'),
	#path('<int:id>/delete', MerchandiseUpdateView.as_view(), name='delete'),
]