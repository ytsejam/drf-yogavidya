from django import forms
from photologue.models import Gallery, Photo
from yogavidya.apps.galleries.models import GalleryExtended,PhotoExtended
from yogavidya.apps.profiles.models import User
from django.forms import HiddenInput
from django.core.exceptions import ValidationError

from django.contrib import messages

class GalleryModelForm(forms.ModelForm):

	class Meta:
	   model=GalleryExtended
	   fields = ["title" ]

 
	def clean_title(self):
		title = self.cleaned_data['title']
		galleries = Gallery.objects.all()
		for gallery in galleries:
			if gallery.title==title:
				raise ValidationError("You already used this Name")
				messages.error(request, "Name already taken")

		return title


class PhotoModelForm(forms.ModelForm):

	class Meta:
	   model=PhotoExtended
	   fields = [ 'title', 'gallery']

