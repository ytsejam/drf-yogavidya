from django.contrib import admin
from django import forms
from .models import GalleryExtended, PhotoExtended
from .forms import GalleryModelForm, PhotoModelForm
from ckeditor_uploader.widgets import CKEditorUploadingWidget
# Register your models here.
 
 
class GalleryAdmin(admin.ModelAdmin):
  form = GalleryModelForm
  content = forms.CharField(widget=CKEditorUploadingWidget())
  class Meta:
    model = GalleryExtended

class PhotoAdmin(admin.ModelAdmin):
  form = PhotoModelForm
  content = forms.CharField(widget=CKEditorUploadingWidget())
  class Meta:
    model = PhotoExtended

admin.site.register(GalleryExtended, GalleryAdmin)
admin.site.register(PhotoExtended, PhotoAdmin)
