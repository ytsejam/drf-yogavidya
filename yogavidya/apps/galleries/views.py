# -*- coding: utf-8 -*-
import random
from django.conf.urls.i18n import i18n_patterns
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404, render
from django.http import JsonResponse
from django.urls import reverse_lazy
from django.utils.text import slugify
from django.views.decorators.http import require_POST
from django.views.generic.edit import CreateView
from django.views.generic import ListView, DetailView
from django.views import View
from django.shortcuts import get_object_or_404
from yogavidya.apps.actions.utils import create_action
from yogavidya.decorators import ajax_required

from photologue.models import Gallery, Photo

from .models import GalleryExtended, PhotoExtended

from yogavidya.apps.profiles.models import User
from .forms import GalleryModelForm, PhotoModelForm
from .utils import generate_random_string
from django.db.models import Q
class AjaxableResponseMixin:
	"""
	Mixin to add AJAX support to a form.
	Must be used with an object-based FormView (e.g. CreateView)
	"""
	def form_invalid(self, form):
		response = super().form_invalid(form)
		if self.request.is_ajax():
			return JsonResponse(form.errors, status=400)
		else:
			return response

	def form_valid(self, form):
		# We make sure to call the parent's form_valid() method because
		# it might do some processing (in the case of CreateView, it will
		# call form.save() for example).
		form.instance.slug = slugify(form.instance.title)
	 
		response = super().form_valid(form)
		if self.request.is_ajax():
			data = {
				'pk': self.object.pk,
				'status': 'ok'
			}
			return JsonResponse(data)
		else:
			return response

class GalleryCreateView(AjaxableResponseMixin, CreateView):
	model = Gallery
	form_class = GalleryModelForm
	success_url = reverse_lazy('profiles:photos')

	

class GalleryListView(ListView):
	model = Gallery


class GalleryDetailView(DetailView):
	model = Gallery
	template_name = 'classroom/galleries/gallery_detail.html'
	slug_field = "slug"
 

@ajax_required
@require_POST
@login_required
def gallery_create(request):
	gallery_form = GalleryModelForm(data=request.POST)

	if gallery_form.is_valid():
		print(gallery_form)
		title=request.POST.get("title")
		slug = slugify(title)
		user_id = request.user.id
		gallery = GalleryExtended(title=title,slug=slug,user_id=user_id)

		gallery.save()
		create_action(request.user,_('gallery_added'), gallery)
		messages.success(request, "Succesfully Created")
		return JsonResponse({'status': 'ok'})
	else:
		messages.error(request, _("There is a Gallery with the Same Name. Choose a different title to create a new gallery."))
		#gallery_form = GalleryModelForm(data=request.GET)
		return JsonResponse({'status': 'ko'})




@ajax_required
@require_POST
@login_required
def photo_create(request):
	form = GalleryModelForm(request.POST or None)
	user=request.user
	if request.method == "POST":
		form = GalleryModelForm(request.POST or None, request.FILES or None)
		print(form)
		if form.is_valid():
			instance = GalleryExtended.objects.get(id=request.GET.get('gallery_id'))
			print(instance)
			instance = form.save(commit=False)
			instance.title = request.POST.get('title')
			instance.slug = slugify(instance.title)
			instance.user = user
			instance.save()
			create_action(request.user,_('gallery_added'), instance)
			messages.success(request, "Succesfully Created")
			return JsonResponse({'status': 'ok'})
		else:
			messages.error(request, "Gallery Cant be Added")
			form = GalleryModelForm(data=request.GET)
			return JsonResponse({'status': 'ko'})


class BasicUploadView(View):
 
	def get(self, request, *args, **kwargs):
		galleries = GalleryExtended.objects.filter()

		context = {
			'galleries':galleries
		}

		return render(self.request, 'galleries/photos-upload.html', context )

	def post(self, request, *args, **kwargs):
		#gallery=Gallery.objects.create(title=generate_random_string(), slug=generate_random_string(), user_id=self.request.user.id)
		title = request.POST.get('title')
		print(title)
		gallery_form = GalleryModelForm( data=request.POST)
		print(gallery_form)
		if gallery_form.is_valid():
			title = request.POST.get('title')
			print(title)
			gallery = Gallery.objects.create(title=title, slug=slugify(title))
			gallery.save()
			
			galleryextended = GalleryExtended.objects.create(title=title, slug=gallery.slug, user_id=request.user.id)
			galleryextended.save()
			data = {'is_valid': True, 'title': galleryextended.title, 'gallery_id': galleryextended.id}
		else:
			data = {'is_valid': False}
		return JsonResponse(data)


@ajax_required
@require_POST
@login_required
def update_photos(request):

	photo_form = PhotoModelForm( data=request.POST, files=request.FILES)
	if request.method == "POST":
		print(request.POST.get('gallery_id'))
		gallery_id = request.POST.get('gallery_id')
		photo_form = PhotoModelForm(request.POST or None, request.FILES or None)
		if photo_form.is_valid():
			gallery = GalleryExtended.objects.get(id=gallery_id)
			pic_title=request.FILES.get('image').name
			random_string=generate_random_string()
			title = pic_title + "_" + random_string
			photo = Photo.objects.create(title=title, slug=slugify(title), image=request.FILES.get('image'))
			photo.save()
			photoextended_title = photo.title[:5] + generate_random_string()
			photoextended = PhotoExtended.objects.create(title=photoextended_title, slug=slugify(photoextended_title), photo_id=photo.id, user_id=request.user.id)
			photoextended.save()
			gallery.photos.add(photoextended)
			 
			data = {'is_valid': True, 'title': photo.title, 'url': photo.image.url, 'id': photoextended.id }
		else:
			data = {'is_valid': False}
		return JsonResponse(data)
 
