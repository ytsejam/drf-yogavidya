from django.contrib import admin
from django.urls import include,re_path, path
from .views import(
    IndexTemplateView
)

app_name = 'core'

urlpatterns = [
	re_path(r"^.*$", IndexTemplateView.as_view(), name="home_auth")
  #path('', IndexTemplateView.as_view(), name='home_auth'),
]
