from django.contrib import admin
from .models import Action


class ActionAdmin(admin.ModelAdmin):
	list_display = ('user', 'verb', 'target', 'created_at')
	list_filter = ('created_at',)
	search_fields = ('verb',)

admin.site.register(Action, ActionAdmin)