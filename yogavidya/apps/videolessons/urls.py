from django.contrib import admin
from django.urls import include, path
from .views import(
    videolesson_list,
    videolesson_create,
    videolesson_detail,
    videolesson_update,
    videolesson_delete,
    videolesson_like,
    create_comment
)

app_name = 'videolessons'
"""
urlpatterns = [
    url(r'^$', videolesson_list, name='list'),
    # ex: /category/5/
    url(r'^detail/(?P<slug>[\w-]+)/$', videolesson_detail, name='detail'),
    url(r'^create/$', videolesson_create, name='create'),
    url(r'^(?P<videolesson_id>[0-9]+)/edit/$',videolesson_update, name='update'),
    url(r'^(?P<videolesson_id>[0-9]+)/delete/$',videolesson_delete, name='delete'),
]
"""
urlpatterns = [
    path('', videolesson_list, name='list'),
    # ex: /category/5/
    path('v/<slug:slug>', videolesson_detail, name='detail'),
    path('create/', videolesson_create, name='create'),
    path('<int:pk>/edit/',videolesson_update, name='update'),
    path('<int:pk>/delete/',videolesson_delete, name='delete'),
    path('like',videolesson_like, name='like'),
    path('comment', create_comment, name='create_comment'),
]
