from django.contrib import admin
from django import forms
from yogavidya.apps.videolessons.models import Videolesson, VideolessonComment
from yogavidya.apps.videolessons.forms import VideolessonModelForm, VideolessonCommentModelForm
from ckeditor.widgets import CKEditorWidget
 
 
class VideolessonAdmin(admin.ModelAdmin):
	content = forms.CharField(widget=CKEditorWidget())
	form = VideolessonModelForm
	prepopulated_fields = {'slug': ('title',), }
	class Meta:
		model = Videolesson


class VideolessonCommentAdmin(admin.ModelAdmin):
	form = VideolessonCommentModelForm
	class Meta:
		model = VideolessonComment
admin.site.register(Videolesson, VideolessonAdmin)
admin.site.register(VideolessonComment)
