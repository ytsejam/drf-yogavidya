# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone


class VideolessonManager(models.Manager):

  def active(self, *args, **kwargs):
    return super(VideolessonManager, self).filter(draft=False).filter(publish__lte=timezone.now())
