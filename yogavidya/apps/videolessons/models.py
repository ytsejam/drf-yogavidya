# -*- coding: utf-8 -*-
from django.conf import settings
from django.urls import reverse
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
from sorl.thumbnail import ImageField
from yogavidya.apps.categories.models  import Category
from yogavidya.apps.tags.models import Tag
from yogavidya.apps.videolessons import managers
#from taggit.managers import TaggableManager
from mptt.models import MPTTModel, TreeForeignKey
from ckeditor.fields import RichTextField
# from django.contrib.auth.models import User
# Create your models here.


class Videolesson(models.Model):


	YOGA = 'YO'
	OTHERS = 'OT'
	QUESTIONSANDANSWERS = 'QA'
	NUTRITION = 'NU'

	IND_PAG = (
	(YOGA, _('Yoga')),
	(OTHERS, _('Others')),
	(QUESTIONSANDANSWERS, _('QANDA')),
	(NUTRITION, _('Nutrition')),
	)


	def upload_location(instance, filename):
		#filebase, extension = filename.split(".")
		# return "%s/%s.%s" %(instance.id, instance.id, extension)
		VideolessonModel = instance.__class__
		try:
			new_id = VideolessonModel.objects.order_by("id").last().id + 1
		except:
			new_id = 1
		return "{} {}".format(new_id, filename)
	# Relations
	categories = models.ManyToManyField(Category,  default=[1],  verbose_name=_(
		"related_categories"),related_name="videolesson_categories" )
	tags = models.ManyToManyField(Tag,  default=[1], verbose_name=_(
		"related_tags"),related_name="videolesson_tags" )
	user = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_(
		"user"), on_delete=models.CASCADE,)
	# Attributes - Mandatory
	title = models.CharField(max_length=200, verbose_name=_(
		"title"), help_text=_("Enter the Videolesson title"))
	slug = models.SlugField(unique=True)
	filter_order = models.PositiveIntegerField(default=1)
	lesson_type = models.CharField(max_length=250,choices=IND_PAG, blank=True, null=True,)
	height_field = models.IntegerField(default=0,null=True, blank=True)
	width_field = models.IntegerField(default=0, null=True, blank=True)
	image = models.ImageField(
							null=True,
							blank=True,
							upload_to=upload_location,
							height_field="height_field",
							width_field="width_field"
							)
	author = models.CharField(max_length=500, default="Yoga Vidya",)
	content = RichTextField()
	excerpt = models.CharField(max_length=140, null=True, blank=True )
	videonumber = models.CharField(max_length=100,blank=True)
	draft = models.BooleanField(default=False)
	publish = models.DateField(auto_now=False, auto_now_add=False)
	index_page = models.BooleanField(default=False)
	chakra_page = models.BooleanField(default=False)
	other_page = models.BooleanField(default=False)
	qa_page = models.BooleanField(default=False)
	nu_page = models.BooleanField(default=False)
	created_at = models.DateTimeField(
		auto_now=True, auto_now_add=False, verbose_name=("created_at"))
	updated_at = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name=_(
		"updated_at"), help_text=_("Enter a new date"))
	# Attributes - Optional
	users_like = models.ManyToManyField(settings.AUTH_USER_MODEL,
										related_name='videolessons_liked',
										blank=True)
	total_likes = models.PositiveIntegerField(db_index=True, default=0)
	# Object Manager
	objects = managers.VideolessonManager()

	# Custom Properties

	# Methods
	def get_absolute_url(self):
		return reverse("videolessons:detail", kwargs={"slug": self.slug})

	# Meta and String
	class Meta:
		verbose_name = _("Videolesson")
		verbose_name_plural = _("Videolessons")
		ordering = ("user", "title")
		unique_together = ("user", "title")

	def __str__(self):
		return "{} - {}".format(self.user.username, self.title)


class VideolessonComment(MPTTModel):
	parent = models.ForeignKey('self', 
								null=True, 
								blank=True, 
								related_name='children',
								on_delete=models.CASCADE)
	user = models.ForeignKey(
					settings.AUTH_USER_MODEL,
					default=1,
					null=True,
					on_delete=models.CASCADE,
					related_name="videolessoncommenter")
	videolesson = models.ForeignKey(
					Videolesson, 
					on_delete=models.CASCADE,
					related_name="comments")
	active = models.BooleanField(default=True)
	published_at = models.DateField(
					auto_now=False,
					auto_now_add=False,
					null=True,
					blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	content = models.TextField()
	voters = models.ManyToManyField(settings.AUTH_USER_MODEL,
									blank=True,
									related_name="videolesson_comment_votes")

	class Meta:
		# sort comments in chronological order by default
		ordering = ('created_at',)

	def __str__(self):
		return 'Comment by {}'.format(self.user.username)


	def get_all_children(self, include_self=False):
		"""
		Gets all of the comment thread.
		"""
		children_list = self._recurse_for_children(self)
		if include_self:
			ix = 0
		else:
			ix = 1
		flat_list = self._flatten(children_list[ix:])
		return flat_list

	def _recurse_for_children(self, node):
		children = []
		children.append(node)
		for child in node.sub_comment.enabled():
			if child != self:
				children_list = self._recurse_for_children(child)
				children.append(children_list)
		return children

	def _flatten(self, L):
		if type(L) != type([]): return [L]
		if L == []: return L
		return self._flatten(L[0]) + self._flatten(L[1:])
	def get_parent(self): 
		if self.parent:
			return self.parent 
		else: 
			return self 
	def get_thread(self): 
		return self.children.all()
def create_slug(instance, new_slug=None):
	slug = slugify(instance.title)
	if new_slug is not None:
		slug = new_slug
	qs = Videolesson.objects.filter(slug=slug).order_by("-id")
	exists = qs.exists()
	if exists:
		new_slug = "%s-%s" % (slug, qs.first().id)
		return create_slug(instance, new_slug=new_slug)
	return slug
@receiver(pre_save, sender=Videolesson)
 
def pre_save_videolesson_receiver(sender, instance, raw, using, **kwargs):
	if not instance.slug:
		instance.slug = create_slug(instance)

pre_save.connect(pre_save_videolesson_receiver, sender=Videolesson)
