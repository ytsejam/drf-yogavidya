from django import forms
#from ckeditor_uploader.widgets import CKEditorUploadingWidget
from yogavidya.apps.videolessons.models import Videolesson, VideolessonComment



class VideolessonModelForm(forms.ModelForm):
	#content = forms.CharField(widget=CKEditorUploadingWidget)
	class Meta:
	   model=Videolesson
	   fields = [ 
	   			"user", 
	   			"title", 
	   			"slug",
	   			"filter_order",
	   			"categories", 
	   			"tags", 
	   			"content",
	   			"videonumber",
	   			"excerpt" ,
	   			"index_page",
	   			"chakra_page", 
	   			"qa_page", 
	   			"nu_page",
	   			"other_page", 
	   			"image",
	   			'total_likes', 
	   			'users_like', 
	   			"draft",
	   			"publish",]

class VideolessonCommentModelForm(forms.ModelForm):
	class Meta:
	   model=VideolessonComment
	   fields = ["parent", "content"]
