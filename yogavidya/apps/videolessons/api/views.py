from rest_framework import generics, status,  viewsets, filters
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404
from django.db.models import Count, F
from rest_framework.views import APIView
from django.views.generic import ListView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from yogavidya.apps.videolessons.api.permissions import IsAuthorOrReadOnly
from yogavidya.apps.videolessons.api.serializers import VideolessonSerializer, VideolessonCommentSerializer
from yogavidya.apps.videolessons.models import Videolesson, VideolessonComment
from yogavidya.apps.core.pagination import CustomPagination, PaginationHandlerMixin
from yogavidya.apps.categories.models import Category

class VideolessonViewSet(viewsets.ModelViewSet):
	queryset = Videolesson.objects.all().order_by("-created_at")
	serializer_class = VideolessonSerializer
	lookup_field = "slug"
	filter_backends = (filters.SearchFilter,)
	search_fields = ('index_page')

	def get_serializer_context(self):
		context = super(VideolessonViewSet, self).get_serializer_context()
		context.update({"request": self.request})
		
		return context

	def retrieve(self, request, slug=None):
		queryset = Videolesson.objects.filter()
		videolesson = get_object_or_404(queryset, slug=slug)
		serializer = VideolessonSerializer(videolesson, context={ 'request': request })
		return Response(serializer.data)

	def perform_create(self, serializer):
		serializer.save(user=self.request.user)

class CategoryVideolessonsListView(APIView, PaginationHandlerMixin):
	pagination_class = CustomPagination
	queryset = Videolesson.objects.all().order_by('-total_likes')
	serializer_class = VideolessonSerializer
	lookup_field = "slug"

	def get(self, request, format=None, slug=None, *args, **kwargs):
		category = get_object_or_404(Category,slug=slug)
		videolessons = category.videolesson_categories.all()
		serializer = VideolessonSerializer(videolessons, many=True)
		page = self.paginate_queryset(videolessons)
		if page is not None:
			serializer = self.get_paginated_response(
				self.serializer_class(page, many=True).data)
		else:
			serializer = self.serializer_class(instance, many=True)
		return Response(serializer.data)

class VideolessonsMostLikedView(APIView):

	queryset = Videolesson.objects.order_by('-total_likes')
	# the many param informs the serializer that it will be serializing more than a single Videolesson.
	serializer_class = VideolessonSerializer
	lookup_field = "slug"

	def get(self, request, format=None):
		videolessons = Videolesson.objects.annotate(most_liked=Count('total_likes')).order_by("-most_liked")
		serializer = VideolessonSerializer(videolessons, many=True)
		return Response(serializer.data)

class VideolessonCreateAPIView(generics.CreateAPIView):
	queryset = Videolesson.objects.all()
	serializer_class = VideolessonSerializer

	def perform_create(self, serializer):
		request_user = self.request.user
		serializer.save(user=request_user)

class VideolessonRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
	queryset = Videolesson.objects.all()
	serializer_class = VideolessonSerializer

class VideolessonLikeAPIView(APIView):
	queryset = Videolesson.objects.all()
	serializer_class = VideolessonSerializer


	def post(self, request, pk):
		Videolesson = get_object_or_404(Videolesson,pk=pk)
		user = self.request.user

		Videolesson.voters.add(user)
		Videolesson.save()



class VideolessonCommentCreateAPIView(generics.CreateAPIView):
	queryset = VideolessonComment.objects.all()
	serializer_class = VideolessonCommentSerializer

	def perform_create(self, serializer):
		kwarg_slug = self.kwargs.get("slug")
		Videolesson = get_object_or_404(Videolesson, slug=kwarg_slug)
		

		serializer.save(user=self.request.user, Videolesson=Videolesson)

class VideolessonCommentsListAPIView(generics.ListAPIView):
	queryset = Videolesson.objects.all()
	serializer_class = VideolessonCommentSerializer
	pagination_class = None

	def get_queryset(self):
		kwarg_slug = self.kwargs.get("slug")
		videolesson = get_object_or_404(Videolesson, slug=kwarg_slug)
		return VideolessonComment.objects.filter(videolesson__slug=kwarg_slug).order_by("-created_at")

class VideolessonCommentRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
	queryset = VideolessonComment.objects.all()
	serializer_class = VideolessonCommentSerializer

class VideolessonCommentLikeAPIView(APIView):
	queryset = VideolessonComment.objects.all()
	serializer_class = VideolessonCommentSerializer

	def delete(self, request, pk):
		comment = get_object_or_404(VideolessonComment,pk=pk)
		user = request.user

		comment.voters.remove(user)
		comment.save()

		serializer_context = {"request": request}
		serializer = self.serializer_class(comment, context=serializer_context)

		return Response(serializer.data, status=status.HTTP_200_OK)

	def post(self, request, pk):
		comment = get_object_or_404(VideolessonComment,pk=pk)
		user = request.user

		comment.voters.add(user)
		comment.save()

		serializer_context = {"request": request}
		serializer = self.serializer_class(comment, context=serializer_context)

		return Response(serializer.data, status=status.HTTP_200_OK)

class VideolessonIndexView(APIView):

	queryset = Videolesson.objects.filter(index_page=True).order_by("-created_at")
	# the many param informs the serializer that it will be serializing more than a single videolesson.
	serializer_class = VideolessonSerializer
	lookup_field = "slug"
	
	def get(self, request, format=None):
		videolessons = Videolesson.objects.filter(index_page=True).order_by("-created_at")
		serializer = VideolessonSerializer(videolessons, many=True)
		return Response(serializer.data)

	def get_serializer_context(self):
		context = super(VideolessonViewSet, self).get_serializer_context()
		context.update({"request": self.request})
		
		return context

	def retrieve(self, request, slug=None):
		queryset = Videolesson.objects.filter(index_page=True).order_by("-created_at")
		videolesson = get_object_or_404(queryset, slug=slug)
		serializer = VideolessonSerializer(videolesson, context={ 'request': request })
		return Response(serializer.data)


class VideolessonChakraView(APIView):

	queryset = Videolesson.objects.filter(chakra_page=True).order_by("-created_at")
	# the many param informs the serializer that it will be serializing more than a single videolesson.
	serializer_class = VideolessonSerializer
	lookup_field = "slug"
	
	def get(self, request, format=None):
		videolessons = Videolesson.objects.filter(chakra_page=True).order_by("-created_at")
		serializer = VideolessonSerializer(videolessons, many=True)
		return Response(serializer.data)

	def get_serializer_context(self):
		context = super(VideolessonViewSet, self).get_serializer_context()
		context.update({"request": self.request})
		
		return context

	def retrieve(self, request, slug=None):
		queryset = Videolesson.objects.filter(chakra_page=True).order_by("-created_at")
		videolesson = get_object_or_404(queryset, slug=slug)
		serializer = VideolessonSerializer(videolesson, context={ 'request': request })
		return Response(serializer.data)

class VideolessonOtherView(APIView):

	queryset = Videolesson.objects.filter(other_page=True).order_by("-created_at")
	# the many param informs the serializer that it will be serializing more than a single videolesson.
	serializer_class = VideolessonSerializer
	lookup_field = "slug"
	
	def get(self, request, format=None):
		videolessons = Videolesson.objects.filter(other_page=True).order_by("-created_at")
		serializer = VideolessonSerializer(videolessons, many=True)
		return Response(serializer.data)

	def get_serializer_context(self):
		context = super(VideolessonViewSet, self).get_serializer_context()
		context.update({"request": self.request})
		
		return context

	def retrieve(self, request, slug=None):
		queryset = Videolesson.objects.filter(other_page=True).order_by("-created_at")
		videolesson = get_object_or_404(queryset, slug=slug)
		serializer = VideolessonSerializer(videolesson, context={ 'request': request })
		return Response(serializer.data)

class VideolessonQAView(APIView):

	queryset = Videolesson.objects.filter(qa_page=True).order_by("-created_at")
	# the many param informs the serializer that it will be serializing more than a single videolesson.
	serializer_class = VideolessonSerializer
	lookup_field = "slug"
	
	def get(self, request, format=None):
		videolessons = Videolesson.objects.filter(qa_page=True).order_by("-created_at")
		serializer = VideolessonSerializer(videolessons, many=True)
		return Response(serializer.data)

	def get_serializer_context(self):
		context = super(VideolessonViewSet, self).get_serializer_context()
		context.update({"request": self.request})
		
		return context

	def retrieve(self, request, slug=None):
		queryset = Videolesson.objects.filter(qa_page=True).order_by("-created_at")
		videolesson = get_object_or_404(queryset, slug=slug)
		serializer = VideolessonSerializer(videolesson, context={ 'request': request })
		return Response(serializer.data)

class VideolessonNutritionView(APIView):

	queryset = Videolesson.objects.filter(nu_page=True).order_by("-created_at")
	# the many param informs the serializer that it will be serializing more than a single videolesson.
	serializer_class = VideolessonSerializer
	lookup_field = "slug"
	
	def get(self, request, format=None):
		videolessons = Videolesson.objects.filter(nu_page=True).order_by("-created_at")
		serializer = VideolessonSerializer(videolessons, many=True)
		return Response(serializer.data)

	def get_serializer_context(self):
		context = super(VideolessonViewSet, self).get_serializer_context()
		context.update({"request": self.request})
		
		return context

	def retrieve(self, request, slug=None):
		queryset = Videolesson.objects.filter(nu_page=True).order_by("-created_at")
		videolesson = get_object_or_404(queryset, slug=slug)
		serializer = VideolessonSerializer(videolesson, context={ 'request': request })
		return Response(serializer.data)	

class VideolessonCommentViewSet(viewsets.ModelViewSet):
	serializer_class = VideolessonCommentSerializer

	def get_queryset(self, queryset=None):
		queryset = VideolessonComment.objects.all()
		queryset = queryset.get_cached_trees()
		return queryset