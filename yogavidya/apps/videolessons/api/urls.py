from django.urls import include, path
from rest_framework.routers import DefaultRouter
from yogavidya.apps.videolessons.api import views as viv
router = DefaultRouter()
router.register(r"videolessons", viv.VideolessonViewSet)
urlpatterns = [
	path("", include((router.urls))),
	path("videolessonslist/index/", 
			viv.VideolessonIndexView.as_view(), 
			name="index-videolessons"
		),
	path("videolessonslist/most_liked/", 
			viv.VideolessonsMostLikedView.as_view(), 
			name="most-liked"
		),
	path("videolessonslist/others/", 
			viv.VideolessonOtherView.as_view(), 
			name="other-videolessons"
		),
	path("videolessonslist/chakras/", 
			viv.VideolessonChakraView.as_view(), 
			name="chakra-videolessons"
		),
	path("videolessonslist/qa/", 
			viv.VideolessonQAView.as_view(), 
			name="qa-videolessons"
		),
	path("videolessonlist/nu/", 
			viv.VideolessonNutritionView.as_view(), 
			name="nu-videolessons"
		),
	path("videolessons/<slug:slug>/comments/", 
			viv.VideolessonCommentsListAPIView.as_view(), 
			name="comments-list"
		),
	path("videolesson/<int:pk>/like/", 
		viv.VideolessonLikeAPIView.as_view(), 
		name="videolesson-like"
	),
	path("videolessons/<slug:slug>/comment/", 
			viv.VideolessonCommentCreateAPIView.as_view(), 
			name="comment-create"
		),
	path("videolessoncomments/<int:pk>/", 
			viv.VideolessonCommentRUDAPIView.as_view(), 
			name="comment-detail"
		),
	path("videolessoncomments/<int:pk>/like/", 
		viv.VideolessonCommentLikeAPIView.as_view(), 
		name="comment-like"
	),
	path("category_videolessons/<slug:slug>/list/", 
		viv.CategoryVideolessonsListView.as_view(), 
		name="category-list"
	),
]