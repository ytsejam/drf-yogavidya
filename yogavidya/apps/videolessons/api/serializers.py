from rest_framework import serializers
from yogavidya.apps.videolessons.models import Videolesson, VideolessonComment

from sorl_thumbnail_serializer.fields import HyperlinkedSorlImageField
from yogavidya.apps.categories.api.serializers import CategorySerializer
from rest_framework_recursive.fields import RecursiveField

class RecursiveField(serializers.Serializer):
		def to_representation(self, value):
				serializer = self.parent.parent.__class__(
						value,
						context=self.context)
				return serializer.data
#wrong
class VideolessonCommentSerializer(serializers.ModelSerializer):

	videolesson = serializers.StringRelatedField(read_only=True)
	
	parent = serializers.StringRelatedField(read_only=True)
	user = serializers.StringRelatedField(read_only=True)
	is_child_node = serializers.StringRelatedField(read_only=True)

	created_at = serializers.SerializerMethodField(read_only=True)
	voters = serializers.SerializerMethodField(read_only=True)
	 
	user_thumbnail = HyperlinkedSorlImageField(
		'55x55',
		options={"crop": "center"},
		source='user.image',
		read_only=True
	)
	children_comments = serializers.ListField(read_only=True, source='get_children', child=RecursiveField())
	class Meta:
		model = VideolessonComment
		fields = ["videolesson","user","parent","is_child_node", "children", "content", "user_thumbnail", "voters","children_comments", "get_descendant_count","created_at"]


	def get_created_at(self, instance):
		return instance.created_at.strftime("%d %B %Y")

	def get_voters(self, instance):
		return instance.voters.count()

VideolessonCommentSerializer._declared_fields['children'] = VideolessonCommentSerializer(
    many=True,
    source='get_children',
)	

class VideolessonSerializer(serializers.ModelSerializer):
	
	comments= VideolessonCommentSerializer(many=True, read_only=True)
	user = serializers.StringRelatedField(read_only=True)
	categories = CategorySerializer(many=True, read_only=True)
	slug = serializers.SlugField(read_only=True)
	likes_count = serializers.SerializerMethodField(read_only=True)
	comments_count = serializers.SerializerMethodField(read_only=True)
	publish = serializers.SerializerMethodField(source='human_format_time')
	user_image = serializers.StringRelatedField(source='user.image', read_only=True)
	user_thumbnail = HyperlinkedSorlImageField(
		'60x60',
		options={"crop": "center"},
		source='user.image',
		read_only=True
	)
	# A thumbnail image, sorl options and read-only
	thumbnail = HyperlinkedSorlImageField(
		'400x400',
		options={"crop": "center"},
		source='image',
		read_only=True
	)
	# A larger version of the image, allows writing
	image = HyperlinkedSorlImageField('1024')

	class Meta:
		model = Videolesson
		
		fields = "__all__"

	def get_created_at(self, instance):
		return instance.created_at.strftime("%d %B %Y")

	def get_likes_count(self, instance):
		return instance.users_like.count()

	def get_comments_count(self, instance):
		return instance.comments.count()


	def get_user_has_liked(self, instance):
		request = self.context.get("request")
		return instance.users_like.filter(pk=request.user.pk).exists()

	def get_publish(self, instance):
			 from django.utils.timesince import timesince
			 return timesince(instance.publish)

 

