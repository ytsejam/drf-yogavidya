import redis
from django.conf import settings
from django.contrib import messages
from django.shortcuts import get_object_or_404, render
from django.http import HttpRequest, HttpResponseRedirect
from django.template import loader
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from yogavidya.decorators import ajax_required
from django.http import HttpResponse, JsonResponse
from django.utils import timezone
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
from .forms import VideolessonModelForm, VideolessonCommentModelForm
from yogavidya.apps.videolessons.models import Videolesson, VideolessonComment
r = redis.StrictRedis(host=settings.REDIS_HOST,
          port=settings.REDIS_PORT,
          db=settings.REDIS_DB)

def videolesson_create(request):
  if not request.user.is_staff or not request.user.is_superuser:
    raise Http404
  if not request.user.is_authenticated():
    raise Http404

  form = VideolessonModelForm(request.POST or None)
  if request.method == "POST":
    form = VideolessonModelForm(request.POST or None, request.FILES or None)
    if form.is_valid():
      instance = form.save(commit=False)
      instance.description = request.POST.get("description")
      instance.save()
      messages.success(request, "Succesfully Created")
      return HttpResponseRedirect(instance.get_absolute_url())
    else:
      messages.error(request, "Image Cant be Added")
  context = {
      "form": form
    }
  return render(request, "videolessons/videolesson_form.html", context)

def videolesson_list(request):
  today=timezone.now().date()
  queryset_list = Videolesson.objects.active().order_by('-created_at').order_by('-filter_order')

  if  request.user.is_staff or  request.user.is_superuser:
    queryset_list = Videolesson.objects.all().order_by('-created_at').order_by('-filter_order')
  query = request.GET.get("q")
  if query:
    queryset_list = queryset_list.filter(
              Q(title__icontains=query) |
              Q(content__icontains=query) |
              Q(user__first_name_icontains=query) |
              Q(user__last_name_icontains=query)
              ).distinct()
  paginator = Paginator(queryset_list, 9) # Show 25 contacts per videolesson
  videolesson_request_var = "page"
  page = request.GET.get('videolesson_request_var')
  try:
      queryset = paginator.page(page)
  except PageNotAnInteger:
      # If page is not an integer, deliver first page.
      queryset = paginator.page(1)
  except EmptyPage:
      # If page is out of range (e.g. 9999), deliver last page of results.
      queryset = paginator.page(paginator.num_pages)
  videolesson_list = queryset_list
  context = {
 
        'videolessons': queryset,
        "videolesson_request_var": videolesson_request_var,
        'videolesson_list': videolesson_list,
        "title": "List",
        "today": today,
  }
  return render(request, 'videolessons/videolesson_list.html', context)
  
@login_required
def videolesson_detail(request, slug):
    videolesson = get_object_or_404(Videolesson, slug=slug)
    similar_videolessons = videolesson.tags.similar_objects()
    total_views = r.incr('videolesson:{}:views'.format(videolesson.id))

    r.zincrby('videolesson_ranking', videolesson.id, 1)
    comments = videolesson.comments.all()
    if request.method == 'POST':
    # comment has been added
      comment_form = VideolessonCommentModelForm(data=request.POST)
      print(comment_form)
      if comment_form.is_valid():
        parent_obj = None
        # get parent comment id from hidden input
        try:
          # id integer e.g. 15
          parent_id = int(request.POST.get('parent_id'))
        except:
          parent_id = None
        # if parent_id has been submitted get parent_obj id
        if parent_id:
          parent_obj = VideolessonComment.objects.get(id=parent_id)
          # if parent object exist
          if parent_obj:
            # create replay comment object
            replay_comment = comment_form.save(commit=False)
            # assign parent_obj to replay comment
            replay_comment.parent = parent_obj
        # normal comment
        # create comment object but do not save to database
        new_comment = comment_form.save(commit=False)
        # assign ship to the comment
        new_comment.videolesson = videolesson
        # save
        new_comment.save()
    else:
      comment_form = VideolessonCommentModelForm()
    context = {
          'videolesson': videolesson,
          'similar_videolessons': similar_videolessons,
          'total_views':total_views,
          'comments': comments,
          'comment_form': comment_form

    }
    return render(request, 'videolessons/videolesson_detail.html', context)
 
def videolesson_update(request, videolesson_id):
    instance = get_object_or_404(Videolesson, pk=videolesson_id)
    if instance.draft or instance.publish > timezone.now().date():
      if not request.user.is_staff or not request.user.is_superuser:
        raise Http404
    form = VideolessonModelForm(request.POST or None, request.FILES or None, instance = instance)
    if form.is_valid():
      instance = form.save(commit=False)
      instance.description = request.POST.get("description")
      print(request.POST["description"])
      instance.save()
      messages.success(request, "Item Saved")
      return HttpResponseRedirect(instance.get_absolute_url())
    context = {
        'instance': instance,
        "form": form
      }

    return render(request, 'videolessons/videolesson_update.html', context)

def videolesson_delete(request, videolesson_id):
    instance = get_object_or_404(Videolesson, pk=videolesson_id)
    instance.delete()
    messages.success(request, "Succesfully Deleted")
    return redirect("videolessons:list")

@ajax_required
@login_required
@require_POST
def videolesson_like(request):
    videolesson_id =request.POST.get('id')
    action = request.POST.get('action')
    if videolesson_id and action:
        try:
            videolesson = Videolesson.objects.get(id=videolesson_id)
            if action == 'like':
                videolesson.users_like.add(request.user)
            else:
                videolesson.users_like.remove(request.user)
            return JsonResponse({'status':'ok'})
        except:
            pass
    return JsonResponse({'status': 'ok'})

@ajax_required
@login_required
@require_POST
def create_commentvq(request):
    if request.method == 'POST':
        comment_text = request.POST.get('content')
        videolesson_id = request.POST.get('videolesson_id')
        response_data = {}

        comment = VideolessonComment(content=comment_text, user=request.user, videolesson_id=videolesson_id)
        comment.save()

        response_data['result'] = 'Create post successful!'
        response_data['comment'] = comment.pk
        response_data['content'] = comment.content
        response_data['created_at'] = comment.created_at.strftime('%B %d, %Y %I:%M %p')
        response_data['user'] = comment.user.username

        return JsonResponse({'status': 'ok', 'data': response_data})
    else:
        pass
    return JsonResponse({'status': 'ko'})


@ajax_required
@login_required
@require_POST
def create_comment(request):
  if request.method == 'POST':
    content_text = request.POST.get('content')
    videolesson_id = request.POST.get('videolesson_id')
    data = {}

    comment = VideolessonComment(content=content_text, user=request.user, videolesson_id=videolesson_id)
    comment.save()

    data['result'] = 'Create Status successful!'
    data['comment'] = comment.pk
    data['content'] = comment.content
    data['created_at'] = comment.created_at.strftime('%B %d, %Y %I:%M %p')
    data['user'] = comment.user.username

    return JsonResponse({'status': 'ok', 'data': data})
  else:
    pass
  return JsonResponse({'status': 'ko'})