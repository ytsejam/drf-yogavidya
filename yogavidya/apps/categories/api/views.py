from rest_framework import viewsets
from yogavidya.apps.categories.api.serializers import CategorySerializer
from yogavidya.apps.categories.models import Category

class CategoryViewSet(viewsets.ModelViewSet):
	queryset = Category.objects.all()
	lookup_field = "slug"
	serializer_class = CategorySerializer

	def perform_create(self, serializer):
		serializer.save()