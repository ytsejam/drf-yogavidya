from django.urls import include, path
from rest_framework.routers import DefaultRouter
from yogavidya.apps.categories.api import views as cav

router = DefaultRouter()
router.register(r"categories", cav.CategoryViewSet)

urlpatterns = [
	path("", include(router.urls))
]