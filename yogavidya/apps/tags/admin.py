from django.contrib import admin
from django import forms
from yogavidya.apps.tags.models import Tag
from yogavidya.apps.tags.forms import TagModelForm

# Register your models here.


class TagAdmin(admin.ModelAdmin):
  form = TagModelForm
  prepopulated_fields = {'slug': ('title',), }
  class Meta:
    model = Tag
    
admin.site.register(Tag, TagAdmin)
