# Generated by Django 3.0.5 on 2020-10-05 00:27

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tags', '0002_auto_20201005_0323'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tag',
            name='height_field',
        ),
        migrations.RemoveField(
            model_name='tag',
            name='image',
        ),
        migrations.RemoveField(
            model_name='tag',
            name='width_field',
        ),
    ]
