from rest_framework import serializers
from yogavidya.apps.tags.models import Tag

class TagSerializer(serializers.ModelSerializer):
	slug = serializers.SlugField(read_only=True)
	created_at = serializers.SerializerMethodField(read_only=True)

	class Meta:
		model = Tag
		exclude = [ "updated_at"]


	def get_created_at(self, instance):
		return instance.created_at.strftime("%d %B %Y")
