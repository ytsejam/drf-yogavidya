from rest_framework import viewsets
from yogavidya.apps.tags.api.serializers import TagSerializer
from yogavidya.apps.tags.models import Tag

class TagViewSet(viewsets.ModelViewSet):
	queryset = Tag.objects.all()
	lookup_field = "slug"
	serializer_class = TagSerializer

	def perform_create(self, serializer):
		serializer.save()