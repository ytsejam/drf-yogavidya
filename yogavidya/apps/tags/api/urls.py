from django.urls import include, path
from rest_framework.routers import DefaultRouter
from yogavidya.apps.tags.api import views as tav

router = DefaultRouter()
router.register(r"tags", tav.TagViewSet)

urlpatterns = [
	path("", include(router.urls))
]