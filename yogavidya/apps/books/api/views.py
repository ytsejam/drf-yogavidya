from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from yogavidya.apps.books.api.serializers import BookSerializer
from yogavidya.apps.books.models import Book

class BookViewSet(viewsets.ModelViewSet):
	queryset = Book.objects.all()
	lookup_field = "slug"
	serializer_class = BookSerializer
	permission_classes = []

	def perform_create(self, serializer):
		serializer.save(user=self.request.user)