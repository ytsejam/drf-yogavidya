from rest_framework import serializers
from sorl_thumbnail_serializer.fields import HyperlinkedSorlImageField
from yogavidya.apps.books.models import Book, BookComment

class BookSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField(read_only=True)
    slug = serializers.SlugField(read_only=True)
    created_at = serializers.SerializerMethodField(read_only=True)
    likes_count = serializers.SerializerMethodField(read_only=True)
    # A thumbnail image, sorl options and read-only
    thumbnail = HyperlinkedSorlImageField(
        '400x400',
        options={"crop": "center"},
        source='image',
        read_only=True
    )
    # A larger version of the image, allows writing
    image = HyperlinkedSorlImageField('1024')
    class Meta:
        model = Book
        exclude = ["users_like", "updated_at"]

    def get_created_at(self, instance):
        return instance.created_at.strftime("%d %B %Y")

    def get_likes_count(self, instance):
        return instance.total_likes.count()

    def get_user_has_liked(self, instance):
        request = self.context.get("request")
        return instance.users_like.filter(pk=request.user.pk).exists()


class BookCommentSerializer(serializers.ModelSerializer):
    book = serializers.StringRelatedField(read_only=True)
    parent = serializers.StringRelatedField(read_only=True)
    user = serializers.StringRelatedField(read_only=True)
    created_at = serializers.SerializerMethodField(read_only=True)
    comments_count = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = BookComment
        exclude = ["updated_at"]

    def get_created_at(self, instance):
        return instance.created_at.strftime("%d %B %Y")

    def get_comments_count(self, instance):
        return instance.comments.count()
