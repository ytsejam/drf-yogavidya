from django.urls import include, path
from rest_framework.routers import DefaultRouter
from yogavidya.apps.books.api import views as bov

router = DefaultRouter()
router.register(r"books", bov.BookViewSet)

urlpatterns = [
	path("", include(router.urls))
]