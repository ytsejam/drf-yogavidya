from django.urls import path
from .views import (
	BookListView,
	BookCreateView,
	BookDetailView,
	BookUpdateView,
	BookDeleteView,

)

app_name="books"

urlpatterns = [
	path('', BookListView.as_view(), name='list'),
	path('create/', BookCreateView.as_view(), name='create'),
	path('<slug:slug>', BookDetailView.as_view(), name='detail'),
	path('<int:id>/update', BookUpdateView.as_view(), name='update'),
	path('<int:id>/delete', BookUpdateView.as_view(), name='delete'),
]