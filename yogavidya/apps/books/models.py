# -*- coding: utf-8 -*-
from django.conf import settings
from django.urls import reverse
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.translation import ugettext_lazy as _
from django.utils.text import slugify
#from sorl.thumbnail import ImageField
from yogavidya.apps.articles import managers
from yogavidya.apps.categories.models import Category
from yogavidya.apps.tags.models import Tag
#from ckeditor_uploader.fields import RichTextUploadingField
#from taggit.managers import TaggableManager
from yogavidya.apps.core.utils import generate_random_string
#from taggit.managers import TaggableManager
from mptt.models import MPTTModel, TreeForeignKey
#from django.contrib.auth.models import User

# Create your models here.


class Book(models.Model):

    def upload_location(instance, filename):
        #filebase, extension = filename.split(".")
        # return "%s/%s.%s" %(instance.id, instance.id, extension)
        try:
            BooksModel = instance.__class__
            new_id = BooksModel.objects.order_by("id").last().id + 1
        except:
            new_id = 1

        return "{} {}".format(new_id, filename)
    # Relations
    categories = models.ManyToManyField(Category,  default=[1],  verbose_name=_(
        "related_categories"), related_name="category_books")
    # tags = models.ManyToManyField(Tag,  default=1, verbose_name=_(
    #    "related_tags"), )
    tags = models.ManyToManyField(Tag,  default=[1],  verbose_name=_(
        "related_categories"), )
    #similar_posts = instance.tags.similar_objects()[:5]
    #all_tabs = instance.tags.all()
    # Attributes - Mandatory
    id = models.AutoField(primary_key=True)
    title = models.CharField(max_length=200, verbose_name=_(
        "title"), help_text=_("Enter the Books title"))
    sub_title = models.CharField(max_length=200, verbose_name=_(
        "sub_title"), help_text=_("Enter the Books title"), null=True,
        blank=True,)
    slug = models.SlugField(unique=True)
    price = models.FloatField(default=0)
    filter_order = models.PositiveIntegerField(default=1)
    height_field = models.IntegerField(default=400)
    width_field = models.IntegerField(default=280)
    image = models.ImageField(
        null=True,
        blank=True,
        upload_to=upload_location,
        height_field="height_field",
        width_field="width_field")
    content = models.TextField()
    excerpt = models.CharField(max_length=140, null=True, blank=True)

    draft = models.BooleanField(default=False)
    index_page = models.BooleanField(default=False)
    publish = models.DateField(auto_now=False, auto_now_add=False)
    created_at = models.DateTimeField(
        auto_now=True, auto_now_add=False, verbose_name=("created_at"))
    updated_at = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name=_(
        "updated_at"), help_text=_("Enter a new date"))
    # Attributes - Optional
    users_like = models.ManyToManyField(settings.AUTH_USER_MODEL,
                                        related_name='books_liked',
                                        blank=True)
    total_likes = models.PositiveIntegerField(db_index=True, default=0)
    # Object Manager
    objects = managers.ArticleManager()

    # Custom Properties

    # Methods
    def get_absolute_url(self):
        return reverse("books:detail", kwargs={"slug": self.slug})

    # Meta and String

    class Meta:
        verbose_name = _("Merchandise")
        verbose_name_plural = _("Merchandises")
        ordering = ("title",)

    def __str__(self):
        return "{}".format(self.title)


class BookComment(MPTTModel):
    parent = models.ForeignKey('self',
                               null=True,
                               blank=True,
                               related_name='replies',
                               on_delete=models.CASCADE)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        default=1,
        null=True,
        on_delete=models.CASCADE,
        related_name="bookcomments")
    book = models.ForeignKey(
        Book,
        on_delete=models.CASCADE,
        related_name="comments")
    active = models.BooleanField(default=True)
    published_at = models.DateField(
        auto_now=False,
        auto_now_add=False,
        null=True,
        blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    content = models.TextField()
    voters = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True,
                                    related_name="book_comment_votes")

    class Meta:
        # sort comments in chronological order by default
        ordering = ('created_at',)

    def __str__(self):
        return 'Comment by {}'.format(self.user.username)


@receiver(pre_save, sender=Book)
def pre_save_book_receiver(sender, instance, raw, using, **kwargs):
    if instance and not instance.slug:
        slug = slugify(instance.title)
        random_string = generate_random_string()
        instance.slug = slug + "_" + random_string


pre_save.connect(pre_save_book_receiver, sender=Book)
