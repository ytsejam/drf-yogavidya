from django import forms
from yogavidya.apps.books.models import Book, BookComment



class BookModelForm(forms.ModelForm):
	class Meta:
	   model=Book
	   fields = [
					"title",
					"slug",
					"categories",
					"tags",
					"content",
					"excerpt",
					"index_page",
					"image",
					"draft",
					"publish"]

class BookCommentModelForm(forms.ModelForm):
	class Meta:
	   model=BookComment
	   fields = [ "content"]
