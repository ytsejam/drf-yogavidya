from django.contrib import admin
from .models import Book, BookComment
from .forms import BookModelForm


class BookAdmin(admin.ModelAdmin):
    form = BookModelForm
    prepopulated_fields = {'slug': ('title',), }

    class Meta:
        model = Book
        fields = "__all__"


admin.site.register(Book)
admin.site.register(BookComment)
