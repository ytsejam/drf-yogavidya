from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.generic import (
	CreateView,
	DetailView,
	ListView,
	UpdateView,
	DeleteView
)
from .forms import BookModelForm
from .models import (Book, BookComment)


class BookCreateView(CreateView):
	queryset = Book.objects.all()
	form_class = BookModelForm
	template_name = 'yogavidya/books/book_create.html'

	def form_valid(self, form):
		return super().form_valid(form)
	 
class BookListView(ListView):
	queryset = Book.objects.all()
	context_object_name = 'books'
	template_name = 'yogavidya/books/book_list.html'

class BookCreateView(CreateView):
	template_name = 'yogavidya/books/book_create.html'

class BookDetailView(DetailView):
	context_object_name = 'book'
	template_name = 'yogavidya/books/book_detail.html'

	def get_object(self):
		slug= self.kwargs.get("slug")
		return get_object_or_404(Book, slug=slug)



class BookUpdateView(UpdateView):
	template_name = 'yogavidya/books/book_update.html'
	form_class = BookModelForm
	template_name = 'yogavidya/books/book_create.html'

	def get_object(self):
		slug= self.kwargs.get("slug")
		return get_object_or_404(Book, slug=slug)

	def form_valid(self, form):
		return super().form_valid(form)

class BookDeleteView(DeleteView):
	template_name = 'yogavidya/books/book_delete.html'

	def get_object(self):
		slug= self.kwargs.get("slug")
		return get_object_or_404(Book, slug=slug)

	def get_success_url(self):
		return reverse('books:list')