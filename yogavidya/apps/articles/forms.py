from django import forms
#from ckeditor_uploader.widgets import CKEditorUploadingWidget
from yogavidya.apps.articles.models import Article, ArticleComment



class ArticleModelForm(forms.ModelForm):
	#content = forms.CharField(widget=CKEditorUploadingWidget)
	class Meta:
	   model=Article
	   fields = [ "title", "slug","filter_order", "author", "categories", "tags", "content","excerpt","index_page","image",'total_likes', 'users_like', "draft","publish",]

class ArticleCommentModelForm(forms.ModelForm):
	class Meta:
	   model=ArticleComment
	   fields = ["content"]
