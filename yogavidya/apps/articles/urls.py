from django.contrib import admin
from django.urls import include, path
from .views import(
    article_list,
    article_create,
    article_detail,
    article_update,
    article_delete,
    article_like,
    create_comment
)

app_name = 'articles'

urlpatterns = [
    path('', article_list, name='list'),
    path('a/<slug:slug>', article_detail, name='detail'),
    path('create/', article_create, name='create'),
    path('<int:pk>/edit/',article_update, name='update'),
    path('<int:pk>/delete/',article_delete, name='delete'),
    path('like',article_like, name='like'),
    path('comment', create_comment, name='create_comment'),
]
