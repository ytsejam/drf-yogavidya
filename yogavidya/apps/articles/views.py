from django.shortcuts import render
# Create your views here.
import redis
from django.conf import settings
from django.views.decorators.http import require_POST
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.http import HttpRequest, HttpResponseRedirect
#from django.template import loader
#from django.urls import reverse
from yogavidya.apps.actions.utils import create_action
from yogavidya.decorators import ajax_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.utils import timezone
from .forms import ArticleModelForm, ArticleCommentModelForm
from .filters import ArticleFilter
from yogavidya.apps.articles.models import Article, ArticleComment
from yogavidya.apps.categories.models import Category

r = redis.StrictRedis(host=settings.REDIS_HOST,
					port=settings.REDIS_PORT,
					db=settings.REDIS_DB)
def search(request):
		article_list = Article.objects.all()
		article_filter = ArticleFilter(request.GET, queryset=article_list)
		return render(request, 'articles/article_list.html', {'filter': article_filter})

@login_required
def article_create(request):
	if not request.user.is_staff or not request.user.is_superuser:
		raise Http404
	if not request.user.is_authenticated():
		raise Http404

	form = ArticleModelForm(request.POST or None)
	if request.method == "POST":
		form = ArticleModelForm(request.POST or None, request.FILES or None)
		if form.is_valid():
			instance = form.save(commit=False)
			instance.description = request.POST.get("description")
			#assign categories to Article
			instance.categories_set.add(request.POST.get("categories"))
			instance.user = request.user
			instance.save()
			create_action(request.user,_('created_article'), instance)
			messages.success(request, "Succesfully Created")
			return redirect(instance.get_absolute_url())
		else:
			messages.error(request, "Article Cant be Added")
			form = ArticleModelForm(data=request.GET)
	context = {
			"form": form
		}
	return render(request, "articles/article_form.html", context)


def article_list(request):
	queryset_list = Article.objects.active().order_by('-order').order_by('-created_at')
	categories = Category.objects.all()
	if request.user.is_staff or request.user.is_superuser:
		queryset_list = Article.objects.all().order_by('-created_at')
	query = request.GET.get("q")
	if query:
		queryset_list = queryset_list.filter(
							Q(title__icontains=query) |
							Q(content__icontains=query) |
							Q(user__first_name_icontains=query) |
							Q(user__last_name_icontains=query)
							).distinct()
	paginator = Paginator(queryset_list, 6)  # Show 6 articles per page
	number_of_pages = paginator.num_pages
	article_request_var = "page"
	page = request.GET.get(article_request_var)
	try:
			queryset = paginator.page(page)
	except PageNotAnInteger:
			# If page is not an integer, deliver first page.
			queryset = paginator.page(1)
	except EmptyPage:
		if request.is_ajax():
			return HttpResponse('')
			# If page is out of range (e.g. 9999), deliver last page of results.
		queryset = paginator.page(paginator.num_pages)
	if request.is_ajax():
			article_list = queryset_list
			context = {
						'is_paginated': True,
						'number_of_pages': number_of_pages,
						'articles': queryset,
						'categories': categories,
						'article_list': article_list,
						"article_request_var": article_request_var,
						"title": "Article List",
			}
			if request.user.is_authenticated:
				return render(request, 'articles/article_list_logged.html', context)
			return render(request, 'articles/article_list.html', context)
	article_list  = queryset_list
	context = {
				'is_paginated': True,
				'articles': queryset,
				'categories': categories,
				'article_list': article_list,
				"article_request_var": article_request_var,
				"title": "Article List",

	}
	if request.user.is_authenticated:
		return render(request, 'articles/article_list_logged.html', context)
	return render(request, 'articles/article_list.html', context)


def article_detail(request, slug):
		article = get_object_or_404(Article, slug=slug)
		similar_articles = article.tags.similar_objects()
		total_views = r.incr('article:{}:views'.format(article.id))

		r.zincrby('article_ranking', article.id, 1)
		comments = article.comments.all()
		if request.method == 'POST':
		# comment has been added
			comment_form = ArticleCommentModelForm(data=request.POST)
			print(comment_form)
			if comment_form.is_valid():
				parent_obj = None
				# get parent comment id from hidden input
				try:
					# id integer e.g. 15
					parent_id = int(request.POST.get('parent_id'))
				except:
					parent_id = None
				# if parent_id has been submitted get parent_obj id
				if parent_id:
					parent_obj = ArticleComment.objects.get(id=parent_id)
					# if parent object exist
					if parent_obj:
						# create replay comment object
						replay_comment = comment_form.save(commit=False)
						# assign parent_obj to replay comment
						replay_comment.parent = parent_obj
				# normal comment
				# create comment object but do not save to database
				new_comment = comment_form.save(commit=False)
				# assign ship to the comment
				new_comment.article = article
				# save
				new_comment.save()
		else:
			comment_form = ArticleCommentModelForm()
		context = {
					'article': article,
					'similar_articles': similar_articles,
					'total_views':total_views,
					'comments': comments,
					'comment_form': comment_form

		}
		return render(request, 'articles/article_detail.html', context)
 
@login_required
def article_ranking(request):
	article_ranking = r.zrange('article_ranking', 0, -1, desc=True)[:10]
	article_ranking_ids = [int(id) for id in article_ranking]
	#get most viewed articles
	most_viewed = list(Article.objects.filter(id__in=article_ranking_ids))
	most_viewed.sort(key=lambda x: article_ranking_ids.index(x.id))
	context = {
		'most_viewed': most_viewed
	}
	return render(reques, 'articles/article_ranking.html', context)

@login_required
def article_update(request, article_id):
		instance = get_object_or_404(Article, pk=article_id)
		if instance.draft or instance.publish > timezone.now().date():
			if not request.user.is_staff or not request.user.is_superuser:
				raise Http404
		form = ArticleModelForm(request.POST or None,
											 request.FILES or None, instance=instance)
		if form.is_valid():
			instance = form.save(commit=False)
			instance.description = request.POST.get("description")
			instance.save()
			create_action(request.user,_('updated article'), instance)
			messages.success(request, "Succesfully Updated")
			return redirect(instance.get_absolute_url())
		context = {
				'instance': instance,
				"form": form
			}

		return render(request, 'articles/article_update.html', context)

@login_required
def article_delete(request, article_id):
		instance = get_object_or_404(Article, pk=article_id)
		instance.delete()
		create_action(request.user,_('Deleted article'), instance)
		messages.success(request, "Succesfully Deleted")
		return redirect("articles:list")

@ajax_required
@login_required
@require_POST
def article_like(request):
	article_id = request.POST.get('id')
	action = request.POST.get('action')
	if article_id and action:
		try:
			article = Article.objects.get(id=article_id)
			print(article.total_likes)
			if action == 'like':
				article.users_like.add(request.user)
				print(article.total_likes)
				article.total_likes += 1
				print(article.total_likes)
				create_action(request.user, '_(likes)', article)
			else:
				article.users_like.remove(request.user)
				article.total_likes -= 1
			return JsonResponse({'status': 'ok'})
		except:
			pass
		return JsonResponse({'status': 'ko'})

@ajax_required
@login_required
@require_POST
def create_commentrwa(request):
    if request.method == 'POST':
        comment_text = request.POST.get('content')
        article_id = request.POST.get('article_id')
        response_data = {}

        comment = ArticleComment(content=comment_text, user=request.user, article_id=article_id)
        comment.save()
        print(comment.content)
        response_data['result'] = 'Create post successful!'
        response_data['comment'] = comment.pk
        response_data['content'] = comment.content
        response_data['created_at'] = comment.created_at.strftime('%B %d, %Y %I:%M %p')
        response_data['user'] = comment.user.username
        data = {'status': 'ok','data': response_data,  'content': comment.content}
        
    else:
        data={'status': 'ko'}

    return JsonResponse(data)
@ajax_required
@login_required
@require_POST
def create_comment(request):
	if request.method == 'POST':
		content_text = request.POST.get('content')
		article_id = request.POST.get('article_id')
		data = {}

		comment = ArticleComment(content=content_text, user=request.user, article_id=article_id)
		comment.save()

		data['result'] = 'Create Status successful!'
		data['node_id'] = comment.pk
		data['content'] = comment.content
		data['created_at'] = comment.created_at.strftime('%B %d, %Y %I:%M %p')
		data['user'] = comment.user.username
		
		return JsonResponse({'status': 'ok', 'data': data})

	else:
		pass
	return JsonResponse({'status': 'ko'})