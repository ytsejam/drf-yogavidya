import json
from rest_framework import serializers
from sorl_thumbnail_serializer.fields import HyperlinkedSorlImageField
from fluent_comments.models import FluentComment

from yogavidya.apps.articles.models import Article, ArticleComment
from yogavidya.apps.categories.api.serializers import CategorySerializer
from rest_framework_recursive.fields import RecursiveField

class RecursiveField(serializers.Serializer):
		def to_representation(self, value):
				serializer = self.parent.parent.__class__(
						value,
						context=self.context)
				return serializer.data

class CommentSerializer(serializers.ModelSerializer):
		children = RecursiveField(many=True)
		
		class Meta:
				model = FluentComment
				fields = (
						'comment',
						'id',
						'children',
					 )

class ArticleCommentSerializer(serializers.ModelSerializer):
	article = serializers.StringRelatedField(read_only=True)
	
	parent = serializers.StringRelatedField(read_only=True)
	user = serializers.StringRelatedField(read_only=True)
	is_child_node = serializers.StringRelatedField(read_only=True)

	created_at = serializers.SerializerMethodField(read_only=True)
	voters = serializers.SerializerMethodField(read_only=True)
 
	user_thumbnail = HyperlinkedSorlImageField(
		'55x55',
		options={"crop": "center"},
		source='user.image',
		read_only=True
	)
	
	class Meta:
		model = ArticleComment
		fields = ["article", "user","parent","is_child_node", "children",  "content", "user_thumbnail", "voters", "get_descendant_count","created_at"]


	def get_created_at(self, instance):
		return instance.created_at.strftime("%d %B %Y")

	def get_voters(self, instance):
		return instance.voters.count()

ArticleCommentSerializer._declared_fields['children'] = ArticleCommentSerializer(
    many=True,
    read_only=True,
    source='get_children',
)		

class ArticleSerializer(serializers.ModelSerializer):
	comments= ArticleCommentSerializer(many=True, read_only=True)
	user = serializers.StringRelatedField(read_only=True)
	categories = CategorySerializer(many=True, read_only=True)
	username = serializers.StringRelatedField(source='user', read_only=True)
	user_image = serializers.StringRelatedField(source='user.image.url', read_only=True)
	user_thumbnail = HyperlinkedSorlImageField(
		'60x60',
		options={"crop": "center"},
		source='user.image',
		read_only=True
	)
	slug = serializers.SlugField(read_only=True)
	created_at = serializers.SerializerMethodField(read_only=True)
	likes_count = serializers.SerializerMethodField(read_only=True)
	comments_count = serializers.SerializerMethodField(read_only=True)
	publish = serializers.SerializerMethodField(source='human_format_time')
	# A thumbnail image, sorl options and read-only
	thumbnail = HyperlinkedSorlImageField(
		'800x800',
		options={"crop": "center"},
		source='image',
		read_only=True
	)
	middle_thumbnail = HyperlinkedSorlImageField(
		'400x300',
		options={"crop": "center"},
		source='image',
		read_only=True
	)
	# A larger version of the image, allows writing
	image = HyperlinkedSorlImageField('1024')

	class Meta:
		model = Article
		exclude = ["updated_at",]

	def get_created_at(self, instance):
		return instance.created_at.strftime("%d %B %Y")

	def get_user_has_liked(self, instance):
		request = self.context.get("request")
		return instance.users_like.filter(pk=request.user.pk).exists()


	def get_likes_count(self, instance):
		return instance.users_like.count()

	def get_comments_count(self, instance):
		return instance.comments.count()

	def get_comments(self,obj):
				 article_comment = FluentComment.objects.filter(object_pk = obj.id, parent_id = None)
				 serializer = CommentSerializer(article_comment,many=True)
				 return serializer.data
				 
	def get_publish(self, instance):
			 from django.utils.timesince import timesince
			 return timesince(instance.publish)

