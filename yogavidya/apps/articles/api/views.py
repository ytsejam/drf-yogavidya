from rest_framework import generics, status,  viewsets, filters
from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404
from django.db.models import Count, F
from rest_framework.views import APIView
from django.views.generic import ListView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from yogavidya.apps.articles.api.permissions import IsAuthorOrReadOnly
from yogavidya.apps.articles.api.serializers import ArticleSerializer, ArticleCommentSerializer
from yogavidya.apps.articles.models import Article, ArticleComment
from yogavidya.apps.categories.models import Category 
from yogavidya.apps.core.pagination import CustomPagination, PaginationHandlerMixin
from fluent_comments.models import FluentComment
from .serializers import CommentSerializer
from django.contrib.contenttypes.models import ContentType
from rest_framework.response import Response

class ArticleViewSet(viewsets.ModelViewSet):
	queryset = Article.objects.all().order_by("-created_at")
	serializer_class = ArticleSerializer
	lookup_field = "slug"
	filter_backends = (filters.SearchFilter,)
	search_fields = ('index_page')
	

	def get_serializer_context(self):
		context = super(ArticleViewSet, self).get_serializer_context()
		context.update({"request": self.request})
		
		return context

	def retrieve(self, request, slug=None):
		queryset = Article.objects.filter()
		article = get_object_or_404(queryset, slug=slug)
		serializer = ArticleSerializer(article, context={ 'request': request })
		return Response(serializer.data)

	def perform_create(self, serializer):
		serializer.save(user=self.request.user)

class ArticleIndexView(APIView):

	queryset = Article.objects.filter(index_page=True).order_by("-created_at")
	# the many param informs the serializer that it will be serializing more than a single article.
	serializer_class = ArticleSerializer
	lookup_field = "slug"
	
	def get(self, request, format=None):
		articles = Article.objects.filter(index_page=True).order_by("-created_at")
		serializer = ArticleSerializer(articles, many=True)
		return Response(serializer.data)

	def get_serializer_context(self):
		context = super(ArticleViewSet, self).get_serializer_context()
		context.update({"request": self.request})
		return context

	def retrieve(self, request, slug=None):
		queryset = Article.objects.filter(index_page=True).order_by("-created_at")
		article = get_object_or_404(queryset, slug=slug)
		serializer = ArticleSerializer(article, context={ 'request': request })
		return Response(serializer.data)


class ArticlesMostLikedView(APIView):

	queryset = Article.objects.order_by('-total_likes')
	# the many param informs the serializer that it will be serializing more than a single article.
	serializer_class = ArticleSerializer
	lookup_field = "slug"

	def get(self, request, format=None):
		articles = Article.objects.annotate(most_liked=Count('total_likes')).order_by("-most_liked")
		serializer = ArticleSerializer(articles, many=True)
		return Response(serializer.data)

class CategoryArticlesListView(APIView, PaginationHandlerMixin):
	pagination_class = CustomPagination
	queryset = Article.objects.all().order_by('-total_likes')
	serializer_class = ArticleSerializer
	lookup_field = "slug"

	def get(self, request, format=None, slug=None, *args, **kwargs):
		category = get_object_or_404(Category,slug=slug)
		articles = category.article_categories.all()
		serializer = ArticleSerializer(articles, many=True)
		page = self.paginate_queryset(articles)
		if page is not None:
			serializer = self.get_paginated_response(
				self.serializer_class(page, many=True).data)
		else:
			serializer = self.serializer_class(instance, many=True)
		return Response(serializer.data)
	
class ArticleCreateAPIView(generics.CreateAPIView):
	queryset = Article.objects.all()
	serializer_class = ArticleSerializer

	def perform_create(self, serializer):
		request_user = self.request.user
		serializer.save(user=request_user)

class ArticleRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
	queryset = Article.objects.all()
	serializer_class = ArticleSerializer

class ArticleLikeAPIView(APIView):
	queryset = Article.objects.all()
	serializer_class = ArticleSerializer

	def delete(self, request, pk):
		article = get_object_or_404(Article,pk=pk)
		user = self.request.user

		article.users_like.remove(user)
		article.save()

		serializer_context = {"request": request}
		serializer = self.serializer_class(article, context=serializer_context)

		return Response(serializer.data, status=status.HTTP_200_OK)

	def post(self, request, slug):
		article = get_object_or_404(Article,slug=slug)
		user = self.request.user

		article.users_like.add(user)
		article.save()

		serializer_context = {"request": request}
		serializer = self.serializer_class(article, context=serializer_context)

		return Response(serializer.data, status=status.HTTP_200_OK)


# class ArticleCommentViewSet(viewsets.ModelViewSet):
# 		queryset = FluentComment.objects.all()
# 		serializer_class = CommentSerializer

# 		def get_queryset(self, queryset=None):
#         queryset = Category.tree.annotate('group_count': Count('group')})
#         queryset = queryset.get_cached_trees()
#         return queryset

# 		def create(self, request, *args, **kwargs):
# 				if request.user.is_authenticated:
# 						data = self.request.data
# 						comment = data['comment']
# 						article = data['Article']
# 						if 'parent' in data:
# 								parent = data['parent']
# 						else:
# 								parent = None
# 						submit_date = datetime.now()
# 						content = ContentType.objects.get(model="Article").pk
# 						comment = FluentComment.objects.create(
# 													object_pk=article,
# 													comment=comment, 
# 													submit_date=submit_date,   
# 													content_type_id=content,
# 													user_id = self.request.user.id,     
# 													site_id=settings.SITE_ID, 
# 													parent_id=parent)
# 						serializer = CommentSerializer(comment,context=  {'request': request})
# 						return Response(serializer.data)


class ArticleCommentViewSet(viewsets.ModelViewSet):
	serializer_class = ArticleCommentSerializer

	def get_queryset(self, queryset=None):
		queryset = ArticleComment.objects.all()
		queryset = queryset.get_cached_trees()
		return queryset

class ArticleCommentCreateAPIView(generics.CreateAPIView):
	queryset = ArticleComment.objects.all()
	serializer_class = ArticleCommentSerializer

	def perform_create(self, serializer):
		kwarg_slug = self.kwargs.get("slug")
		article = get_object_or_404(Article, slug=kwarg_slug)
		serializer.save(user=self.request.user, article=article)

class ArticleCommentsListAPIView(generics.ListAPIView):
	queryset = Article.objects.all()
	serializer_class = ArticleCommentSerializer
	pagination_class = None
	
	def get_queryset(self):
		kwarg_slug = self.kwargs.get("slug")
		article = get_object_or_404(Article, slug=kwarg_slug)
		return ArticleComment.objects.filter(article__slug=kwarg_slug).order_by("-created_at")

class ArticleCommentRUDAPIView(generics.RetrieveUpdateDestroyAPIView):
	queryset = ArticleComment.objects.all()
	serializer_class = ArticleCommentSerializer

class ArticleCommentLikeAPIView(APIView):
	queryset = ArticleComment.objects.all()
	serializer_class = ArticleCommentSerializer

	def delete(self, request, pk):
		comment = get_object_or_404(ArticleComment,pk=pk)
		user = request.user

		comment.voters.remove(user)
		comment.save()

		serializer_context = {"request": request}
		serializer = self.serializer_class(comment, context=serializer_context)

		return Response(serializer.data, status=status.HTTP_200_OK)

	def post(self, request, pk):
		comment = get_object_or_404(ArticleComment,pk=pk)
		user = request.user

		comment.voters.add(user)
		comment.save()

		serializer_context = {"request": request}
		serializer = self.serializer_class(comment, context=serializer_context)

		return Response(serializer.data, status=status.HTTP_200_OK)
