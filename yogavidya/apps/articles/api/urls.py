from django.urls import include, path
from rest_framework.routers import DefaultRouter
from yogavidya.apps.articles.api import views as av

router = DefaultRouter()
router.register(r"articles", av.ArticleViewSet)

urlpatterns = [
	path("", include(router.urls)),
	path("articleslist/index/", 
			av.ArticleIndexView.as_view(), 
			name="index-articles"
		),
	path("articles/<slug:slug>/comments/", 
			av.ArticleCommentsListAPIView.as_view(), 
			name="comments-list"
		),
	path("articleslist/most_liked/", 
			av.ArticlesMostLikedView.as_view(), 
			name="most-liked"
		),
	path("article/<slug:slug>/like/", 
		av.ArticleLikeAPIView.as_view(), 
		name="article-like"
	),
	path("article/<slug:slug>/comment/", 
			av.ArticleCommentCreateAPIView.as_view(), 
			name="comment-create"
		),
	path("articlecomments/<int:pk>/", 
			av.ArticleCommentRUDAPIView.as_view(), 
			name="comment-detail"
		),
	path("articlecomments/<int:pk>/like/", 
		av.ArticleCommentLikeAPIView.as_view(), 
		name="comment-like"
	),
	path("category_articles/<slug:slug>/list/", 
		av.CategoryArticlesListView.as_view(), 
		name="category-list"
	),
]