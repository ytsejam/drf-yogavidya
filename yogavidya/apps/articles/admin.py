from django.contrib import admin
from django import forms
from yogavidya.apps.articles.models import Article, ArticleComment
from yogavidya.apps.articles.forms import ArticleModelForm, ArticleCommentModelForm
from ckeditor.widgets import CKEditorWidget
 
 
class ArticleAdmin(admin.ModelAdmin):
	content = forms.CharField(widget=CKEditorWidget())
	form = ArticleModelForm
	prepopulated_fields = {'slug': ('title',), }
	class Meta:
		model = Article


class ArticleCommentAdmin(admin.ModelAdmin):
	form = ArticleCommentModelForm
	class Meta:
		model = ArticleComment
admin.site.register(Article, ArticleAdmin)
admin.site.register(ArticleComment)
