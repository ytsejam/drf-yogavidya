# -*- coding: utf-8 -*-
import os
from django.conf import settings
from django.urls import reverse
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

#from sorl.thumbnail import ImageField
from yogavidya.apps.articles import managers
from yogavidya.apps.categories.models  import Category
from yogavidya.apps.tags.models import Tag
from yogavidya.apps.core.utils import generate_random_string
#from taggit.managers import TaggableManager
from mptt.models import MPTTModel, TreeForeignKey
#from django.contrib.auth.models import User
from ckeditor.fields import RichTextField

# Create your models here.
class ImageField(models.ImageField):
	def value_to_string(self, obj): # obj is Model instance, in this case, obj is 'Class'
		return obj.fig.url # not return self.url

class Article(models.Model):
   
	def upload_location(instance, filename):
		#filebase, extension = filename.split(".")
		# return "%s/%s.%s" %(instance.id, instance.id, extension)
		try:
			ArticleModel = instance.__class__
			new_id = ArticleModel.objects.order_by("id").last().id + 1
		except:
			new_id=1

		return "{} {}".format(new_id, filename)
	# Relations
	categories = models.ManyToManyField(Category,  default=[1],  verbose_name=_(
		"related_categories"),related_name="article_categories" )
	tags = models.ManyToManyField(Tag,  default=[1], verbose_name=_(
		"related_tags"),related_name="article_tags" )
	#tags = TaggableManager()
	#similar_posts = instance.tags.similar_objects()[:5]
	#all_tabs = instance.tags.all()
	user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1,  verbose_name=_(
		"related_user"),related_name="articles", on_delete=models.CASCADE,)
	# Attributes - Mandatory
	id = models.AutoField(primary_key=True)
	title = models.CharField(max_length=200, verbose_name=_(
		"title"), help_text=_("Enter the Article title"))
	slug = models.SlugField(max_length=255, unique=True)
	filter_order = models.PositiveIntegerField(default=1)
	height_field = models.IntegerField(default=0)
	width_field = models.IntegerField(default=0)
	image = models.ImageField(
							null=True,
							blank=True,
							upload_to=upload_location,
							height_field="height_field",
							width_field="width_field")
	author = models.CharField(max_length=500, default="Yoga Vidya",)
	content = RichTextField()
	excerpt = models.CharField(max_length=140, null=True, blank=True )
	
	draft = models.BooleanField(default=False)
	index_page = models.BooleanField(default=False)
	publish = models.DateField(auto_now=False, auto_now_add=False)
	created_at = models.DateTimeField(
		auto_now=True, auto_now_add=False, verbose_name=("created_at"))
	updated_at = models.DateTimeField(auto_now_add=True, auto_now=False, verbose_name=_(
		"updated_at"), help_text=_("Enter a new date"))
	# Attributes - Optional
	users_like = models.ManyToManyField(settings.AUTH_USER_MODEL,
										related_name='articles_liked',
										blank=True)
	total_likes = models.PositiveIntegerField(db_index=True, default=0)
	# Object Manager
	objects = managers.ArticleManager()

	# Custom Properties

	# Methods
	def get_absolute_url(self):
		return reverse("articles:detail", kwargs={"slug": self.slug})

 
	# Meta and String
	class Meta:
		verbose_name = _("Article")
		verbose_name_plural = _("Articles")
		ordering = ("user", "title")
		unique_together = ("user", "title")
		
	def __str__(self):
		return "{} - {}".format(self.user.username, self.title)

	def human_format_time(self):
	   from django.utils.timesince import timesince
	   return timesince(self.publish)


class ArticleComment(MPTTModel):
	parent = TreeForeignKey('self', 
							null=True, 
							blank=True, 
							related_name='children',
							on_delete=models.CASCADE)
	user = models.ForeignKey(
					settings.AUTH_USER_MODEL,
					default=1,
					null=True,
					on_delete=models.CASCADE,
					related_name="articlecommenter")
	article = models.ForeignKey(
					Article, 
					on_delete=models.CASCADE,
					related_name="comments")
	active = models.BooleanField(default=True)
	published_at = models.DateField(
					auto_now_add=True,
					null=True,
					blank=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)
	content = models.TextField()
	voters = models.ManyToManyField(settings.AUTH_USER_MODEL,
									blank=True,
									related_name="article_comment_votes")

	class Meta:
		# sort comments in chronological order by default
		ordering = ('created_at',)

	def __str__(self):
		return 'Comment by {}'.format(self.content)

	def human_format_time(self):
	  from django.utils.timesince import timesince
	  return timesince(self.published_at)

	def get_all_children(self, include_self=False):
		"""
		Gets all of the comment thread.
		"""
		children_list = self._recurse_for_children(self)
		if include_self:
			ix = 0
		else:
			ix = 1
		flat_list = self._flatten(children_list[ix:])
		return flat_list

	def _recurse_for_children(self, node):
		children = []
		children.append(node)
		for child in node.sub_comment.enabled():
			if child != self:
				children_list = self._recurse_for_children(child)
				children.append(children_list)
		return children

	def _flatten(self, L):
		if type(L) != type([]): return [L]
		if L == []: return L
		return self._flatten(L[0]) + self._flatten(L[1:])
	def get_parent(self): 
		if self.parent:
			return self.parent 
		else: 
			return self 
	def get_thread(self): 
		return self.children.all()

	

@receiver(pre_save, sender=Article)
def pre_save_article_receiver(sender, instance, raw, using, **kwargs):
	if instance and not instance.slug:
		slug = slugify(instance.title)
		random_string = generate_random_string()
		instance.slug = slug + "_" + random_string


pre_save.connect(pre_save_article_receiver, sender=Article)
