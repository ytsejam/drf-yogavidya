# -*- coding: utf-8 -*-
from .base import *
from django.core.exceptions import ImproperlyConfigured
from django.urls import reverse_lazy
ALLOWED_HOSTS = ["www.yogavidya.com.tr", "yogavidya.com.tr"]
def get_env_variable(var_name):
    try:
        return os.environ[var_name]
    except KeyError:
        error_msg = "Set the %s environment variable" % var_name
        raise ImproperlyConfigured(error_msg)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': get_env_variable('DATABASE_NAME'),
        'USER': get_env_variable('DATABASE_USER'),
        'PASSWORD': get_env_variable('DATABASE_PASSWORD'),
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
CORS_ORIGIN_WHITELIST = (
     'https://www.yogavidya.com.tr',
     'http://localhost:8080',
     'https://localhost',
     'http://localhost',
 )
CSRF_COOKIE_NAME = "XSRF-TOKEN"
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SECURE_SSL_REDIRECT = True
SESSION_COOKIE_SECURE = True

STATIC_ROOT = "/home/ytsejam/public_html/yogavidya-django/static_cdn/"
MEDIA_ROOT = "/home/ytsejam/public_html/yogavidya-django/media_cdn/"
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static'), 
    os.path.join(BASE_DIR, 'frontend/dist'), 
    os.path.join(BASE_DIR, 'frontend/public/assets'), 
]

VUE_IMAGES_ROOT = "/home/ytsejam/public_html/yogavidya-django/frontend/dist/img/"

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]
DEBUG = False
