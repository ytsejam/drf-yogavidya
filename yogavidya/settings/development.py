# -*- coding: utf-8 -*-
from .base import *
ALLOWED_HOSTS = ["localhost"]
STATIC_ROOT = os.path.join(BASE_DIR, 'static_cdn')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),    
)
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

MEDIAFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),    
)
CSRF_COOKIE_NAME = "XSRF-TOKEN"
#SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SECURE_SSL_REDIRECT = False
SESSION_COOKIE_SECURE = False
DEBUG = True