import axios from "axios";
import { API_URL } from '@/services/config';
const ApiService = {
  loading: false,

  error: '',
  init () {
    axios.defaults.baseURL = API_URL
    axios.defaults.xsrfHeaderName = "X-CSRFToken"
    axios.defaults.xsrfCookieName = 'csrftoken'
  },
  get (path, data = null) {
    return this.call('get', path, data)
  },
  post (path, data = null) {
    return this.call('post', path, data)
  },

  call (method, path, data, options) {
    this.loading = true
    this.error = ''
    return axios.call(method, path, data, options)
      .catch((error) => {
        this.error = error.message
        // only re-throw if you want it caught higher-up the chain. if not, a console.error() will suffice
        throw new Error(`Api ${error}`)
      })
      .finally(() => {
        this.loading = false
      })
  },
  // get (resource, slug="") {
  //   return Vue.axios
  //       .get(`${resource}/${slug}`)
  //       .catch((error) => {
  //         throw new Error(`ApiService ${error}`)
  //   })
  // },
};
export default ApiService
