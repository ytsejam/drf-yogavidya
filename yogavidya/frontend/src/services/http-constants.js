import axios from "axios";
axios.defaults.xsrfHeaderName = "X-CSRFToken"
axios.defaults.xsrfCookieName = 'csrftoken'

let baseURL

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  baseURL = `http://localhost:8000/`
} else {
  baseURL = `https://www.yogavidya.com.tr/`
}

export const HTTP = axios.create(
  {
    baseURL: baseURL
  }

);