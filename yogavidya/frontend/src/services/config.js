export default {}

let API_SELECT_URL

if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
  API_SELECT_URL = `http://localhost:8000/api`
} else {
  API_SELECT_URL = `https://www.yogavidya.com.tr/api`
}

console.log(API_SELECT_URL)
export const API_URL= API_SELECT_URL