import {HTTP} from "./http-constants";
import axios from "axios";
import Cookies from 'js-cookie';
axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';
axios.defaults.xsrfCookieName = 'csrftoken';
axios.defaults.withCredentials = true; 
const apiCall = process.env.NODE_ENV === 'test' ? axios : HTTP;
var csrftoken = Cookies.get('XSRF-TOKEN');
console.log(csrftoken);
export default {
  async login(payload) {
    try {
      const {email, password} = {...payload};
      const response = await axios({
        method: 'post',
        url: '/rest-auth/login/',
        data: {
          email, password
        },
        headers:{
          "X-CSRFToken": csrftoken
        }
      });
      return Promise.resolve(response);
    } catch(err) {
      return Promise.reject(err);
    }
  },
  async register(payload) {
    try {
      const {username, password1, password2, email} = {...payload};
      const response = await apiCall
        .post('/api/rest-auth/registration/', {
          email,
          password1,
          password2,
          username
        });
        console.log("test");
        console.log(response);
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in DjangoService/register', err);
      return Promise.reject(err);
    }
  },
  async getAllUsers() {
    try {
      const response = await apiCall.get('/api/users/');
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in DjangoService/getAllUsers', err);
      return Promise.reject(err);
    }
  },
  async getUserInformation() {
    try {
      const response = await apiCall.get(`/api/user/information/`, {});
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in DjangoService/getUser', err);
      return Promise.reject(err);
    }
  },
  async getUserStatusses() {
    try {
      const response = await apiCall.get(`/api/user/information/`, {});
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in DjangoService/getUser', err);
      return Promise.reject(err);
    }
  },
  async updateUserInfo(payload) {
    try {
      const response = await apiCall
        .put(`/rest-auth/user/`,
          {
            first_name: payload.firstName,
            last_name: payload.lastName,
            username: payload.username
          });
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in DjangoService/updateUserInfo', err);
      return Promise.reject(err);
    }
  },
  async resetPassword(payload) {
    try {
      const response = await apiCall
        .post(`/api/rest-auth/password/reset/`,{email: payload.email});
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in DjangoService/resetPassword', err);
      return Promise.reject(err);
    }
  },
  async confirmPasswordReset(payload) {
    try {
      const response = await apiCall
        .post(`/rest-auth/password/reset/confirm/`,
          {
            uid: payload.uid,
            token: payload.token,
            new_password1: payload.new_password1,
            new_password2: payload.new_password2,
          });
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in DjangoService/confirmPasswordReset', err);
      return Promise.reject(err);
    }
  },
  async changePassword(payload) {
    try {
      const response = await apiCall
      .post(`/rest-auth/password/change/`,
        {
          old_password: payload.old_password,
          new_password1: payload.new_password1,
          new_password2: payload.new_password2,
        });
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in DjangoService/changePassword', err);
      return Promise.reject(err);
    }
  },
  async logout() {
    try {
      const response = await axios({
        method: 'post',
        url: '/rest-auth/logout/',
        data: {},
        headers:{
          "X-CSRFToken": csrftoken
        }
      });
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in DjangoService/logout', err);
      return Promise.reject(err);
    }
  },
}

 

 