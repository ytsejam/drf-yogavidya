import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import VueCarousel from "vue-carousel";
import VueAgile from 'vue-agile'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import Vuelidate from "vuelidate";
import { BootstrapVue, BootstrapVueIcons } from "bootstrap-vue";
import vueVimeoPlayer from 'vue-vimeo-player'
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import "./registerServiceWorker.js";
import "babel-polyfill";
import axios from "axios";
import ApiService from "./services/api.service.js";
import "@/services/DjangoUserService";
import createAxiosResponseInterceptor from '@/services/interceptors/interceptors'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faBars } from '@fortawesome/free-solid-svg-icons';
import { faCogs } from '@fortawesome/free-solid-svg-icons';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { faPlusCircle } from '@fortawesome/free-solid-svg-icons';
import { faFacebook } from '@fortawesome/free-brands-svg-icons';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faEllipsisH } from '@fortawesome/free-solid-svg-icons';
import { faSlidersH } from '@fortawesome/free-solid-svg-icons';
import { faUserSecret } from '@fortawesome/free-solid-svg-icons';
import { faComments} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import 'swiper/css/swiper.css'
import LightBox from 'vue-it-bigger'
import IconBase from '@/components/IconBase.vue'

axios.defaults.xsrfHeaderName = 'X-CSRFTOKEN';
axios.defaults.xsrfCookieName = 'csrftoken';
library.add(faBars,
						faEllipsisH,
            faHeart,
            faSlidersH,
						faUserSecret,
						faFacebook,
						faTwitter,
            faComments,
            faPlusCircle,
            faCogs)

Vue.component('font-awesome-icon', FontAwesomeIcon)

ApiService.init();
createAxiosResponseInterceptor();

Vue.config.productionTip = false;
Vue.prototype.$scrollToTop = () => window.scrollTo(0,0)

Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(axios);
Vue.use(VueCarousel);
Vue.use(VueAgile)
Vue.use(LightBox);
Vue.use(VueAwesomeSwiper, /* { default options with global component } */)
Vue.use(Vuelidate)
Vue.use(vueVimeoPlayer)
Vue.component('icon-base', IconBase);
//import '@/assets/bootstrap.css' ;
//import '@/assets/main.css' ;
//import '@/assets/custom.css';
var filter = function(text, length, clamp){
    clamp = clamp || '...';
    var node = document.createElement('div');
    node.innerHTML = text;
    var content = node.textContent;
    return content.length > length ? content.slice(0, length) + clamp : content;
};
 
Vue.filter('truncate', filter);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
