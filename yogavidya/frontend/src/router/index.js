import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import NotFound from "../views/NotFound.vue";
import Profile from "../views/Profile.vue";
import TeachersPage from "../views/TeachersPage.vue";
import Teacher from "../components/profiles/Teacher.vue";
import Timeline from "../views/Timeline.vue";
import ProfileSettings from "../components/profiles/ProfileSettings.vue";
import ProfilePhotos from "../components/profiles/ProfilePhotos.vue";
import PersonalInformation from "../components/profiles/PersonalInformation.vue";
import AccountSettings from "../components/profiles/AccountSettings.vue";
import ChangePassword from "../components/profiles/ChangePassword.vue";
import HobbiesInterests from "../components/profiles/HobbiesInterests.vue";
import Login from "../views/registration/Login.vue";
import Register from "../views/registration/Register.vue";

import Articles from "@/components/articles/Articles.vue";
import Article from "@/components/articles/Article.vue";
import CategoryArticles from "@/components/articles/CategoryArticles.vue";

import Books from "@/components/books/Books.vue";
import Book from "@/components/books/Book.vue";

import Page from "@/components/pages/Page.vue";
import TeacherInfo from "@/components/pages/TeacherInfo.vue";

import Videolessons from "@/components/videolessons/Videolessons.vue";
import Videolesson from "@/components/videolessons/Videolesson.vue";
import CategoryVideolessons from "@/components/videolessons/CategoryVideolessons.vue";
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/profile",
    name: "profile",
    component: Profile
  },
  {
    path: "/timeline",
    name: "timeline",
    component: Timeline
  },
  {
    path: "/profile_settings",
    name: "profile_settings",
    component: ProfileSettings
  },
  
  {
    path: "/personal_information/",
    name: "personal_information",
    component: PersonalInformation
  },
  {
    path: "/account_settings",
    name: "account_settings",
    component: AccountSettings
  },
  {
    path: "/photos",
    name: "profile_photos",
    component: ProfilePhotos
  },
  {
    path: "/change_password",
    name: "change_password",
    component: ChangePassword
  },
  {
    path: "/hobbies_interests",
    name: "hobbies_interests",
    component: HobbiesInterests
  },
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/register",
    name: "register",
    component: Register
  },
  {
    path: "/logout",
    name: "logout",
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "/egitmenlik",
    name: "TeacherInfo",
    component: TeacherInfo
  },

  {
    path: "/articles/",
    name: "articles",
    component: Articles,
    
  },
  {
    path: "/category/:slug/articles/",
    name: "categoryArticles",
    component: CategoryArticles,
  },
  {
    path: "/article/:slug/",
    name: "article",
    component: Article,
    props:true
  },
  {
    path: "/books/",
    name: "books",
    component: Books,
    
  },
  {
    path: "/book/:slug/",
    name: "book",
    component: Book,
    props:true
  },
  {
    path: "/page/:slug/",
    name: "page",
    component: Page,
    props:true
  },
  {
    path: "/teachers/",
    name: "teachers",
    component: TeachersPage,
    
  },
  {
    path: "/teachers/:slug/",
    name: "teacher",
    component: Teacher,
    
  },
  {
    path: "/videolessons/",
    name: "videolessons",
    component: Videolessons,
    
  },
  {
    path: "/videolesson/:slug/",
    name: "videolesson",
    component: Videolesson,
    props:true
  },
  {
    path: "/category/:slug/videolessons/",
    name: "categoryVideolessons",
    component: CategoryVideolessons,
  },
  {
    path: "*",
    name: "page-not-found",
    component: NotFound
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior() {
    document.getElementById('app').scrollIntoView();
  }
});

export default router;
