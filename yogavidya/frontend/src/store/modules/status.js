import ApiService from "@/services/api.service.js";
const state = {
  statusses: [],
  statuscomments: [],
  statuscomment: {},
  status: {},
  next: null,
  loading:false
};
const getters = {
  
  statusses(state) {
    return state.statusses;
  },
  status(state) {
    return state.status;
  },
  statuscomments(state) {
    return state.statuscomments;
  },
  statuscomment(state) {
    return state.statuscomment;
  },
  isLoading(state) {
    return state.loading;
  },
  next(state) {
    return state.next;
  }
};
const mutations = {
  fetchStart (state) {
    state.loading = true
  },
  "fetchEnd" (state) {
    state.loading = false
  },
  setStatusses (state, pStatusses, next) {
    state.statusses = pStatusses
    state.next = next
    state.errors = {}
  },
  setStatusComments (state, pStatuscomments) {
    state.statuscomments = pStatuscomments
    state.errors = {}
  },
  setNext(state, next) {
    state.next = next
  },
  setAStatus (state, pStatus) {
    state.status = pStatus
    state.errors = {}
  },
  setError (state, errors) {
    state.errors = errors
  }
}

const actions = {
  fetchStatusses (context) {
    context.commit("fetchStart");
    let endpoint = "statusses/"
      
    if (context.state.next) {
      endpoint = context.state.next;
    }

    return ApiService.get(endpoint)
        .then(data=>{
          console.log(data);
          
          context.commit("setStatusses", data.data.results)
          if (data.data.next) {
            context.commit("setNext", data.data.next);
          } else {
            context.commit("setNext", null)
          }
        })
        .catch((response) => {
        console.log(response);
        context.commit("setError", response.data.errors)
      })
  },
  fetchAStatus (context, payload) {
    context.commit("fetchStart")
    const {slug} = payload;
    return ApiService.get(`statusses/${slug}/`)
      .then((data) => {
        console.log(data);
        context.commit("setAStatus", data.data);
        context.commit("fetchEnd")
      })
      .then(

        ApiService.get(`statusses/${slug}/comments/`)
        .then((data) => {
          console.log(data);
          context.commit("setStatusComments", data.data.results);
          context.commit("fetchEnd")
        })
        .catch((response) => {

          context.commit("setError", response.data)
        })

      )
      .catch((response) => {

        context.commit("setError", response.data)
      })
  },
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
