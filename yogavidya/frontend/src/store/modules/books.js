import ApiService from "@/services/api.service.js";
const state = {
  books: [],
  book: {},
  next: null,
  loading:false
};
const getters = {
  book(state) {
    return state.book;
  },
  books(state) {
    return state.books;
  },
  isLoading(state) {
    return state.loading;
  },
  next(state) {
    return state.next;
  }
};
const mutations = {
  fetchStart (state) {
    state.loading = true
  },
  "fetchEnd" (state) {
    state.loading = false
  },
  setBooks (state, pBooks, next) {
    state.books = pBooks
    state.next = next
    state.errors = {}
  },
  setNext(state, next) {
    state.next = next
  },
  setABook (state, pBook) {
    state.book = pBook
    state.errors = {}
  },
  setError (state, errors) {
    state.errors = errors
  }
}

const actions = {
  fetchBooks (context) {
    context.commit("fetchStart");
    let endpoint = "books/"
      
    if (context.state.next) {
      endpoint = context.state.next;
    }

    return ApiService.get(endpoint)
        .then(data=>{
          console.log(data);
          
          context.commit("setBooks", data.data.results)
          if (data.data.next) {
            context.commit("setNext", data.data.next);
          } else {
            context.commit("setNext", null)
          }
        })
        .catch((response) => {
        console.log(response);
        context.commit("setError", response.data.errors)
      })
  },
  fetchAnBook (context, payload) {
    context.commit("fetchStart")
    const {slug} = payload;
    return ApiService.get(`books/${slug}/`)
      .then((data) => {
        console.log(data.data);
        context.commit("setABook", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {

        context.commit("setError", response.data)
      })
  },
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
