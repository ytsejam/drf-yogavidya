import ApiService from "@/services/api.service.js";
const getDefaultState = () => {
  return {
    articles: [],
    indexArticles:[],
    mostLikedArticles: [],
    articlecomments: [],
    articlecomment: {},
    article: {},
    next: null,
    previous: null,
    loading:false,
    current: null,
  }

}

const state = getDefaultState()

const getters = {
  articles(state) {
    return state.articles;
  },
  indexArticles(state) {
    return state.indexArticles;
  },
  mostLikedArticles(state) {
    return state.mostLikedArticles;
  },
  article(state) {
    return state.article;
  },
  articlecomments(state) {
    return state.articlecomments;
  },
  articlecomment(state) {
    return state.articlecomment;
  },
  isLoading(state) {
    return state.loading;
  },
  next(state) {
    return state.next;
  },
  previous(state) {
    return state.previous;
  },
  current(state) {
    return state.current;
  }
};
const mutations = {
  fetchStart (state) {
    state.loading = true
  },
  "fetchEnd" (state) {
    state.loading = false
  },
  setArticles (state, pArticles, next) {
    state.articles = pArticles
    state.next = next
   
    state.errors = {}
  },
  setMostLikedArticles (state, pArticles) {
    state.mostLikedArticles = pArticles
    state.errors = {}
  },
  setIndexArticles (state, pArticles) {
    state.indexArticles = pArticles
    state.errors = {}
  },
  setArticleComments (state, pArticles) {
    state.articlecomments = pArticles
    state.errors = {}
  },
  setNext(state, next) {
    state.next = next
  },
  setPrevious(state, previous) {
    state.previous = previous
  },
  setCurrent(state, current) {
    state.current = current
  },
  setAnArticle (state, pArticle) {
    state.article = pArticle
    state.errors = {}
  },
  setError (state, errors) {
    state.errors = errors
  },
  resetState (state) {
    Object.assign(state, getDefaultState())
  }
}

const actions = {
  fetchArticles (context) {
    context.commit("fetchStart");
    let endpoint = "articles/"
      
    if (context.state.next) {
      endpoint = context.state.next;
    }

    return ApiService.get(endpoint)
        .then(data=>{
          context.commit("setArticles", data.data.results);
          context.commit("setCurrent", data.data.current);
          if (data.data.next) {
            context.commit("setNext", data.data.next);
            context.commit("setPrevious", data.data.previous);
            
          } else {
            context.commit("setNext", null)
            context.commit("setPrevious", null)
          }
        })
        .catch((response) => {
        context.commit("setError", response.data.errors)
      })
  },
  
  fetchCategoryArticles (context, payload) {
    context.commit("resetState");
    context.commit("fetchStart");
    const {slug} = payload;
    let endpoint = `category_articles/${slug}/list/`

    if (context.state.next) {
      endpoint = context.state.next;
    }

    return ApiService.get(endpoint)
        .then(data=>{
          
          context.commit("setArticles", data.data);
          context.commit("setCurrent", data.data.current);
          if (data.data.next) {
            context.commit("setNext", data.data.next);
            
          } else {
            context.commit("setNext", null)
          }
        })
        .catch((response) => {
        console.log(response);
        context.commit("setError", response.data.errors)
      })
  },
  fetchAnArticle (context, payload) {
    context.commit("fetchStart")
    const {slug} = payload;
    return ApiService.get(`articles/${slug}/`)
      .then((data) => {
        context.commit("setAnArticle", data.data);
        context.commit("fetchEnd")
      })
      .then(

        ApiService.get(`articles/${slug}/comments/`)
        .then((data) => {
          console.log(data);
          context.commit("setArticleComments", data.data.results);
          context.commit("fetchEnd")
        })
        .catch((response) => {

          context.commit("setError", response.data)
        })

      )
      .catch((response) => {

        context.commit("setError", response.data)
      })
  },
  fetchIndexArticles (context) {
    context.commit("fetchStart")
    return ApiService.get('articleslist/index/')
      .then((data) => {
        context.commit("setIndexArticles", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {
        context.commit("setError", response.data.errors);
      })
    },
  fetchMostLikedArticles (context) {
    context.commit("fetchStart")
    return ApiService.get('articleslist/most_liked/')
      .then((data) => {
        context.commit("setMostLikedArticles", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {
        context.commit("setError", response.data.errors);
      })
    },
  resetArticlesState ({ commit }) {
    commit('resetState')
  },
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
