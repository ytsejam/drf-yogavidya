import ApiService from "@/services/api.service.js";
const getDefaultState = () => {
  return {
    teachers: [],
    students: [],
    teacher: {},
    student: {},
    next: null,
    loading:false,
    current: null,
  }

}

const state = getDefaultState()

const getters = {
  teachers(state) {
    return state.teachers;
  },
  students(state) {
    return state.students;
  },
  teacher(state) {
    return state.teacher;
  },
  student(state) {
    return state.student;
  },
  isLoading(state) {
    return state.loading;
  },
  next(state) {
    return state.next;
  },
  current(state) {
    return state.current;
  }
};

const mutations = {
  fetchStart (state) {
    state.loading = true
  },
  fetchEnd (state) {
    state.loading = false
  },
  setTeachers (state, pTeachers, next) {
    state.teachers = pTeachers
    state.next = next
    state.errors = {}
  },
  setStudents (state, pStudents, next) {
    state.students = pStudents
    state.next = next
    state.errors = {}
  },
  setATeacher (state, pTeacher, next) {
    state.teacher = pTeacher
    state.next = next
    state.errors = {}
  },
  setAStudent (state, pStudent, next) {
    state.student = pStudent
    state.next = next
    state.errors = {}
  },
  setNext(state, next) {
    state.next = next
  },
  setCurrent(state, current) {
    state.current = current
  },
  setError (state, errors) {
    state.errors = errors
  },
  resetState (state) {
    Object.assign(state, getDefaultState())
  }
};

const actions = {
  fetchTeachers (context) {
    context.commit("fetchStart");
    let endpoint = "teachers/"
      
    if (context.state.next) {
      endpoint = context.state.next;
    }

    return ApiService.get(endpoint)
        .then(data=>{
          console.log(data);
          
          context.commit("setTeachers", data.data);
          context.commit("setCurrent", data.data.current);
          if (data.data.next) {
            context.commit("setNext", data.data.next);
          } else {
            context.commit("setNext", null)
          }
        })
        .catch((response) => {
        console.log(response);
        context.commit("setError", response.data.errors)
      })
  },
  fetchStudents (context) {
    context.commit("fetchStart");
    let endpoint = "students/"
      
    if (context.state.next) {
      endpoint = context.state.next;
    }

    return ApiService.get(endpoint)
        .then(data=>{
          console.log(data);
          
          context.commit("setStudents", data.data.results);
          context.commit("setCurrent", data.data.current);
          if (data.data.next) {
            context.commit("setNext", data.data.next);
          } else {
            context.commit("setNext", null)
          }
        })
        .catch((response) => {
        console.log(response);
        context.commit("setError", response.data.errors)
      })
  },
  fetchATeacher (context, payload) {
    context.commit("fetchStart")
    const {slug} = payload
    return ApiService.get(`teachers/${slug}/`)
      .then((data) => {
        console.log(data);
        context.commit("setATeacher", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {
        context.commit("setError", response.data.errors)
      })
  },
  fetchAStudent (context, payload) {
    context.commit("fetchStart")
    const {slug} = payload
    return ApiService.get(`students/${slug}/`)
      .then((data) => {
        console.log(data);
        context.commit("setAStudent", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {
        context.commit("setError", response.data.errors)
      })
  },
  resetMembersState ({ commit }) {
    commit('resetState')
  },
};
 
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
