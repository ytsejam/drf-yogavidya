import ApiService from "@/services/api.service.js";
function initialState() {
  return {
    pages: [],
    page: {},
    loading:false
  }
  
};
const getters = {
  page(state) {
    return state.page;
  },
  pages(state) {
    return state.pages;
  },
  isLoading(state) {
    return state.loading;
  },
  pageTitle(state) {
    return state.pageTitle;
  }
};
const mutations = {
  fetchStart (state) {
    state.loading = true
  },
  fetchEnd (state) {
    state.loading = false
  },
  setPages (state, pPages) {
    state.pages = pPages
    state.errors = {}
  },
  setAPage (state, pPage) {
    state.page = pPage
    state.errors = {}
  },
  setError (state, errors) {
    state.errors = errors
  },
  setPageTitle (state, pTitle) {
    state.pageTitle = pTitle
  },
  setCleanPage(state){
    state.page = null
  },
  reset(state) {
    const s = initialState();
    Object.keys(s).forEach(key => {
      state[key] = s[key];
    });
    console.log('state', state)
  }
}

const actions = {
  fetchPages (context) {
    context.commit("fetchStart");
    let endpoint = "pages/"
      
    if (this.next) {
      endpoint = this.next;
    }

    return ApiService.get(endpoint)
        .then(data=>{
          context.commit("setPages", data.data.results)
          if (data.data.next) {
            this.next = data.data.next;
          } else {
            this.next = null;
          }
        })
        .catch((response) => {
        console.log(response);
        context.commit("setError", response.data.errors)
      })
  },
  fetchAPage (context, payload) {
    context.commit("fetchStart");
    context.commit("setCleanPage");
    const {slug} = payload;
    return ApiService.get(`pages/${slug}/`)
      .then((data) => {
        console.log(data.data);
        context.commit("setAPage", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {

        context.commit("setError", response.data)
      })
  },
  resetAPage(context){
    context.commit("reset");
  }
};
export default {
  namespaced: true,
  state: initialState,
  getters,
  actions,
  mutations
}
