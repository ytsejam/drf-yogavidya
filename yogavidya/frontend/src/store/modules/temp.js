import DjangoAPI from "@/services/DjangoUserService.js"
function initialState() {
  return {
    accessKey: null,
    expiration: 0,
    username: '',
    
  }
}

export const mutations = {
  setUserKey(state, payload) {
    state.accessKey = payload.accessKey;
    state.expiration = Date.now() + 30000
  },
  reset(state) {
    const s = initialState();
    Object.keys(s).forEach(key => {
      state[key] = s[key];
    });
    console.log('state', state)
  }
};

export const actions = {
  async loginUser(context, payload) {
    try {
      const response = await DjangoAPI.login(payload);
      context.commit('setUserKey',{
        accessKey: response.data.key,
      });
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in user.actions/loginUser', err)
      return Promise.reject(err);
    }
  },

  async getUserInfo(context, payload) {
    try{
      const response = await DjangoAPI.getUser(payload);
      console.log(payload);
      console.log(response);
      if (response.status == 403 ) {
        context.commit('setUserKey', null)
      }
      context.commit('setUserInfo', response.data)
    }
    catch(err) {
      console.log(err.response)
    }
  },
  async logout(context) {
    try {
      const response = await DjangoAPI.logout();
      context.commit('setUserKey',{accessKey: null});
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in store.user/logout', err);
      return Promise.reject(err);
    }
  }
};

<template>
  <div id="app">
    <HeaderComponent v-if="accessKey === null" />
    <ProfileHeader v-if="accessKey !== null" />
    <router-view />
    <FooterComponent />
  </div>
</template>
<script>
import { mapState } from 'vuex';
//import { apiService } from "@/common/api.service.js"
import FooterComponent from "@/components/snippets/Footer.vue"
import HeaderComponent from "@/components/snippets/Header.vue"
import ProfileHeader from "@/components/snippets/ProfileHeader.vue"

export default {
  name: "App",
  components: {
    HeaderComponent,
    ProfileHeader,
    FooterComponent,
  },
  data: () => ({
      drawer: false,
      drawerRight: false,
      right: false,
      left: false
    }),
    props: {
      source: String
  },
  methods: {

  },
  computed: {
    alert () {
        return this.$store.state.alert
      },
    ...mapState({
      accessKey: state => state.user.accessKey,
     }),
    //mapGetters('auth', ['isAuthenticated',]),
  },
  mounted() {
    console.log(this.$store.state.user.accessKey);
  },
  created() {
    //this.setUserInfo();
    
  }
};
</script>