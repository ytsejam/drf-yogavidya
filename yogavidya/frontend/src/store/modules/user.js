import DjangoAPI from "@/services/DjangoUserService.js";

const getDefaultState = () => {
  return {
    accessKey: null,
    expiration: 0,
    id: '',
    email: '',
    username: '',
    firstName: '',
    lastName: '',
    path: '',
    alertMessage: null,
    uid: '',
    token: '',
    image: '',
    thumbnail: '',
    is_student: '',
    is_teacher: '',
    bio: '',
    city: '',
    country: '',
    area: '',
    state: '',
    location: '',
    phone: '',
    gender: '',
    birth_date: '',
    ministatus: '',
    terms: '',
    height_field: '',
    width_field: '',
    hobbies: '',
    fav_tv: '',
    fav_books: '',
    fav_games: '',
    fav_movies: '',
    fav_music: '',
    fav_writers: '',
    fav_routes: '',
  }
}

const state = getDefaultState()

const getters = {};

const mutations = {
  setUserKey(state, payload) {
    state.accessKey = payload.accessKey;
    state.expiration = Date.now() + 30000
  },
  setSnackbar(state, payload) {
    state.alertMessage = payload.message;
    state.color = payload.color;
  },
  setAlertMessage(state, payload) {
    state.alertMessage = payload;
  },
  setPasswordResetData(state, payload) {
    state.uid = payload.uid;
    state.token = payload.token;
  },
  setUserInfo(state, payload) {
    state.email = payload.email;
    state.firstName = payload.first_name;
    state.lastName = payload.last_name;
    state.username = payload.username;
    state.birth_date = payload.birth_date;
    state.thumbnail = payload.thumbnail;
    state.big_thumbnail = payload.big_thumbnail;
    state.image = payload.image;
    state.city = payload.city;
    state.id = payload.id;
  },
  setUserDetails(state, payload) {
    state.is_student = payload.is_student;
    state.is_teacher = payload.is_teacher;
    state.first_name = payload.first_name;
    state.last_name = payload.last_name;
    state.bio = payload.bio;
    state.city = payload.city;
    state.country = payload.country;
    state.area = payload.area;
    state.state = payload.state;
    state.location = payload.location;
    state.phone = payload.phone;
    state.gender = payload.gender;
    state.birth_date = payload.birth_date;
    state.ministatus = payload.ministatus;
    state.terms = payload.terms;
    state.height_field = payload.height_field;
    state.width_field = payload.width_field;
    state.height_field = payload.height_field;
    state.hobbies = payload.hobbies;
    state.fav_tv = payload.fav_tv;
    state.fav_books = payload.fav_books;
    state.fav_games = payload.fav_games;
    state.fav_movies = payload.fav_movies;
    state.fav_music = payload.fav_music;
    state.fav_writers = payload.fav_writers;
    state.fav_routes = payload.fav_routes;
  },
  setFirstName(state, payload) {
    state.firstName = payload;
  },
  setLastName(state, payload) {
    state.lastName = payload;
  },
  setUsername(state, payload) {
    state.userName = payload;
  },
  // reset(state) {
  //   const s = initialState();
  //   Object.keys(s).forEach(key => {
  //     state[key] = s[key];
  //   });
  //   console.log('state', state)
  // },
  resetState (state) {
    Object.assign(state, getDefaultState())
  }
};

const actions = {
  async loginUser(context, payload) {
    try {
      const response = await DjangoAPI.login(payload);
      console.log(response);
      context.commit('setUserKey',{
        accessKey: response.data.key,
      });
      //Vue.http.headers.common['Authorization'] = 'Bearer ' + response.body.key
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in user.actions/loginUser', err)
      return Promise.reject(err);
    }
  },
  async registerUser(context, payload) {
    try {
      const response = await DjangoAPI.register(payload);
      context.commit('setUserKey',{
        accessKey: response.data.key,
      });
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in store.user/registerUser', err);
      return Promise.reject(err);
    }
  },
  async getAllUsers() {
    DjangoAPI.getAllUsers()
  },

  async getUserInfo(context, payload) {
    try{
      const response = await DjangoAPI.getUserInformation(payload);
      console.log(response);
      if (response.status == 403 ) {
        context.commit('setUserKey', payload.null)
      }
      context.commit('setUserInfo', response.data)
    }
    catch(err) {
      console.log(err)
    }
  },
  async getUserInformation(context, payload) {
    try{
      const response = await DjangoAPI.getUserInformation(payload);
      console.log(response);
      if (response.status == 403 ) {
        context.commit('setUserKey', payload.null)
      }
      context.commit('setUserDetails', response.data)
    }
    catch(err) {
      console.log(err.response)
    }
  },
  async updateUserInfo(context, payload) {
    try {
      const response = await DjangoAPI.updateUserInfo(payload);
      context.commit('setUserInfo', response.data);
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in store.user/updateUserInfo', err);
      return Promise.reject(err);
    }
  },
  async resetPassword(context, payload) {
    try {
      const response = await DjangoAPI.resetPassword(payload);
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in store.user/resetPassword', err);
      return Promise.reject(err);
    }
  },
  async confirmPasswordReset(context, payload) {
    try {
      const response = await DjangoAPI.confirmPasswordReset(payload);
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in store.user/confirmPasswordReset', err);
      return Promise.reject(err);
    }
  },
  async changePassword(context, payload) {
    try {
      const response = await DjangoAPI.changePassword(payload);
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in store.user/changePassword', err);
      return Promise.reject(err);
    }
  },
  async logout(context) {
    try {
      const response = await DjangoAPI.logout();
      context.commit('reset');
      return Promise.resolve(response);
    } catch(err) {
      console.log('err in store.user/logout', err);
      return Promise.reject(err);
    }
  },
  resetUserState ({ commit }) {
    commit('resetState')
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}