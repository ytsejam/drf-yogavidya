import ApiService from "@/services/api.service.js";
const getDefaultState = () => {
  return {
    videolessons: [],
    videolessoncomments: [],
    mostLikedVideolessons: [],
    indexvideolessons: [],
    othervideolessons: [],
    chakravideolessons: [],
    qavideolessons: [],
    videolesson: {},
    next: null,
    loading:false,
    current: null,
  }

}

const state = getDefaultState()

const getters = {
  videolessons(state) {
    return state.videolessons;
  },
  videolesson(state) {
    return state.videolesson;
  },
  videolessoncomments(state) {
    return state.videolessoncomments;
  },
  videolessoncomment(state) {
    return state.videolessoncomment;
  },
  mostLikedVideolessons(state) {
    return state.mostLikedVideolessons;
  },
  indexvideolessons(state) {
    return state.indexvideolessons;
  },
  othervideolessons(state) {
    return state.othervideolessons;
  },
  chakravideolessons(state) {
    return state.chakravideolessons;
  },
  qavideolessons(state) {
    return state.qavideolessons;
  },
  isLoading(state) {
    return state.loading;
  },
  next(state) {
    return state.next;
  },
  current(state) {
    return state.current;
  }
};

const mutations = {
  fetchStart (state) {
    state.loading = true
  },
  fetchEnd (state) {
    state.loading = false
  },
  setVideolessons (state, pVideolessons, next) {
    state.videolessons = pVideolessons
    state.next = next
    state.errors = {}
  },
  setVideolessonComments (state, pVideolessons) {
    state.videolessoncomments = pVideolessons
    state.errors = {}
  },
  setNext(state, next) {
    state.next = next
  },
  setCurrent(state, current) {
    state.current = current
  },
  setMostLikedVideolessons (state, pVideolessons) {
    state.mostLikedVideolessons = pVideolessons
    state.errors = {}
  },
  setIndexVideolessons (state, pVideolessons) {
    state.indexvideolessons = pVideolessons
    state.errors = {}
  },
  setOthersVideolessons (state, pVideolessons) {
    state.othervideolessons = pVideolessons
    state.errors = {}
  },
  setChakrasVideolessons (state, pVideolessons) {
    state.chakravideolessons = pVideolessons
    state.errors = {}
  },
  setQAVideolessons (state, pVideolessons) {
    state.qavideolessons = pVideolessons
    state.errors = {}
  },
  setAVideolesson (state, pVideolesson) {
    state.videolesson = pVideolesson
    state.errors = {}
  },
  setError (state, errors) {
    state.errors = errors
  },
  resetState (state) {
    Object.assign(state, getDefaultState())
  }
};
 

const actions = {
  fetchVideolessons (context) {
    context.commit("fetchStart");
    let endpoint = "videolessons/"
      
    if (context.state.next) {
      endpoint = context.state.next;
    }

    return ApiService.get(endpoint)
        .then(data=>{
          
          context.commit("setVideolessons", data.data.results);
          context.commit("setCurrent", data.data.current);
          if (data.data.next) {
            context.commit("setNext", data.data.next);
          } else {
            context.commit("setNext", null)
          }
        })
        .catch((response) => {
        console.log(response);
        context.commit("setError", response.data.errors)
      })
  },
  fetchCategoryVideolessons (context, payload) {
    context.commit("resetState");
    context.commit("fetchStart");
    const {slug} = payload;
    let endpoint = `category_videolessons/${slug}/list/`

    if (context.state.next) {
      endpoint = context.state.next;
    }

    return ApiService.get(endpoint)
        .then(data=>{
          
          context.commit("setVideolessons", data.data.results);
          context.commit("setCurrent", data.data.current);
          if (data.data.next) {
            context.commit("setNext", data.data.next);
            
          } else {
            context.commit("setNext", null)
          }
        })
        .catch((response) => {
        console.log(response);
        context.commit("setError", response.data.errors)
      })
  },

  fetchAVideolesson (context, payload) {
    context.commit("fetchStart")
    const {slug} = payload;
    return ApiService.get(`videolessons/${slug}/`)
      .then((data) => {
        context.commit("setAVideolesson", data.data);
        context.commit("fetchEnd")
      })
      .then(

        ApiService.get(`videolessons/${slug}/comments/`)
        .then((data) => {
          console.log(data);
          context.commit("setVideolessonComments", data.data.results);
          context.commit("fetchEnd")
        })
        .catch((response) => {

          context.commit("setError", response.data)
        })

      )
      .catch((response) => {

        context.commit("setError", response.data)
      })
  },
  fetchIndexVideolessons (context) {
    context.commit("fetchStart")
    return ApiService.get('videolessonslist/index/')
      .then((data) => {
        console.log(data);
        context.commit("setIndexVideolessons", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {
        context.commit("setError", response.data.errors);
      })
  },
  fetchOthersVideolessons (context) {
    context.commit("fetchStart")
    return ApiService.get('videolessonslist/others/')
      .then((data) => {
        context.commit("setOthersVideolessons", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {
        context.commit("setError", response.data.errors);
      })
  },
  fetchChakrasVideolessons (context) {
    context.commit("fetchStart")
    return ApiService.get('videolessonslist/chakras/')
      .then((data) => {
        context.commit("setChakrasVideolessons", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {
        context.commit("setError", response.data.errors);
      })
  },
  fetchQAVideolessons (context) {
    context.commit("fetchStart")
    return ApiService.get('videolessonslist/qa/')
      .then((data) => {
        context.commit("setQAVideolessons", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {
        context.commit("setError", response.data.errors);
      })
  },
  fetchMostLikedVideolessons (context) {
    context.commit("fetchStart")
    return ApiService.get('videolessonslist/most_liked/')
      .then((data) => {
        console.log(data);
        context.commit("setMostLikedVideolessons", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {
        context.commit("setError", response.data.errors);
      })
    },
  resetVideolessonsState ({ commit }) {
    commit('resetState')
  },
};
 
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
