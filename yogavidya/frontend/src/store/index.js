import Vue from "vue";
import Vuex from "vuex";
import articles from "./modules/articles";
import books from "./modules/books";
import members from "./modules/members";
import status from "./modules/status";
import pages from "./modules/pages";
import user from "./modules/user";
import videolessons from "./modules/videolessons";
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex);
const debug = process.env_NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
  	articles,
  	books,
  	members,
    pages,
    status,
  	user,
  	videolessons,
  },
  strict: debug,
  plugins: [createPersistedState({
  })]
});
