import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import axios from "axios";
import ApiService from "./services/api.service.js";
import "@/services/DjangoUserService";
import createAxiosResponseInterceptor from '@/services/interceptors/interceptors'
import createAxiosResponseInterceptor from '@/services/interceptors/interceptors'
import VueAxios from "vue-axios";
import VueCarousel from "vue-carousel";
import { BootstrapVue, BootstrapVueIcons } from "bootstrap-vue";

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import "./registerServiceWorker.js";
import "babel-polyfill";

Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(VueAxios, axios);
Vue.use(VueCarousel);
Vue.config.productionTip = false;
Vue.prototype.$scrollToTop = () => window.scrollTo(0,0)
ApiService.init();
import dotenv from "dotenv";

dotenv.config()
if(config.error){
  console.log('Could not load env file', config.error)
}
console.log(process.env);


createAxiosResponseInterceptor();


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
