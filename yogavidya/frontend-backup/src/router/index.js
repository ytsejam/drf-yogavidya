import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import Login from "../views/registration/Login.vue";
import Articles from "@/components/articles/Articles.vue";
import Article from "@/components/articles/Article.vue";
import Books from "@/components/books/Books.vue";
import Book from "@/components/books/Book.vue";
import Page from "@/components/pages/Page.vue";
import TeacherInfo from "@/components/pages/TeacherInfo.vue";
import Videolessons from "@/components/videolessons/Videolessons.vue";
import Videolesson from "@/components/videolessons/Videolesson.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/login",
    name: "login",
    component: Login
  },
  {
    path: "/login",
    name: "logout",
    component: Login
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: "/egitmenlik",
    name: "TeacherInfo",
    component: TeacherInfo
  },

  {
    path: "/articles/",
    name: "articles",
    component: Articles,
    
  },
  {
    path: "/article/:slug/",
    name: "article",
    component: Article,
    props:true
  },
  {
    path: "/books/",
    name: "books",
    component: Books,
    
  },
  {
    path: "/book/:slug/",
    name: "book",
    component: Book,
    props:true
  },
  {
    path: "/page/:slug/",
    name: "page",
    component: Page,
    props:true
  },
  {
    path: "/videolessons/",
    name: "videolessons",
    component: Videolessons,
    
  },
  {
    path: "/videolesson/:slug/",
    name: "videolesson",
    component: Videolesson,
    props:true
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
  scrollBehavior() {
    document.getElementById('app').scrollIntoView();
  }
});

export default router;
