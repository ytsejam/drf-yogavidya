import ApiService from "@/services/api.service.js";
const state = {
  articles: [],
  article: {},
  loading:false
};
const getters = {
  article(state) {
    return state.article;
  },
  articles(state) {
    return state.articles;
  },
  isLoading(state) {
    return state.loading;
  }
};
const mutations = {
  fetchStart (state) {
    state.loading = true
  },
  "fetchEnd" (state) {
    state.loading = false
  },
  setArticles (state, pArticles) {
    state.articles = pArticles
    state.errors = {}
  },
  setAnArticle (state, pArticle) {
    state.article = pArticle
    state.errors = {}
  },
  setError (state, errors) {
    state.errors = errors
  }
}

const actions = {
  fetchArticles (context) {
    context.commit("fetchStart");
    let endpoint = "articles/"
      
    if (this.next) {
      endpoint = this.next;
    }

    return ApiService.get(endpoint)
        .then(data=>{
          context.commit("setArticles", data.data.results)
          if (data.data.next) {
            this.next = data.data.next;
          } else {
            this.next = null;
          }
        })
        .catch((response) => {
        console.log(response);
        context.commit("setError", response.data.errors)
      })
  },
  fetchAnArticle (context, payload) {
    context.commit("fetchStart")
    const {slug} = payload;
    return ApiService.get(`articles/${slug}/`)
      .then((data) => {
        console.log(data.data);
        context.commit("setAnArticle", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {

        context.commit("setError", response.data)
      })
  },
  fetchIndexArticles (context) {
    context.commit("fetchStart")
    return ApiService.get('articleslist/index/')
      .then((data) => {
        console.log(data.data);
        context.commit("setArticles", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {
        context.commit("setError", response.data.errors);
      })
    }
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
