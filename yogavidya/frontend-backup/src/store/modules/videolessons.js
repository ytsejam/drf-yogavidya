import ApiService from "@/services/api.service.js";
import axios from "axios";

const state = {
	videolessons: [],
  indexvideolessons: [],
  othervideolessons: [],
  chakravideolessons: [],
  qavideolessons: [],
	videolesson: {},
	loading:false
};
const getters = {
	videolesson(state) {
		return state.videolesson;
	},
	videolessons(state) {
		return state.videolessons;
	},
  indexvideolessons(state) {
    return state.indexvideolessons;
  },
  othervideolessons(state) {
    return state.othervideolessons;
  },
  chakravideolessons(state) {
    return state.chakravideolessons;
  },
  qavideolessons(state) {
    return state.qavideolessons;
  },
	isLoading(state) {
		return state.loading;
	}
};
const mutations = {
  fetchStart (state) {
    state.loading = true
  },
  fetchEnd (state) {
    state.loading = false
  },
  setVideolessons (state, pVideolessons) {
    state.videolessons = pVideolessons
    state.errors = {}
  },
  setIndexVideolessons (state, pVideolessons) {
    state.indexvideolessons = pVideolessons
    state.errors = {}
  },
  setOthersVideolessons (state, pVideolessons) {
    state.othervideolessons = pVideolessons
    state.errors = {}
  },
  setChakrasVideolessons (state, pVideolessons) {
    state.chakravideolessons = pVideolessons
    state.errors = {}
  },
  setQAVideolessons (state, pVideolessons) {
    state.qavideolessons = pVideolessons
    state.errors = {}
  },
  setAVideolesson (state, pVideolesson) {
    state.videolesson = pVideolesson
    state.errors = {}
  },
  setError (state, errors) {
    state.errors = errors
  }
}

const actions = {
  fetchVideolessons (context) {
    context.commit("fetchStart")
    return ApiService.init()
      .get('videolessons/')
      .then((data) => {
        context.commit("setVideolessons", data.videolessons.results);
        context.commit("fetchEnd")
      })
      .catch((response) => {
        context.commit("setError", response.data.errors)
      })
  },

  fetchAVidelesson (context, payload) {
    context.commit("fetchStart")
    const {slug} = payload
    return axios(`http://localhost:8000/api/videolessons/${slug}/`)
      .then((data) => {
        context.commit("setAVideolesson", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {
        context.commit("setError", response.data.errors)
      })
  },
  fetchIndexVideolessons (context) {
    context.commit("fetchStart")
    return axios('http://localhost:8000/api/videolessonslist/index/')
      .then((data) => {
        context.commit("setIndexVideolessons", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {
        context.commit("setError", response.data.errors);
      })
    },

  fetchOthersVideolessons (context) {
    context.commit("fetchStart")
    return axios('http://localhost:8000/api/videolessonslist/others/')
      .then((data) => {
        console.log(data.data);
        context.commit("setOthersVideolessons", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {
        context.commit("setError", response.data.errors);
      })
    },

  fetchChakrasVideolessons (context) {
    context.commit("fetchStart")
    return axios('http://localhost:8000/api/videolessonslist/chakras/')
      .then((data) => {
        console.log(data.data);
        context.commit("setChakrasVideolessons", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {
        context.commit("setError", response.data.errors);
      })
    },

  fetchQAVideolessons (context) {
    context.commit("fetchStart")
    return axios('http://localhost:8000/api/videolessonslist/qa/')
      .then((data) => {
        console.log(data.data);
        context.commit("setQAVideolessons", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {
        context.commit("setError", response.data.errors);
      })
    },
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
