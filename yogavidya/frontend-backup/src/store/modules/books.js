import ApiService from "@/services/api.service.js";
const state = {
  books: [],
  book: {},
  loading:false
};
const getters = {
  book(state) {
    return state.book;
  },
  books(state) {
    return state.books;
  },
  isLoading(state) {
    return state.loading;
  }
};
const mutations = {
  fetchStart (state) {
    state.loading = true
  },
  "fetchEnd" (state) {
    state.loading = false
  },
  setBooks (state, pBooks) {
    state.books = pBooks
    state.errors = {}
  },
  setABook (state, pBook) {
    state.book = pBook
    state.errors = {}
  },
  setError (state, errors) {
    state.errors = errors
  }
}

const actions = {
  fetchBooks (context) {
    context.commit("fetchStart");
    let endpoint = "books/"
      
    if (this.next) {
      endpoint = this.next;
    }

    return ApiService.get(endpoint)
        .then(data=>{
          context.commit("setBooks", data.data.results)
          if (data.data.next) {
            this.next = data.data.next;
          } else {
            this.next = null;
          }
        })
        .catch((response) => {
        console.log(response);
        context.commit("setError", response.data.errors)
      })
  },
  fetchAnBook (context, payload) {
    context.commit("fetchStart")
    const {slug} = payload;
    return ApiService.get(`books/${slug}/`)
      .then((data) => {
        console.log(data.data);
        context.commit("setABook", data.data);
        context.commit("fetchEnd")
      })
      .catch((response) => {

        context.commit("setError", response.data)
      })
  },
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
