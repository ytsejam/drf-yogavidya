const state = {
  articles: [],

};
const getters = {
  articles(state) {
    return state.articles;
  },
};
const mutations = {
  [SET_ARTICLES] (state, pArticles) {
    state.article = pArticles
    state.errors = {}
  }
}
const actions = {
  [FETCH_INDEX_ARTICLES] (context) {
    context.commit(FETCH_START)
    return ApiService
      .get('/articlelist/index/')
      .then((data) => {
        context.commit(SET_ARTICLES, data.articles);
        context.commit(FETCH_END)
      })
      .catch((response) => {
        context.commit(SET_ERROR, response.data.errors);
      })
    }
};
export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
