// global
export const FETCH_START = "loadingOn"
export const FETCH_END = "loadingOff"
export const SET_ERROR = "setError"
// related to cats
export const SET_AN_ARTICLE = "setAnArticle"
export const SET_ARTICLES = "setArticles"

export const SET_A_BOOK = "setABook"
export const SET_BOOKS = "setBooks"

export const SET_A_PAGE = "setAPage"
export const SET_PAGES = "setPages"

export const SET_A_VIDEOLESSON = "setAnVideolesson"
export const SET_VIDEOLESSONS = "setVideolessons"
export const SET_INDEX_VIDEOLESSONS = "setIndexVideolessons"
export const SET_OTHERS_VIDEOLESSONS = "setOthersVideolessons"
export const SET_CHAKRAS_VIDEOLESSONS = "setChakrasVideolessons"
export const SET_QA_VIDEOLESSONS = "setQAVideolessons"