export const FETCH_AN_ARTICLE = "fetchAnArticle"
export const FETCH_ARTICLES = "fetchArticles"
export const FETCH_INDEX_ARTICLES = "fetchIndexArticles"

export const FETCH_A_BOOK = "fetchABook"
export const FETCH_BOOKS = "fetchBooks"

export const FETCH_A_PAGE = "fetchAPage"
export const FETCH_PAGES = "fetchPages"

export const FETCH_A_VIDEOLESSON = "fetchAVideolesson"
export const FETCH_VIDEOLESSONS = "fetchVideolessons"
export const FETCH_INDEX_VIDEOLESSONS = "fetchIndexVideolessons"
export const FETCH_OTHERS_VIDEOLESSONS = "fetchOthersVideolessons"
export const FETCH_CHAKRAS_VIDEOLESSONS = "fetchChakrasVideolessons"
export const FETCH_QA_VIDEOLESSONS = "fetchQAVideolessons"

