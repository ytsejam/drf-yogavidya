import Vue from "vue";
import Vuex from "vuex";
import articles from "./modules/articles";
import books from "./modules/books";
import pages from "./modules/pages";
import user from "./modules/user";
import videolessons from "./modules/videolessons";
import createPersistedState from 'vuex-persistedstate'

Vue.use(Vuex);
const debug = process.env_NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
  	articles,
  	books,
  	pages,
  	user,
  	videolessons,
  },
  strict: debug,
  plugins: [createPersistedState({
  })]
});
export store