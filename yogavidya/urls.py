from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django.urls import include, path, re_path
from django_registration.backends.one_step.views import RegistrationView
from django.contrib.auth.views import PasswordResetConfirmView
from yogavidya.apps.profiles.forms import YogaUserForm
from yogavidya.apps.core.views import IndexTemplateView
from django.conf.urls.static import static 
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.views.static import serve  
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView
)            
from .views import home
from yogavidya.apps.articles import views
from yogavidya.apps.videolessons import views
from rest_auth.registration.views import VerifyEmailView, RegisterView
from yogavidya.apps.profiles.api.views import ConfirmEmailView
from baton.autodiscover import admin
urlpatterns = [
    path('admin/', admin.site.urls),
    path('baton/', include('baton.urls')),
    path('accounts/register/',
            RegistrationView.as_view(
                form_class=YogaUserForm,
                success_url="/"
            ), name="django_registration_register"),
    path('accounts/reset/<uidb64>/<token>/', PasswordResetConfirmView.as_view(
                    template_name="account/password_reset.html"), 
    name='password_reset_confirm'),
    path('accounts/',
            include('django_registration.backends.one_step.urls')),
    path('accounts/',
            include('django.contrib.auth.urls')),
    
    
    path(_('articles/'), include('yogavidya.apps.articles.urls', namespace='articles')),
    path(_('pages/'), include('yogavidya.apps.pages.urls', namespace='pages')),
    path(_('books/'), include('yogavidya.apps.books.urls', namespace='merchandises')),
    path(_('videolessons/'), include('yogavidya.apps.videolessons.urls', namespace='videolessons')),
    path(_('profiles/'), include('yogavidya.apps.profiles.urls', namespace='profiles')),
]
urlpatterns += [
    
    path('api/',
        include("yogavidya.apps.articles.api.urls")),
    path('api/',
        include("yogavidya.apps.books.api.urls")),
    path('api/',
        include("yogavidya.apps.categories.api.urls")),
    path('api/',
        include("yogavidya.apps.filemanager.api.urls")),
    path('api/',
        include("yogavidya.apps.locations.api.urls")),
    path('api/',
        include("yogavidya.apps.pages.api.urls")),
    path('api/',
        include("yogavidya.apps.profiles.api.urls")),
    path('api/',
        include("yogavidya.apps.statusses.api.urls")),
    path('api/',
        include("yogavidya.apps.tags.api.urls")),
    path('api/',
        include("yogavidya.apps.videolessons.api.urls")),
    path('api-auth/',
		include("rest_framework.urls")),
    path('api/rest-auth/',
		include("rest_auth.urls")),
    path('api/rest-auth/registration/',
		include("rest_auth.registration.urls")),

    path('api/rest-auth/registration/verify-email/', VerifyEmailView.as_view(),
     name='account_email_verification_sent'),
    path('api/rest-auth/password/reset/', VerifyEmailView.as_view(),
     name='account_email_verification_sent'),
    #re_path(r'^account-confirm-email/(?P<key>[-:\w]+)/$', VerifyEmailView.as_view(
    #            template_name="account/email_confirm.html"
    #        ),
    re_path(r'^rest-auth/registration/account-confirm-email/(?P<key>[-:\w]+)/$', 
                ConfirmEmailView.as_view(), name='account_confirm_email'),

    # NEW: custom verify-token view which is not included in django-rest-passwordreset
    #path('reset-password/verify-token/', CustomPasswordTokenVerificationView.as_view(), name='password_reset_verify_token'),
    # NEW: The django-rest-passwordreset urls to request a token and confirm pw-reset
    #path('api/reset-password/', include('django_rest_passwordreset.urls', namespace='password_reset')),
    path('api/password_reset/', include('django_rest_passwordreset.urls', namespace='password_reset')),

    re_path(r'^media/(?P<path>.*)$', serve, {
        'document_root': settings.MEDIA_ROOT
    }),
    
    #re_path(r'^img/(?P<path>.*)$', serve, {
    #    'document_root': settings.VUE_IMAGES_ROOT
    #}),
    re_path(r'^.*$', IndexTemplateView.as_view(), name="home"),

    
]

       # add this

if settings.DEBUG:     
    urlpatterns += staticfiles_urlpatterns()  
    urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)


