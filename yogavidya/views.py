from datetime import timedelta
from yogavidya.apps.articles.models import Article
from yogavidya.apps.videolessons.models import Videolesson
import datetime
from django.utils.timezone import now
from django.shortcuts import render
from datetime import timedelta
from yogavidya.apps.articles.models import Article
from yogavidya.apps.videolessons.models import Videolesson
from django.urls import reverse
from django.conf import settings
def home(request):
	today = datetime.date.today()
	index_article_list = Article.objects.filter(index_page=True).order_by("created_at")
	index_videolesson_list = Videolesson.objects.filter(index_page=True).order_by("created_at")
	chakra_videolesson_list = Videolesson.objects.filter(chakra_page=True).order_by("created_at")
	other_videolesson_list = Videolesson.objects.filter(other_page=True).order_by("created_at")
	qa_videolesson_list = Videolesson.objects.filter(chakra_page=False).filter(qa_page=True).order_by("created_at")
	last_articles = Article.objects.filter(index_page=True).order_by("created_at")[:2]
	last_videolessons = Videolesson.objects.filter(index_page=True).order_by("created_at")[:2]

	context =  {
						'today': today,
						'now': now(),
						'sectiontitle': 'YogaVidya',
						'index_article_list': index_article_list,
						'index_videolesson_list': index_videolesson_list,
						'last_videolessons': last_videolessons,
						'chakra_videolesson_list': chakra_videolesson_list,
						'other_videolesson_list': other_videolesson_list,
						'qa_videolesson_list': qa_videolesson_list,
						'last_articles': last_articles,
	}
	
	return render(request, "yogavidya/index.html", context)